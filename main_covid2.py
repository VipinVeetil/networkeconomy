from __future__ import division
import main
import cPickle
import parameters as p
import run 
import write_network
import csv
import os
import matplotlib.pyplot as plt
from collections import OrderedDict
import copy

def convergence():
  transientTimeSteps = 500
  timeSteps = 1
  pVariables = {'economy': ['mean_price_change'],'firm': None,'household':None}

  baseOthers={'network_type': 'SF',
              'n':1000,
              'd':5,
              'production_function' : 'CES',
              'network_from_file': False,
              'household_preference_homogeneous': False,
              'representative_household': True,
              'transient_time_steps': transientTimeSteps,
              'time_steps': timeSteps,
              'stochastic_productivity': False,
              'firm_productivity_homogeneous':False,
              'one_time_productivity_shock':False,
              'labor_mobility':0.1,
              'record_transient_data':True}  


  iterations = 10
  cores = 10

  parameter_names = ['labor_mobility']

  parameter_ranges = {'labor_mobility':[0.1,1.01]}
  parameter_increments = {'labor_mobility':0.1}
  variables = {'labor_mobility':pVariables}
  other_parameters = {'labor_mobility':baseOthers}
  iterations_parameters = {'labor_mobility':iterations}
  cores_parameters = {'labor_mobility':cores}
  network = 'SF'
  fun = 'CES_convergence__'


  main.main(network_name = network, production_function=fun,  time_series={'economy':True, 'firm':True, 'household': False},
                  parameter_names=parameter_names,
                  parameter_ranges=parameter_ranges,
                  parameter_increments=parameter_increments,
                  variables=variables,
                  other_parameters_=other_parameters,
                  iterations_=iterations_parameters,
                  cores_=cores_parameters,
                  model_name='network_economy',
                  model_url='url',
                  paper_name='realShocks',
                  paper_url='url')

def firms_one_time_productivity():
  transientTimeSteps = 100
  timeSteps = 100
  pVariables = {'economy': None,'firm': ['wealth'],'household':None}

  baseOthers={'network_type': 'SF',
              'n':10000,
              'd':5,
              'production_function' : 'CES',
              'network_from_file': False,
              'household_preference_homogeneous': False,
              'representative_household': True,
              'transient_time_steps': transientTimeSteps,
              'time_steps': timeSteps,
              'stochastic_productivity': False,
              'firm_productivity_homogeneous':False,
              'one_time_productivity_shock':True,
              'labor_mobility':0.1}  


  iterations = 1
  cores = 1

  parameter_names = ['one_time_productivity_shock_std']

  parameter_ranges = {'one_time_productivity_shock_std':[0.1,0.2]}
  parameter_increments = {'one_time_productivity_shock_std':0.5}
  variables = {'one_time_productivity_shock_std':pVariables}
  other_parameters = {'one_time_productivity_shock_std':baseOthers}
  iterations_parameters = {'one_time_productivity_shock_std':iterations}
  cores_parameters = {'one_time_productivity_shock_std':cores}
  network = 'SF'
  fun = 'CES_firm'


  main.main(network_name = network, production_function=fun,  time_series={'economy':True, 'firm':True, 'household': False},
                  parameter_names=parameter_names,
                  parameter_ranges=parameter_ranges,
                  parameter_increments=parameter_increments,
                  variables=variables,
                  other_parameters_=other_parameters,
                  iterations_=iterations_parameters,
                  cores_=cores_parameters,
                  model_name='network_economy',
                  model_url='url',
                  paper_name='realShocks',
                  paper_url='url')

def one_time_productivity():
  transientTimeSteps = 100
  timeSteps = 100
  pVariables = {'economy': ['gdp_equilibrium_prices','unemployment_rate'],'firm': None,'household':None}

  baseOthers={'network_type': 'SF',
              'n':1000,
              'd':5,
              'production_function' : 'CES',
              'network_from_file': False,
              'household_preference_homogeneous': False,
              'representative_household': True,
              'transient_time_steps': transientTimeSteps,
              'time_steps': timeSteps,
              'stochastic_productivity': False,
              'firm_productivity_homogeneous':False,
              'one_time_productivity_shock':True,
              'labor_mobility':0.1}  


  iterations = 100
  cores = 10

  #parameter_names = ['labor_mobility','one_time_productivity_shock_std']
  #parameter_names = ['labor_mobility']
  parameter_names = ['one_time_productivity_shock_std']

  parameter_ranges = {'one_time_productivity_shock_std':[0.0,10.1],'labor_mobility':[0.1,1.01]}
  parameter_increments = {'one_time_productivity_shock_std':0.05,'labor_mobility':0.05}
  variables = {'one_time_productivity_shock_std':pVariables,'labor_mobility':pVariables}
  other_parameters = {'one_time_productivity_shock_std':baseOthers,'labor_mobility':baseOthers}
  iterations_parameters = {'one_time_productivity_shock_std':iterations,'labor_mobility':iterations}
  cores_parameters = {'one_time_productivity_shock_std':cores,'labor_mobility':cores}
  network = 'SF'
  fun = 'CES'


  main.main(network_name = network, production_function=fun,  time_series={'economy':True, 'firm':False, 'household': False},
                  parameter_names=parameter_names,
                  parameter_ranges=parameter_ranges,
                  parameter_increments=parameter_increments,
                  variables=variables,
                  other_parameters_=other_parameters,
                  iterations_=iterations_parameters,
                  cores_=cores_parameters,
                  model_name='network_economy',
                  model_url='url',
                  paper_name='covid2',
                  paper_url='url')  

def idiosyncratic1():
  transientTimeSteps = 200
  timeSteps = 100
  pVariables = {'economy': ['welfare_mean','links_changed','sumOutput'],'firm': None,'household':None}
  #pVariables = {'economy': ['gdp_equilibrium_prices','sum_output_equilibrium_prices'],'firm': None,'household':None}

  baseOthers={#'network_type': 'powerlaw',
              'network_type': 'SF',
              'd':5,
              'powerlaw_exponent':1.61,
              'network_from_file': False,
              'household_preference_homogeneous': False,
              'representative_household': True,
              'transient_time_steps': transientTimeSteps,
              'time_steps': timeSteps,
              #'stochastic_productivity': True,
              'stochastic_productivity': False,
              'firm_productivity_homogeneous':False,
              'productivity_multiplicative':False,
              'alpha':0.3,
              'productivity_labor':False,
              'production_function':'CD',
              'sigma':0.2,
              'rewiring_endogenous':True,
              'productivity_sigma':0.01,
              'probability_firm_input_seller_change':0.01,
              'probability_firm_entry':0.1}

  iterations = 10
  cores = 10

  num_firms = [10,100,1000,10000]
  

 

  parameter_names = ['n']
  parameter_ranges = {'n':None}
  parameter_increments = {'n':num_firms}
  variables = {'n':pVariables}
  other_parameters = {'n':baseOthers}
  iterations_parameters = {'n':iterations}
  cores_parameters = {'n':cores}
  network = 'powerlaw'
  fun = 'endo1'


  main.main(network_name = network, production_function=fun,  time_series={'economy':True, 'firm':False, 'household': False},
                  parameter_names=parameter_names,
                  parameter_ranges=parameter_ranges,
                  parameter_increments=parameter_increments,
                  variables=variables,
                  other_parameters_=other_parameters,
                  iterations_=iterations_parameters,
                  cores_=cores_parameters,
                  model_name='network_economy',
                  model_url='url',
                  paper_name='realShocks',
                  paper_url='url')

def idiosyncratic2():
  transientTimeSteps = 200
  timeSteps = 100
  pVariables = {'economy': ['welfare_mean','links_changed','sumOutput'],'firm': None,'household':None}
  #pVariables = {'economy': ['gdp_equilibrium_prices','welfare_mean','sum_output_equilibrium_prices'],'firm': None,'household':None}

  baseOthers={#'network_type': 'powerlaw',
              'network_type': 'SF',
              'd':5,
              'powerlaw_exponent':1.61,
              'network_from_file': False,
              'household_preference_homogeneous': False,
              'representative_household': True,
              'transient_time_steps': transientTimeSteps,
              'time_steps': timeSteps,
              #'stochastic_productivity': True,
              'stochastic_productivity': False,
              'firm_productivity_homogeneous':False,
              'productivity_multiplicative':False,
              'alpha':0.3,
              'productivity_labor':False,
              'production_function':'CD',
              'sigma':0.2,
              'rewiring_endogenous':True,
              'productivity_sigma':0.01,
              'probability_firm_input_seller_change':0.01,
              'probability_firm_entry':0.1}

  iterations = 10
  cores = 10

  
  num_firms = [100000]


  parameter_names = ['n']
  parameter_ranges = {'n':None}
  parameter_increments = {'n':num_firms}
  variables = {'n':pVariables}
  other_parameters = {'n':baseOthers}
  iterations_parameters = {'n':iterations}
  cores_parameters = {'n':cores}
  network = 'powerlaw'
  fun = 'endo2'


  main.main(network_name = network, production_function=fun,  time_series={'economy':True, 'firm':False, 'household': False},
                  parameter_names=parameter_names,
                  parameter_ranges=parameter_ranges,
                  parameter_increments=parameter_increments,
                  variables=variables,
                  other_parameters_=other_parameters,
                  iterations_=iterations_parameters,
                  cores_=cores_parameters,
                  model_name='network_economy',
                  model_url='url',
                  paper_name='realShocks',
                  paper_url='url')

def idiosyncratic3():
  transientTimeSteps = 200
  timeSteps = 100
  pVariables = {'economy': ['welfare_mean','links_changed','sumOutput'],'firm': None,'household':None}
  #pVariables = {'economy': ['gdp_equilibrium_prices','welfare_mean','sum_output_equilibrium_prices','sumOutput'],'firm': None,'household':None}

  baseOthers={#'network_type': 'powerlaw',
              'network_type': 'SF',
              'd':5,
              'powerlaw_exponent':1.61,
              'network_from_file': False,
              'household_preference_homogeneous': False,
              'representative_household': True,
              'transient_time_steps': transientTimeSteps,
              'time_steps': timeSteps,
              #'stochastic_productivity': True,
              'stochastic_productivity': False,
              'firm_productivity_homogeneous':False,
              'productivity_multiplicative':False,
              'alpha':0.3,
              'productivity_labor':False,
              'production_function':'CD',
              'sigma':0.2,
              'rewiring_endogenous':True,
              'productivity_sigma':0.01,
              'probability_firm_input_seller_change':0.01,
              'probability_firm_entry':0.1}

  iterations = 1
  cores = 1

  num_firms = [1000000]


  parameter_names = ['n']
  parameter_ranges = {'n':None}
  parameter_increments = {'n':num_firms}
  variables = {'n':pVariables}
  other_parameters = {'n':baseOthers}
  iterations_parameters = {'n':iterations}
  cores_parameters = {'n':cores}
  network = 'powerlaw'
  fun = 'endo3'


  main.main(network_name = network, production_function=fun,  time_series={'economy':True, 'firm':False, 'household': False},
                  parameter_names=parameter_names,
                  parameter_ranges=parameter_ranges,
                  parameter_increments=parameter_increments,
                  variables=variables,
                  other_parameters_=other_parameters,
                  iterations_=iterations_parameters,
                  cores_=cores_parameters,
                  model_name='network_economy',
                  model_url='url',
                  paper_name='realShocks',
                  paper_url='url')

def idiosyncratic3A():
  transientTimeSteps = 100
  timeSteps = 500
  pVariables = {'economy': ['gdp_equilibrium_prices'],'firm': None,'household':None}

  baseOthers={'network_type': 'powerlaw',
              'powerlaw_exponent':1.61,
              'network_from_file': False,
              'household_preference_homogeneous': False,
              'representative_household': True,
              'transient_time_steps': transientTimeSteps,
              'time_steps': timeSteps,
              'stochastic_productivity': True,
              'firm_productivity_homogeneous':False}

  iterations = 5
  cores = 1

  num_firms = [1000000]


  parameter_names = ['n']
  parameter_ranges = {'n':None}
  parameter_increments = {'n':num_firms}
  variables = {'n':pVariables}
  other_parameters = {'n':baseOthers}
  iterations_parameters = {'n':iterations}
  cores_parameters = {'n':cores}
  network = 'powerlaw'
  fun = 'CD3A'


  main.main(network_name = network, production_function=fun,  time_series={'economy':True, 'firm':False, 'household': False},
                  parameter_names=parameter_names,
                  parameter_ranges=parameter_ranges,
                  parameter_increments=parameter_increments,
                  variables=variables,
                  other_parameters_=other_parameters,
                  iterations_=iterations_parameters,
                  cores_=cores_parameters,
                  model_name='network_economy',
                  model_url='url',
                  paper_name='realShocks',
                  paper_url='url')

def lockdown_productivity():
    transientTimeSteps = 100
    timeSteps = 100
    #pVariables = {'economy': ['gdp_equilibrium_prices','sum_output_equilibrium_prices','sectoral_output','unemployment_rate'],'firm': None,'household':None}
    pVariables = {'economy': ['gdp_equilibrium_prices','sum_output_equilibrium_prices','unemployment_rate'],'firm': None,'household':None}

    
    baseOthers={'network_from_file': True,
              'household_preference_homogeneous': False,
              'representative_household': True,
              'transient_time_steps': transientTimeSteps,
              'time_steps': timeSteps,
              'production_function' : 'CES',
              'set_household_expenditure_shares':True,
              'lockdown_as_productivity_shock': True, 
              'lockdown_productivity_begin':9,
              'lockdown_productivity_end':None,                                                       
              'lockdownShock_multiple':1,
              'lockdownShock_cutOff':0.9,
              'weights_dynamic':True,
              #'labor_mobility':0.1,
              'files_directory':'covid2Files'}

    iterations = 1
    cores = 1
    parameter_names = ['alpha']
    parameter_ranges = {'alpha':[0.25,0.26]}
    parameter_increments = {'alpha':0.1}
    variables = {'alpha':pVariables}
    other_parameters = {'alpha':baseOthers}
    iterations_parameters = {'alpha':iterations}
    cores_parameters = {'alpha':cores}
    network = 'networkUS70'
    fun = 'CES_lockdownProductivityEquilibrium'

    main.main(network_name = network, production_function=fun,  time_series={'economy':True, 'firm':False, 'household': False},
                  parameter_names=parameter_names,
                  parameter_ranges=parameter_ranges,
                  parameter_increments=parameter_increments,
                  variables=variables,
                  other_parameters_=other_parameters,
                  iterations_=iterations_parameters,
                  cores_=cores_parameters,
                  model_name='network_economy',
                  model_url='url',
                  paper_name='realShocks',
                  paper_url='url')

def lockdown():

    transientTimeSteps = 100
    timeSteps = 100
    pVariables = {'economy': ['gdp_equilibrium_prices','sum_output_equilibrium_prices','gdp_fixed_shareSec_equilibrium_prices',
    'unemployment_rate','intermediate_share','inventory_prop','consumer_demand','intermediate_demand'],'firm': None,'household':None}

    baseOthers={
              'network_from_file': True,
              'household_preference_homogeneous': False,
              'representative_household': True,
              'transient_time_steps': transientTimeSteps,
              'time_steps': timeSteps,
              'lockdown_timeSteps': [20,21,22,23,24,25,26],
              #'record_intermediate_share_deviation_time_steps':[58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96],
              'production_function' : 'CES',
              'set_household_expenditure_shares':True,
              'lockdown': True,              
              'labor_mobility':0.1,
              'sigma':-1,
              'weights_dynamic':True,
              'lockdownShock_multiple':1,
              'lockdown_cutOff':0.9,
              'lockdown_inventory_share':1,
              'price_stickiness_type': 'linear',
              'linear_price_stickiness_old_share':0.9,
              'files_directory':'covid2Files'}


    iterations = 1
    cores = 1

    others_priceStick = copy.deepcopy(baseOthers)
    del others_priceStick['linear_price_stickiness_old_share']

    others_labor_mobility = copy.deepcopy(baseOthers)
    del others_labor_mobility['labor_mobility']

    others_sigma = copy.deepcopy(baseOthers)
    del others_sigma['sigma']

    others_lockdownShock_multiple = copy.deepcopy(baseOthers)
    del others_lockdownShock_multiple['lockdownShock_multiple']

    others_prob_price_change =  copy.deepcopy(baseOthers)
    del others_prob_price_change['price_stickiness_type']
    others_prob_price_change['endogenous_probability_price_change'] = True

   
    parameter_ranges = {'labor_mobility':[0.05,0.51],'sigma':[-1.5,-0.09],'lockdownShock_multiple':[0.7,1.31],'linear_price_stickiness_old_share':[0.0,0.97],'price_sensitivity':[0.05,0.51]}
    #parameter_ranges = {'linear_price_stickiness_old_share':[0.3,0.91]}
    
    parameter_increments = {'labor_mobility':0.1,'sigma':0.1,'lockdownShock_multiple':0.1,'linear_price_stickiness_old_share':0.05,'price_sensitivity':0.05}
    #parameter_increments = {'linear_price_stickiness_old_share':0.3}
    variables = {'labor_mobility':pVariables,'sigma':pVariables,'lockdownShock_multiple':pVariables,'linear_price_stickiness_old_share':pVariables,'price_sensitivity':pVariables}
    other_parameters = {'labor_mobility':others_labor_mobility,'sigma':others_sigma,'lockdownShock_multiple':others_lockdownShock_multiple,'linear_price_stickiness_old_share':others_priceStick,'price_sensitivity':others_priceStick}
    iterations_parameters = {'labor_mobility':iterations,'sigma':iterations,'lockdownShock_multiple':iterations,'linear_price_stickiness_old_share':iterations,'price_sensitivity':iterations}
    cores_parameters = {'labor_mobility':cores,'sigma':cores,'lockdownShock_multiple':cores,'linear_price_stickiness_old_share':cores,'price_sensitivity':cores}
 
    network = 'networkUS70'
    fun = 'CES'
    

    parameter_names = ['labor_mobility']
    #parameter_names = ['sigma']
    #parameter_names = ['lockdownShock_multiple']
    #parameter_names = ['linear_price_stickiness_old_share']
    #parameter_names = ['price_sensitivity']
    
    
    main.main(network_name = network, production_function=fun,  time_series={'economy':True, 'firm':False, 'household': False},
                  parameter_names=parameter_names,
                  parameter_ranges=parameter_ranges,
                  parameter_increments=parameter_increments,
                  variables=variables,
                  other_parameters_=other_parameters,
                  iterations_=iterations_parameters,
                  cores_=cores_parameters,
                  model_name='network_economy',
                  model_url='url',
                  paper_name='covid2',
                  paper_url='url')
    """

    parameter_ranges = {'labor_mobility':[1,1.1]}
    parameter_increments = {'labor_mobility':1.0}
    variables = {'labor_mobility':pVariables}
    other_parameters = {'labor_mobility':others_labor_mobility}
    iterations_parameters = {'labor_mobility':iterations}
    cores_parameters = {'labor_mobility':cores}

    network = 'networkUS70'
    fun = 'CES_laborMobFull'

    parameter_names = ['labor_mobility']

    main.main(network_name = network, production_function=fun,  time_series={'economy':True, 'firm':False, 'household': False},
                parameter_names=parameter_names,
                parameter_ranges=parameter_ranges,
                parameter_increments=parameter_increments,
                variables=variables,
                other_parameters_=other_parameters,
                iterations_=iterations_parameters,
                cores_=cores_parameters,
                model_name='network_economy',
                model_url='url',
                paper_name='covid2',
                paper_url='url')
    """

def lockdown_pickle(priceStickiness,record_dist):
  print priceStickiness, record_dist
  parameters = p.Parameters()
  parameters.data_time_series['economy'] = True
  variables = ['gdp_equilibrium_prices','sum_output_equilibrium_prices','unemployment_rate',
  'intermediate_share','intermediate_share_currentOutput','inventory_prop',
  'consumer_demand','gdp_fixed_shareSec_equilibrium_prices',
  'intermediate_demand','priceLevel','wealth','gdp','sumOutput']
  

  for var in variables:
    parameters.record_variables['economy'][var] = True

  if record_dist:
    parameters.record_variables['economy']['intermediate_share_firms'] = True



  parameters.transient_time_steps = 100
  parameters.time_steps = 130
  parameters.network_from_file = True
  parameters.household_preference_homogeneous = False
  parameters.representative_household = True
  parameters.lockdown_timeSteps = [60,61,62,63,64,65,66]
  #parameters.lockdown_timeSteps = [10,11,12,13,14,15,16]
  #parameters.lockdown_timeSteps = [20,21,22,23,24,25,26]
  #parameters.record_intermediate_share_deviation_time_steps = [58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96]
  parameters.production_function = 'CES'
  parameters.set_household_expenditure_shares = True
  parameters.lockdown = True
  parameters.labor_mobility = 0.1
  parameters.sigma = -1
  parameters.weights_dynamic = True
  parameters.lockdownShock_multiple = 1
  parameters.lockdown_cutOff = 0.9
  parameters.lockdown_inventory_share = 1
  parameters.price_stickiness_type = 'linear'
  parameters.linear_price_stickiness_old_share = priceStickiness
  parameters.files_directory = 'covid2Files'



  run_instance = run.Run(parameters)
  run_instance.create_economy()
  run_instance.transient()
  run_instance.time_steps()
  economy = run_instance.samayam.economy


  directory_name = 'data' + '_' + 'covid2'
  if not os.path.exists(directory_name):
      os.makedirs(directory_name)
  os.chdir(directory_name)

  file_name = str(parameters.linear_price_stickiness_old_share) + 'priceStickiness'
  with open('%s.cPickle' % file_name, 'wb') as econ:
      cPickle.dump(economy, econ, protocol=cPickle.HIGHEST_PROTOCOL)
  os.chdir('..')


#lockdown_pickle(priceStickiness=0.3,record_dist=False)
#lockdown_pickle(priceStickiness=0.6,record_dist=False)
#lockdown_pickle(priceStickiness=0.9,record_dist=False)

#lockdown_productivity()
lockdown()

#one_time_productivity()
#firms_one_time_productivity()

#idiosyncratic1()
#idiosyncratic2()
#idiosyncratic3()

#print "done first half"
#idiosyncratic3A()

#convergence()



