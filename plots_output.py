from __future__ import division
import pandas as pd
import matplotlib.pyplot as plt
import random as random
import numpy as np
from math import log
import math
import ast
from itertools import chain
from scipy import stats
from matplotlib.ticker import FormatStrFormatter
import collections
import os
import csv
import plots
import cPickle




files = ['equiFinOutput_linear_price_stickiness_old_share_economy_network_linear_CD_smallTheta_negative.csv',
    'equiFinOutput_linear_price_stickiness_old_share_economy_network_linear_CD_smallTheta_positive.csv',
    'equiFinOutput_monetaryShock_exponent_economy_network_False_CD_smallTheta_negative.csv',
    'equiFinOutput_monetaryShock_exponent_economy_network_False_CD_smallTheta_positive.csv',
    'equiFinOutput_s_economy_network_False_CD_largeTheta_negative.csv',
    'equiFinOutput_s_economy_network_False_CD_smallTheta_negative.csv',
    'equiFinOutput_s_economy_network_linear_CD_smallTheta_negative.csv']

def flexible():
    fontSize_supTitle = 20
    fontSize_xlabel = 16
    fontSize_ylabel = 16
    fontSize_ticks = 12

    csv_file = 'SR_equiFinOutput_s_economy_network_False_CD_largeTheta_negative.csv'
    data = pd.read_csv(csv_file)
    data = pd.DataFrame(data)
    parameter_values = []
    output_values = []
    rows = data.shape[0]
    
    valuesDict = {}
    for r in xrange(rows):
        pVal = data.iloc[r][0]
        out = data.iloc[r][1]
        parameter_values.append(pVal)
        output_values.append(out)    
        if pVal in valuesDict:
            valuesDict[pVal].append(out)
        else:
            valuesDict[pVal] = [out]

    params = []
    avgValues = []
    
    ks = valuesDict.keys()
    ks.sort()


    for pVal in ks:
        mean = np.mean(valuesDict[pVal])
        params.append(pVal)
        avgValues.append(mean)

    plt.scatter(params,avgValues,color='cornflowerblue',label=r'$\theta=1.1$',marker='*')
    plt.plot(params,avgValues,color='cornflowerblue',linestyle='dotted')

    csv_file = 'SR_equiFinOutput_s_economy_network_False_CD_smallTheta_negative.csv'
    data = pd.read_csv(csv_file)
    data = pd.DataFrame(data)
    parameter_values = []
    output_values = []
    rows = data.shape[0]
   
    valuesDict = {}
    for r in xrange(rows):
        pVal = data.iloc[r][0]
        out = data.iloc[r][1]
        parameter_values.append(pVal)
        output_values.append(out)        
        if pVal in valuesDict:
            valuesDict[pVal].append(out)
        else:
            valuesDict[pVal] = [out]

    params = []
    avgValues = []

    ks = valuesDict.keys()
    ks.sort()

    for pVal in ks:
        mean = np.mean(valuesDict[pVal])
        params.append(pVal)
        avgValues.append(mean)

    plt.scatter(params,avgValues,color='black',label=r'$\theta=0.9$',marker='*')
    plt.plot(params,avgValues,color='black',linestyle='dotted')
    
    
    baseTitle = 'Variation in the size of shocks'
    baseXLabel = 'Size of shock (proportion)'
    baseYLabel = 'Change in GDP (proportion)'
    child_directory = 'plots_output'
    if not os.path.exists(child_directory):
        os.makedirs(child_directory)
    os.chdir(child_directory)
    plt.suptitle(baseTitle, fontsize=fontSize_supTitle)
    plt.xlabel(baseXLabel, fontsize=fontSize_xlabel)
    plt.ylabel(baseYLabel, fontsize=fontSize_ylabel)
    plt.tick_params(axis='both', labelsize=fontSize_ticks)
    plt.ticklabel_format(style='plain',axis='both',useOffset=False)
    plt.legend(loc=2,fontsize=fontSize_xlabel)
    plt.grid()
    plt.xlim([-0.0015,0.0015])
    plt.ylim([-0.0001,0.0001])
    saveName =  'sizeShock_flexible'
    plt.savefig(saveName, bbox_inches='tight')
    plt.close()
    os.chdir('..')   
        
def stickiness_linear():
    fontSize_supTitle = 20
    fontSize_xlabel = 16
    fontSize_ylabel = 16
    fontSize_ticks = 12

    csv_file = 'SR_equiFinOutput_s_economy_network_linear_CD_smallTheta_negative.csv'
    data = pd.read_csv(csv_file)
    data = pd.DataFrame(data)
    parameter_values = []
    output_values = []
    rows = data.shape[0]
    
    valuesDict = {}
    for r in xrange(rows):
        pVal = data.iloc[r][0]
        out = data.iloc[r][1]
        parameter_values.append(pVal)
        output_values.append(out)    
        if pVal in valuesDict:
            valuesDict[pVal].append(out)
        else:
            valuesDict[pVal] = [out]

    params = []
    avgValues = []

    ks = valuesDict.keys()
    ks.sort()

    for pVal in ks:
        mean = np.mean(valuesDict[pVal])
        params.append(pVal)
        avgValues.append(mean)

    plt.scatter(params,avgValues,color='black',label=r'$\theta=0.9$',marker='*')
    plt.plot(params,avgValues,color='black',linestyle='dotted')

    
    baseTitle = 'Variation in the size of shocks'
    baseXLabel = 'Size of shock (proportion)'
    baseYLabel = 'Change in GDP (proportion)'
    child_directory = 'plots_output'
    if not os.path.exists(child_directory):
        os.makedirs(child_directory)
    os.chdir(child_directory)
    plt.suptitle(baseTitle, fontsize=fontSize_supTitle)
    plt.xlabel(baseXLabel, fontsize=fontSize_xlabel)
    plt.ylabel(baseYLabel, fontsize=fontSize_ylabel)
    plt.tick_params(axis='both', labelsize=fontSize_ticks)
    plt.ticklabel_format(style='plain',axis='both',useOffset=False)
    plt.legend(loc=2,fontsize=fontSize_xlabel)
    plt.grid()
    plt.xlim([-0.0015,0.0015])
    plt.ylim([-0.00015,0.00015])
    saveName =  'sizeShock_sticky_linear'
    plt.savefig(saveName, bbox_inches='tight')
    plt.close()
    os.chdir('..')

def asymmetry():
    fontSize_supTitle = 20
    fontSize_xlabel = 16
    fontSize_ylabel = 16
    fontSize_ticks = 12

    csv_file = 'SR_equiFinOutput_linear_price_stickiness_old_share_economy_network_linear_CD_smallTheta_positive.csv'
    data = pd.read_csv(csv_file)
    data = pd.DataFrame(data)
    parameter_values = []
    output_values = []
    rows = data.shape[0]

    valuesDict = {}
    for r in xrange(rows):
        pVal = data.iloc[r][0]
        out = data.iloc[r][1]
        parameter_values.append(pVal)
        output_values.append(out)    
        if pVal in valuesDict:
            valuesDict[pVal].append(out)
        else:
            valuesDict[pVal] = [out]


    params = []
    avgValues_positive = []

    ks = valuesDict.keys()
    ks.sort()
    
    for pVal in ks:
        mean = np.mean(valuesDict[pVal])
        params.append(pVal)
        avgValues_positive.append(mean)

    #plt.scatter(params,avgValues,color='cornflowerblue',label=r'Positive shock $(s=0.01)$',marker='*')

    csv_file = 'SR_equiFinOutput_linear_price_stickiness_old_share_economy_network_linear_CD_smallTheta_negative.csv'
    data = pd.read_csv(csv_file)
    data = pd.DataFrame(data)
    parameter_values = []
    output_values = []
    rows = data.shape[0]

    valuesDict = {}
    for r in xrange(rows):
        pVal = data.iloc[r][0]
        out = data.iloc[r][1]
        parameter_values.append(pVal)
        output_values.append(out)    
        if pVal in valuesDict:
            valuesDict[pVal].append(out)
        else:
            valuesDict[pVal] = [out]

    params = []
    avgValues_negative = []
    
    ks = valuesDict.keys()
    ks.sort()
    
    for pVal in ks:
        mean = np.mean(valuesDict[pVal])
        params.append(pVal)
        avgValues_negative.append(mean)


    yVals = []

    for i in xrange(len(avgValues_negative)):
        pos = avgValues_positive[i]
        neg = avgValues_negative[i]
        diff =  abs(neg)/abs(pos)
        yVals.append(diff)


    #plt.scatter(params,avgValues,color='black',label=r'Negative shock ($s=-0.01$)',marker='*')
    plt.scatter(params,yVals,color='black',label=r'Ratio $\frac{|negative|}{positive}$',marker='*')
    plt.plot(params,yVals,color='black',linestyle='dotted')

    baseTitle = 'Asymmetry in the effect of shocks'
    baseXLabel = r'Price stickiness (share of old price $\gamma$)'
    baseYLabel = r'GDP response ratio'
    child_directory = 'plots_output'
    if not os.path.exists(child_directory):
        os.makedirs(child_directory)
    os.chdir(child_directory)
    plt.suptitle(baseTitle, fontsize=fontSize_supTitle)
    plt.xlabel(baseXLabel, fontsize=fontSize_xlabel)
    plt.ylabel(baseYLabel, fontsize=fontSize_ylabel)
    plt.tick_params(axis='both', labelsize=fontSize_ticks)
    plt.ticklabel_format(style='plain',axis='both',useOffset=False)
    plt.legend(loc=2,fontsize=fontSize_xlabel)
    plt.grid()
    #plt.xlim([-0.0015,0.0015])
    #plt.ylim([-0.00015,0.00015])
    saveName =  'asymmetry_sticky_linear'
    plt.savefig(saveName, bbox_inches='tight')
    plt.close()
    os.chdir('..')

def theta():
    fontSize_supTitle = 20
    fontSize_xlabel = 16
    fontSize_ylabel = 16
    fontSize_ticks = 12

    csv_file = 'SR_equiFinOutput_monetaryShock_exponent_economy_network_False_CD_smallTheta_negative.csv'
    data = pd.read_csv(csv_file)
    data = pd.DataFrame(data)
    parameter_values = []
    output_values = []
    rows = data.shape[0]

    valuesDict = {}
    for r in xrange(rows):
        pVal = data.iloc[r][0]
        out = data.iloc[r][1]
        parameter_values.append(pVal)
        output_values.append(out)    
        if pVal in valuesDict:
            valuesDict[pVal].append(out)
        else:
            valuesDict[pVal] = [out]

    params = []
    avgValues = []

    ks = valuesDict.keys()
    ks.sort()

    for pVal in ks:
        mean = np.mean(valuesDict[pVal])
        params.append(pVal)
        avgValues.append(mean)

    plt.scatter(params,avgValues,color='black',label='Negative shock (s=-0.001)',marker='*')
    plt.plot(params,avgValues,color='black',linestyle='dotted')

    csv_file = 'SR_equiFinOutput_monetaryShock_exponent_economy_network_False_CD_smallTheta_positive.csv'
    data = pd.read_csv(csv_file)
    data = pd.DataFrame(data)
    parameter_values = []
    output_values = []
    rows = data.shape[0]

    valuesDict = {}
    for r in xrange(rows):
        pVal = data.iloc[r][0]
        out = data.iloc[r][1]
        parameter_values.append(pVal)
        output_values.append(out)    
        if pVal in valuesDict:
            valuesDict[pVal].append(out)
        else:
            valuesDict[pVal] = [out]

    params = []
    avgValues = []

    ks = valuesDict.keys()
    ks.sort()

    for pVal in ks:
        mean = np.mean(valuesDict[pVal])
        params.append(pVal)
        avgValues.append(mean)

    plt.scatter(params,avgValues,color='cornflowerblue',label='Positive shock (s=0.001)',marker='*')
    plt.plot(params,avgValues,color='cornflowerblue',linestyle='dotted')
        
    
    baseTitle = 'Variation in the impact of shocks'
    baseXLabel = r'$\theta$'
    baseYLabel = 'Change in GDP (proportion)'
    child_directory = 'plots_output'
    if not os.path.exists(child_directory):
        os.makedirs(child_directory)
    os.chdir(child_directory)
    plt.suptitle(baseTitle, fontsize=fontSize_supTitle)
    plt.xlabel(baseXLabel, fontsize=fontSize_xlabel)
    plt.ylabel(baseYLabel, fontsize=fontSize_ylabel)
    plt.tick_params(axis='both', labelsize=fontSize_ticks)
    plt.ticklabel_format(style='plain',axis='both',useOffset=False)
    plt.legend(loc=2,fontsize=fontSize_xlabel)
    plt.grid()
    #plt.xlim([-0.0015,0.0015])
    plt.ylim([-0.001,0.001])
    saveName =  'theta_flexible'
    plt.savefig(saveName, bbox_inches='tight')
    plt.close()
    os.chdir('..')

def timeSeries():
    fontSize_supTitle = 20
    fontSize_xlabel = 20
    fontSize_ylabel = 20
    fontSize_ticks = 16
    
   
    colors = {'finalOutput': 'black'}
    markers = {'finalOutput': 'o'}
    labels = {'finalOutput': 'Output'}

    #signs = ['positive', 'negative']
    signs = ['positive']
    #theta = ['largeTheta', 'smallTheta']
    theta = ['smallTheta']

    #stickiness = ['False','linear']
    stickiness = ['False']
    color = 'royalblue'
    for s in signs:
      for t in theta:
          for stick in stickiness:
            file = 'economyPickle_' + s + '_' + t + '_' + 'CD' + '_' + stick + '.cPickle'
            E = cPickle.load(open(file, "rb"))
            #finalOutput = getattr(E, 'finalOutput_equilibrium_prices')
            finalOutput = getattr(E, 'finalOutput')
            #finalOutput = getattr(E, 'finalOutput_consumer_equilibrium_prices')
            
            #finalOutput = getattr(E, 'welfare_mean')
            
            
            #finalOutput = getattr(E, 'mean_price_change')
            #finalOutput = getattr(E, 'eta')
            #finalOutput = getattr(E, 'PCE')
            
            
            
            #finalOutput = getattr(E, 'sumOutput')
            #finalOutput = getattr(E, 'wealth')
            finalOutput = finalOutput[60:]
            print finalOutput[-1]
            #normfinalOutput = finalOutput[0]
            #normfinalOutput = np.mean(finalOutput)
            #print finalOutput

            begin=1
            end=27
            minus = 6
            #timeSteps = range(begin-minus,end-minus)
            #finalOutput = [i/normfinalOutput for i in finalOutput]            
            #finalOutput = finalOutput[begin:end]
            #finalOutput = finalOutput[begin:end]
            timeSteps = range(0,len(finalOutput))
            plt.scatter(timeSteps, finalOutput, color=color)#, label=lab, marker=marker)
            plt.plot(timeSteps, finalOutput, color=color, linestyle='-')

            baseTitle = 'Time Series'
            baseXLabel = 'Quarters'
            baseYLabel = 'GDP (normalized)'
            child_directory = 'plots_output'
            if not os.path.exists(child_directory):
              os.makedirs(child_directory)
            os.chdir(child_directory)
            plt.suptitle(baseTitle, fontsize=fontSize_supTitle)
            plt.xlabel(baseXLabel, fontsize=fontSize_xlabel)
            plt.ylabel(baseYLabel, fontsize=fontSize_ylabel)
            plt.tick_params(axis='both', labelsize=fontSize_ticks)
            plt.ticklabel_format(style='plain',axis='both',useOffset=False)
            plt.legend(loc=1,fontsize=fontSize_xlabel)
            plt.grid()
            #plt.ylim([0.9999,1.0001])

            saveName =  'timeSeries_' + s + t + stick + '.png'
            plt.savefig(saveName, bbox_inches='tight')
            plt.close()
            os.chdir('..')    

def all():
    child_directory = 'data_output'

    if not os.path.exists(child_directory):
        os.makedirs(child_directory)
    os.chdir(child_directory)
    timeSeries()
    #flexible()
    #stickiness_linear()
    #asymmetry()
    #theta()
    os.chdir('..')

all()


