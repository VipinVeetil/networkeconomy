from __future__ import division
import pandas as pd
import matplotlib.pyplot as plt
import random as random
import numpy as np
from math import log
import math
import ast
from itertools import chain
from scipy import stats
from matplotlib.ticker import FormatStrFormatter
import collections
import os
import csv
import plots
import cPickle
import pandas as pd
import ast
import copy
import powerlaw
import matplotlib.ticker as ticker

def write_size_degree_dat(num_firms):
	file_name = "size_distER" + str(num_firms) + ".txt"
	with open(file_name,'r') as file:
		for line in file:
			dist_size = ast.literal_eval(line)


	with open("size.dat",'w') as file:
		for element in dist_size[-1]:
			file.write(str(element)+ '\n')



	file_name = "out_distER" + str(num_firms) + ".txt"
	with open(file_name,'r') as file:
		for line in file:
			dist_deg = ast.literal_eval(line)

	with open("degree.dat",'w') as file:
		for element in dist_deg[-1]:
			file.write(str(element)+ '\n')

def heat_map():
	labelFont = 20
	titleFont = 20
	ticksFont = 18
	

	plots.heat_map_simple(csv_file='multi_gdp_multiParam_linear_price_stickiness_old_shareprobability_firm_input_seller_change_economy_SF_endo100000.csv',
                          x_var='linear_price_stickiness_old_share',
                          y_var='probability_firm_input_seller_change',
                          color_var='vol',
                          fontSize_xlabel=labelFont,
                          fontSize_ylabel=labelFont,
                          fontSize_title=titleFont,
                          fontSize_ticks=ticksFont,
                          xlabel=r'Price stickiness $\tau$',
                          ylabel=r'Probability link change $\rho$',
                          title=r'Aggregate volatility ($\tau$ vs. $\rho$)',
                          child_directory='plots_realShocks',
                          yticks_increments=0.1,
                          xticks_increments=0.1,
                          plus_x_tick=False,
                          plus_y_tick=False,
                          colorbar=True)

	plots.heat_map_simple(csv_file='multi_gdp_multiParam_dprobability_firm_input_seller_change_economy_SF_endo100000.csv',
                          x_var='d',
                          y_var='probability_firm_input_seller_change',
                          color_var='vol',
                          fontSize_xlabel=labelFont,
                          fontSize_ylabel=labelFont,
                          fontSize_title=titleFont,
                          fontSize_ticks=ticksFont,
                          xlabel=r'Mean degree $d$',
                          ylabel=r'Probability link change $\rho$',
                          title=r'Aggregate volatility ($d$ vs. $\rho$)',
                          child_directory='plots_realShocks',
                          yticks_increments=0.1,
                          xticks_increments=2,
                          plus_x_tick=True,
                          plus_y_tick=False,
                          colorbar=True)
    
def write_US_gdpGrowth():
	data = pd.read_csv("us_gdp_growth_fed.csv")
	data = pd.DataFrame(data)
	data = data["A191RL1A225NBEA"]
	data = list(data)
	data = data[:]
	with open("us_gdp_growth.dat",'w') as file:
		for d in data:
			file.write(str(d)+ '\n')

def write_USsizes():
	sizes_dict = collections.OrderedDict({})
	with open("US_firmSize.txt",'r') as file:
		for line in file:
			line = line.split(",")
			line = [int(i) for i in line]
			tup = (line[0], line[1])
			s = line[-1]
			sizes_dict[tup] = s
	
	size_distribution = []

	for tup in sizes_dict:
		begin = tup[0]
		begin = max(1,begin)
		end = tup[1]
		vals = sizes_dict[tup]
		diff = (end-begin)/vals
		count = 0
		for n in xrange(vals):
			s = begin + count * diff
			size_distribution.append(s)
			count += 1

	with open('sizesUS.dat','w') as file:
		for s in size_distribution:
			file.write(str(s)+ '\n')

def secondOrder_in_degree(networkName):
    allFirms = []
    firstOrder = {}
    with open(networkName) as data:
        for line in data:
            numbers = line.split(",")
            numbers = [str(int(i)) for i in numbers]
            allFirms += numbers
            buyer = numbers[0]
            sellers = numbers[1:]
            firstOrder[buyer] = sellers


    allFirms = list(set(allFirms))

    secondOrder = {}

    for firm in firstOrder:    
        count = 0
        sellers = firstOrder[firm] 
        for s in sellers:
            second_sellers = firstOrder[s]
            deg = len(second_sellers)
            count += deg
            if count > 0:
                secondOrder[firm] = count

    dist = list(secondOrder.values())
    dist = sorted(dist)
    return dist

def secondOrder_out_degree(networkName):
    network = {}
    with open(networkName) as data:
        for line in data:
            line = line.split(",")
            buyer = int(line[0])
            sellers = line[1:]
            sellers = [int(i) for i in sellers]
            network[buyer] = sellers

    outNetwork = {}
    for firm in network:
        outNetwork[firm] = []
        sellers = network[firm]
        for s in sellers:
            outNetwork[s] = []



    for buyer in network:
        sellers = network[buyer]
        for s in sellers:
            outNetwork[s].append(buyer)

    for firm in outNetwork:
        buyers = outNetwork[firm]
        buyers = list(set(buyers))
        outNetwork[firm] = buyers


    second_deg = {}

    for firm in outNetwork:
        buyers = outNetwork[firm]
        count = 0
        for b in buyers:
            second_buyers = outNetwork[b]
            count += len(second_buyers)
        if count > 0:
            second_deg[firm] = count

    vals = sorted(second_deg.values())
    return vals    

def computeOutDegree(addOne,networkName):
    network = {}
    with open(networkName) as data:
        for line in data:
            line = line.split(",")
            buyer = int(line[0])
            sellers = line[1:]
            sellers = [int(i) for i in sellers]
            network[buyer] = sellers

    outNetwork = {}
    for firm in network:
        outNetwork[firm] = []
        sellers = network[firm]
        for s in sellers:
            outNetwork[s] = []



    for buyer in network:
        sellers = network[buyer]
        for s in sellers:
            outNetwork[s].append(buyer)

    for firm in outNetwork:
        buyers = outNetwork[firm]
        buyers = list(set(buyers))
        outNetwork[firm] = buyers

    outDeg = []

    for firm in outNetwork:
        buyers = outNetwork[firm]
        if addOne:
            deg = len(buyers) + 1
            outDeg.append(deg)
        else:
            deg = len(buyers)
            if deg > 0:
                outDeg.append(deg)
    return outDeg

def deg_dist_US(first_second,labelFont,titleFont,ticksFont):	
	labelFont += 8
	titleFont += 6
	ticksFont += 8

	exp = 2
	max_s = 10000
	min_s = 1

	
	network = {}
	with open("network.txt",'r') as file:
		for line in file:
			line = line.split(",")
			line = [int(i) for i in line]
			buyer = line[0]
			sellers = line[1:]
			network[buyer] = sellers



	if first_second == "first":
		out_dist = computeOutDegree(addOne=False,networkName="network.txt")
		in_dist = []
		for n in network:
			val = len(network[n])
			in_dist.append(val)

		with open("inUS.dat",'w') as file:
			for i in in_dist:
				file.write(str(i)+ '\n')

		print "done in file"

		with open("outUS.dat",'w') as file:
			for i in out_dist:
				file.write(str(i)+ '\n')
			



	elif first_second == "second":
		out_dist = secondOrder_out_degree("network.txt")
		in_dist = secondOrder_in_degree("network.txt")


	num_firms = len(in_dist)
	[firm_list,deg_list] = CCDF(list_sizes=in_dist,logV=exp,max_size=max_s,min_size=min_s)
	print max(deg_list), "max degree"
	firm_list = [i - math.log(num_firms,exp) for i in firm_list]
	plt.plot(deg_list,firm_list,color='salmon',label='In-degree',lw=2,marker='.',markersize=0.75)
	plt.scatter(deg_list, firm_list, color='salmon',marker='.',s=0.5)

	num_firms = len(out_dist)
	[firm_list,deg_list] = CCDF(list_sizes=out_dist,logV=exp,max_size=max_s,min_size=min_s)
	print max(deg_list), "max degree"
	firm_list = [i - math.log(num_firms,exp) for i in firm_list]
	plt.plot(deg_list,firm_list,color='royalblue',label='Out-degree',lw=2,marker='.',markersize=0.75)
	plt.scatter(deg_list, firm_list, color='royalblue',marker='.',s=0.5)
	


	plt.xlabel("Degree",fontsize=labelFont)
	plt.ylabel(r"Cumulative probability $p(X\geq x)$",fontsize=labelFont)
	plt.legend(loc=1,fontsize=labelFont-2)
	plt.ticklabel_format(style='plain',axis='both',useOffset=False,fontsize=ticksFont)

	if first_second == "first":
		plt.suptitle("First order degree distribution",fontsize=titleFont)
		y_tick_labels = {0:r'$2^0$',-4:r'$2^{-4}$',-8:r'$2^{-8}$',-12:r'$2^{-12}$',-16:r'$2^{-16}$'}
		x_tick_labels = {0:r"$2^0$",2:r"$2^2$",4:r"$2^4$",6:r"$2^6$",8:r"$2^8$",10:r"$2^{10}$"}
	elif first_second == "second":
		plt.suptitle("Second order degree distribution",fontsize=titleFont)
		y_tick_labels = {0:r'$2^0$',-5:r'$2^{-5}$',-10:r'$2^{-10}$',-15:r'$2^{-15}$',-20:r'$2^{-20}$'}
		x_tick_labels = {0:r"$2^0$",2:r"$2^2$",4:r"$2^4$",6:r"$2^6$",8:r"$2^8$",10:r"$2^{10}$",12:r"$2^{12}$"}

	plt.xticks(x_tick_labels.keys(), x_tick_labels.values(),fontsize=ticksFont)
	plt.yticks(y_tick_labels.keys(), y_tick_labels.values(),fontsize=ticksFont)
	plt.grid()

	save_name = first_second + "_" + "US_deg_dist.png"
	child_directory = 'plots_realShocks'
	if not os.path.exists(child_directory):
	    os.makedirs(child_directory)    
	os.chdir(child_directory)
	plt.savefig(save_name,bbox_inches='tight')
	plt.close()    
	os.chdir('..')

def goodness_fit():
	size = np.loadtxt('size.dat')
	size = list(size)

	degree = np.loadtxt('degree.dat')
	degree = list(degree)

	fit = powerlaw.Fit(size)
	print fit.power_law.alpha, "size fit"
	R,p = fit.distribution_compare('power_law', 'lognormal', normalized_ratio = True)
	print R,p, 'size'

	R,p = fit.distribution_compare('power_law', 'exponential', normalized_ratio = True)
	print R,p, 'size, exp'

	fit = powerlaw.Fit(degree)
	print fit.power_law.alpha, "degree fit"

	R,p = fit.distribution_compare('power_law', 'lognormal', normalized_ratio = True)
	print R,p, 'degree'

	R,p = fit.distribution_compare('power_law', 'exponential', normalized_ratio = True)
	print R,p, 'degree, exp'

def return_inflation(transient,num_firms):
	file_name = "wealthER" + str(num_firms) +".txt"
	with open(file_name,'r') as file:
		for line in file:
			wealth = ast.literal_eval(line)

	wealth = wealth[transient:]
	yearly_wealth = []

	for i in xrange(0,len(wealth),12):
		w = wealth[i]
		yearly_wealth.append(w)	
	
	inflation = []

	for i in xrange(len(yearly_wealth)-1):
		a = yearly_wealth[i]
		b = yearly_wealth[i+1]
		c = (b-a)/a
		inflation.append(c)
	return inflation

def tail_risk(transient,num_firms,labelFont,titleFont,ticksFont):	
	inflation = return_inflation(transient,num_firms)
	#print len(inflation), "length inflation"
	file_name = "gdpER" + str(num_firms) + ".txt"
	with open(file_name,'r') as file:
		for line in file:
			data = ast.literal_eval(line)

	data = data[transient:]
	print len(data), "num of data points"
	quarters = int(len(data)/3)
	quarterly_gdp = []
	for q in xrange(quarters):
		count = 0
		for i in xrange(3):
			k = q * 3 + i
			g = data[k]
			count += g
		quarterly_gdp.append(count)
	quarterly_changes = []

	print len(quarterly_gdp), "num of quarterly gdp"

	for i in xrange(len(quarterly_gdp)-1):
		a = quarterly_gdp[i]
		b = quarterly_gdp[i+1]
		c = (b-a)/a
		quarterly_changes.append(c)

	print len(inflation), "num of inflation"
	real_quarterly_change = []
	count = 0
	for i in xrange(1,len(quarterly_changes)):
		c = quarterly_changes[i-1]
		#if count < len(inflation):
		#	pi = inflation[count]
		#	temp = c-pi
		#real_quarterly_change.append(temp)				
		real_quarterly_change.append(c)	
		if i%4 == 0:
			count += 1


	print len(real_quarterly_change), "num of quarterly changes"

	with open('gdp_quarterlyChange.dat','w') as file:
		for i in real_quarterly_change:
			file.write(str(i)+ '\n')
	



	years = int(len(data)/12)
	yearly_gdp = []
	for y in xrange(years):
		count = 0
		for i in xrange(12):
			k = y*12 + i
			g = data[k]
			count+= g
		yearly_gdp.append(count)		
	changes = []
	for i in xrange(len(yearly_gdp)-1):
		a = yearly_gdp[i]
		b = yearly_gdp[i+1]
		c = (b-a)/a
		changes.append(c)

	real_changes = []

	for i in xrange(len(changes)):
		c = changes[i]
		pi = inflation[i]
		temp = c-pi
		real_changes.append(temp)
	
	std = np.std(real_changes)
	print std,"agg vol"

	real_changes = sorted(real_changes)

	print len(real_changes), "num yearly changes"

	with open('gdp_annualChange.dat','w') as file:
		for i in real_changes:
			file.write(str(i)+ '\n')



	#plt.hist(changes,bins=10,color='black')
	#plt.scatter(real_changes,[1]*len(real_changes))
	#plt.show()

def firm_volatility(transient,num_firms,labelFont,titleFont,ticksFont):
	file_name = "firm_volatilityER" + str(num_firms) +".txt"
	with open(file_name,'r') as file:
		for line in file:
			data = ast.literal_eval(line)

	num_years = len(data[1])

	file_name = "wealthER" + str(num_firms) +".txt"
	with open(file_name,'r') as file:
		for line in file:
			wealth = ast.literal_eval(line)

	wealth = wealth[transient:]
	yearly_wealth = []

	for i in xrange(0,len(wealth),12):
		w = wealth[i]
		yearly_wealth.append(w)

	print len(yearly_wealth), "yearly_wealth len"

	intervals = range(1,num_years+2)
	intervals = [i*10 for i in intervals]
	print intervals, "intervals"

	inflation = []

	for i in xrange(len(intervals)-1):
		a = intervals[i]
		b = intervals[i+1]
		c = (b-a)/a
		inflation.append(c)

	print inflation, "inflation"

	distribution = []
	for j in xrange(len(data[1])):
		temp = []
		count = 0
		for i in xrange(1,num_firms+1):
			v = data[i][j]
			if v <0:
				count += 1
			temp.append(v)
		temp = sorted(temp)
		distribution.append(temp)

	# look up online how to divide y axis by a constant, in this case number of firms so as to have probabilities

	ind = -1
	dist = distribution[ind]
	print len(dist), "num of firms"
	dist = [i-inflation[ind] for i in dist]
	std =  np.std(dist)

	std = round(std,2)
	writeUp = 'Standard deviation = ' + str(std)
	scale_y = num_firms
	#fig = plt.figure()
	#ax = fig.add_subplot(121)

	n, bin_edges = np.histogram(dist, 100)
	# Normalize it, so that every bins value gives the probability of that bin
	bin_probability = n/float(n.sum())
	# Get the mid points of every bin
	bin_middles = (bin_edges[1:]+bin_edges[:-1])/2.
	# Compute the bin-width
	bin_width = bin_edges[1]-bin_edges[0]
	# Plot the histogram as a bar plot
	plt.bar(bin_middles, bin_probability, width=bin_width,color='grey')


	#plt.hist(dist,bins=1000,color='black')
	

	#y_tick_labels = {0:r'$2^{0}$',-2:r'$2^{-2}$',-4:r'$2^{-4}$',-6:r'$2^{-6}$',-8:r'$2^{-8}$',-10:r'$2^{-10}$'}
	#x_tick_labels = {0:r"$10^0$",1:r"$10^1$",2:r"$10^2$",3:r"$10^3$"}
	#x_tick_labels = {0:r"$0$",120:r"$10$",240:r"$20$",360:r"$30$",480:r"$40$",600:r"$50$",720:r"$60$",840:r"$70$",960:r"$80$",
	#1080:r"$90$",1200:r"$100$"}

	plt.ylabel("Proportion of firms",fontsize=(labelFont+2))
	plt.xlabel("Annual sales growth",fontsize=(labelFont+2))
	plt.suptitle("Firm-level volatility",fontsize=(titleFont+2))
	plt.legend(loc=1,fontsize=labelFont-2)
	plt.text(0.7,0.1,writeUp,fontsize=16)
	plt.ticklabel_format(style='plain',axis='both',useOffset=False,fontsize=(ticksFont+4))
	plt.xticks(fontsize=(labelFont+2))
	plt.yticks(fontsize=(labelFont+2))

	#plt.ticklabel_format(fontsize=(ticksFont+4))
	#plt.xlim(xmin=-100,xmax=1300)
	#plt.xticks(x_tick_labels.keys(), x_tick_labels.values(),fontsize=ticksFont)
	#plt.yticks(y_tick_labels.keys(), y_tick_labels.values(),fontsize=ticksFont)
	plt.grid()
	

	save_name =  "firm_volatilityER"  + ".png"
	child_directory = 'plots_realShocks'
	if not os.path.exists(child_directory):
	    os.makedirs(child_directory)    
	os.chdir(child_directory)
	plt.savefig(save_name,bbox_inches='tight')
	plt.close()    
	os.chdir('..')

def size_changes(num_firms,labelFont,titleFont,ticksFont):
	labelFont += 8
	titleFont += 6
	ticksFont += 8
	
	file_name = 'mean_size_changeER' + str(num_firms) + '.txt'
	with open(file_name,'r') as file:
		for line in file:
			data = ast.literal_eval(line)
	
	data = data[840:]
	data = [math.log(i,2) for i in data]
	plt.scatter(range(len(data)),data,color='black',alpha=0.5,s=10)
	

	y_tick_labels = {0:r'$2^{0}$',-2:r'$2^{-2}$',-4:r'$2^{-4}$',-6:r'$2^{-6}$'}#,-8:r'$2^{-8}$',-10:r'$2^{-10}$'}
	#x_tick_labels = {0:r"$10^0$",1:r"$10^1$",2:r"$10^2$",3:r"$10^3$"}
	x_tick_labels = {0:r"$100$",120:r"$110$",240:r"$120$",360:r"$130$",480:r"$140$",600:r"$150$"}#,720:r"$60$",840:r"$70$",960:r"$80$",1080:r"$90$",1200:r"$100$"}

	plt.xlabel("Years",fontsize=labelFont)
	plt.ylabel(r"$\Phi$",fontsize=labelFont)
	plt.suptitle("Changes in sizes of firms",fontsize=titleFont)
	plt.legend(loc=1,fontsize=labelFont-2)
	plt.ticklabel_format(style='plain',axis='both',useOffset=False,fontsize=ticksFont)
	#plt.xlim(xmin=-100,xmax=1300)
	plt.xticks(x_tick_labels.keys(), x_tick_labels.values(),fontsize=ticksFont)
	plt.yticks(y_tick_labels.keys(), y_tick_labels.values(),fontsize=ticksFont)
	plt.grid()

	save_name =  "size_changedER"  + ".png"
	child_directory = 'plots_realShocks'
	if not os.path.exists(child_directory):
	    os.makedirs(child_directory)    
	os.chdir(child_directory)
	plt.savefig(save_name,bbox_inches='tight')
	plt.close()    
	os.chdir('..')

def link_changes(num_firms,mean_degree,labelFont,titleFont,ticksFont):
	labelFont += 8
	titleFont += 6
	ticksFont += 8

	file_name = 'links_changedER' + str(num_firms) + '.txt'
	with open(file_name,'r') as file:
		for line in file:
			data = ast.literal_eval(line)

	#plt.plot(data,color='black')
	data = data[780:]
	print len(data), "links changed data lent"
	data = [i/(mean_degree * num_firms) for i in data]
	plt.scatter(range(len(data)),data,color='black',alpha=0.5,s=10)
	

	y_tick_labels = {0.0140:r'$0.014$',0.0150:r'$0.015$',0.0160:r'$0.016$',0.0170:r'$0.017$'}
	#x_tick_labels = {0:r"$10^0$",1:r"$10^1$",2:r"$10^2$",3:r"$10^3$"}
	x_tick_labels = {0:r"$100$",120:r"$110$",240:r"$120$",360:r"$130$",480:r"$140$",600:r"$150$"}#,720:r"$60$",840:r"$70$",960:r"$80$",1080:r"$90$",1200:r"$100$"}



	plt.xlabel("Years",fontsize=labelFont)
	plt.ylabel(r"$\Delta$",fontsize=labelFont)
	plt.suptitle("Changes in production relations",fontsize=titleFont)
	plt.legend(loc=1,fontsize=labelFont-2)
	plt.ticklabel_format(style='plain',axis='both',useOffset=False,fontsize=ticksFont)
	#plt.xlim(xmin=-100,xmax=1300)
	# plt.ylim(ymin=0)
	plt.yticks(fontsize=ticksFont)
	plt.xticks(x_tick_labels.keys(), x_tick_labels.values(),fontsize=ticksFont)
	plt.yticks(y_tick_labels.keys(), y_tick_labels.values(),fontsize=ticksFont)
	plt.grid()


	
	save_name =  "links_changedER"  + ".png"
	child_directory = 'plots_realShocks'
	if not os.path.exists(child_directory):
	    os.makedirs(child_directory)    
	os.chdir(child_directory)
	plt.savefig(save_name,bbox_inches='tight')
	plt.close()    
	os.chdir('..')

def convergence_distribution(num_firms,labelFont,titleFont,ticksFont):
	labelFont += 8
	titleFont += 6
	ticksFont += 8


	file_name = "size_dist_stepsER" + str(num_firms) + ".txt"
	with open(file_name, 'r') as file:
		for line in file:
			data = ast.literal_eval(line)
	data = data[50:]
	data = data[:-50]
	events = data[0].keys()
	small_omega = []	
	last = data[-1]
	for i in xrange(len(data)):
		temp = 0
		a = data[i]
		for e in events:			
			temp += abs(a[e]-last[e])
		small_omega.append(temp)
	small_omega = [i/small_omega[0] for i in small_omega] 
	log_small_omega = []
	for i in small_omega:
		if i>0:
			log_small_omega.append(math.log(i,2))
		#else:
			#log_small_omega.append(0)

	log_small_omega = log_small_omega[:-140]

	plt.plot(log_small_omega,color='black',label='Degree distribution',lw=2)
	#plt.xlim(xmax=1300,xmin=-100)
	plt.ylim(ymax=1)

	y_tick_labels = {0:r'$2^0$',-2:r'$2^{-2}$',-4:r'$2^{-4}$',-6:r'$2^{-6}$'}#,-8:r'$2^{-8}$',-10:r'$2^{-10}$'}
	#x_tick_labels = {0:r"$10^0$",1:r"$10^1$",2:r"$10^2$",3:r"$10^3$"}
	x_tick_labels = {0:r"$0$",120:r"$10$",240:r"$20$",360:r"$30$",480:r"$40$",600:r"$50$",720:r"$60$",840:r"$70$",960:r"$80$",
	1080:r"$90$",1200:r"$100$"}


	plt.xlabel("Years",fontsize=labelFont)
	plt.ylabel(r"$\omega_{degree}$",fontsize=labelFont+6)
	plt.suptitle("Convergence of degree distribution",fontsize=titleFont)
	#plt.legend(loc=1,fontsize=labelFont-2)
	plt.ticklabel_format(style='plain',axis='both',useOffset=False,fontsize=ticksFont)
	plt.ylim(ymax=0)

	plt.xticks(x_tick_labels.keys(), x_tick_labels.values(),fontsize=ticksFont)
	plt.yticks(y_tick_labels.keys(), y_tick_labels.values(),fontsize=ticksFont)
	plt.grid()



	save_name =  "convergenceSteps_degree"  + ".png"
	child_directory = 'plots_realShocks'
	if not os.path.exists(child_directory):
	    os.makedirs(child_directory)    
	os.chdir(child_directory)
	plt.savefig(save_name,bbox_inches='tight')
	plt.close()    
	os.chdir('..')




	file_name = "deg_dist_stepsER" + str(num_firms) + ".txt"
	with open(file_name, 'r') as file:
		for line in file:			
			data = ast.literal_eval(line)
	data = data[50:]
	data = data[:-50]
	events = data[0].keys()
	print events, "events"
	print data[0].keys()
	small_omega = []	
	last = data[-1]
	print len(events), "num of events"
	print len(last.keys()), "num of last events"
	for i in xrange(len(data)):
		temp = 0
		a = data[i]
		for e in events:	
			if e in a and e in last:
				temp += abs(a[e]-last[e])
			#elif e in last and e not in a:
			#	temp += abs(last[e])
		small_omega.append(temp)
	small_omega = [i/small_omega[0] for i in small_omega] 
	log_small_omega = []
	for i in small_omega:
		if i>0:
			log_small_omega.append(math.log(i,2))
		#else:
		#	log_small_omega.append(0)

	log_small_omega = log_small_omega[:-120]
	plt.plot(log_small_omega,color='black',label='Size distribution',lw=2)
	#plt.xlim(xmax=1300,xmin=-100)

	y_tick_labels = {0:r'$2^0$',-2:r'$2^{-2}$',-4:r'$2^{-4}$',-6:r'$2^{-6}$',-8:r'$2^{-8}$',-10:r'$2^{-10}$'}
	#x_tick_labels = {0:r"$10^0$",1:r"$10^1$",2:r"$10^2$",3:r"$10^3$"}
	x_tick_labels = {0:r"$0$",120:r"$10$",240:r"$20$",360:r"$30$",480:r"$40$",600:r"$50$",720:r"$60$",840:r"$70$",960:r"$80$",
	1080:r"$90$",1200:r"$100$"}

	plt.xlim(xmax=1200)
	plt.xlabel("Years",fontsize=labelFont)
	plt.ylabel(r"$\omega_{size}$",fontsize=labelFont+6)
	plt.suptitle("Convergence of size distribution",fontsize=titleFont)
	plt.ylim(ymax=1)
	#plt.legend(loc=1,fontsize=labelFont-2)
	plt.ticklabel_format(style='plain',axis='both',useOffset=False,fontsize=ticksFont)
	plt.ylim(ymax=0)
	plt.xticks(x_tick_labels.keys(), x_tick_labels.values(),fontsize=ticksFont)
	plt.yticks(y_tick_labels.keys(), y_tick_labels.values(),fontsize=ticksFont)
	plt.grid()



	save_name =  "convergenceSteps_size"  + ".png"
	child_directory = 'plots_realShocks'
	if not os.path.exists(child_directory):
	    os.makedirs(child_directory)    
	os.chdir(child_directory)
	plt.savefig(save_name,bbox_inches='tight')
	plt.close()    
	os.chdir('..')

def cumulate_list(nums):
    nums.reverse()
    cum_nums = []
    for i in xrange(len(nums)):
        c = 0
        for j in xrange(i, len(nums)):
            c += nums[j]
        cum_nums.append(c)
    return cum_nums

def test():
    file = 'n_economy_SF_endo3.csv'
    data = pd.read_csv(file)
    data = pd.DataFrame(data)
    vals = list(data[:]["out_of_market_firms"])
    print vals

def CCDF(list_sizes,logV,max_size,min_size):
    sizes = range(min_size,max_size+1)
    sizes_dict = collections.OrderedDict({})
    for s in sizes:
        sizes_dict[s] = 0
    for val in list_sizes:
        for s in sizes:
            if val > s:
                sizes_dict[s] += 1
    cum = []
    sizes = []
    for s in sizes_dict:
        v  = sizes_dict[s]
        if v>0:
            v = math.log(v,logV)
            cum.append(v)
            s = math.log(s,logV)
            sizes.append(s)
    return cum, sizes

def zipf(list_sizes,logV,max_size,min_size):
    sizes = range(min_size,max_size+1)
    sizes_dict = collections.OrderedDict({})
    for s in sizes:
        sizes_dict[s] = 0
    for val in list_sizes:
        l = math.log(val,logV)      
        for s in sizes:
            if l > s:
                sizes_dict[s] += 1
    print sizes_dict, "sizes_dict"
    cum = []
    sizes = []
    for s in sizes_dict:
        v  = sizes_dict[s]
        if v>0:
            v = math.log(v,logV)
            cum.append(v)
            sizes.append(s)
    return cum, sizes

def size_distribution(num_firms,network,labelFont,titleFont,ticksFont):
	labelFont += 8
	titleFont += 6
	ticksFont += 8
    
	file = "size_dist" + str(network) + str(num_firms) + ".txt"
	with open(file,'r') as file:
	    for line in file:
	        sizes = ast.literal_eval(line)

	print len(sizes), "num sizes"
	print len(sizes[0])



	n = num_firms
	mul = 100000
	exp = 2
	minS = 12
	maxS = 20

	sizes_raw = [random.uniform(0,1) for i in xrange(n)]
	sizes_raw = [i * mul for i in sizes_raw]
	print min(sizes_raw), "min raw size"
	[firm_list,sizes_list] = zipf(sizes_raw,exp,max_size=maxS,min_size=minS)
	print firm_list, "time 0"
	firm_list = [i - math.log(num_firms,exp) for i in firm_list]
	plt.plot(sizes_list,firm_list, color='salmon',label='Year 0',linestyle='solid',lw=2)
	plt.scatter(sizes_list,firm_list, color='salmon',s=20)#,markersize=1,)


	sizes_raw = sizes[3]
	sizes_raw = [i * mul for i in sizes_raw]
	[firm_list,sizes_list] = zipf(sizes_raw,exp,max_size=maxS,min_size=minS)
	print firm_list, "time 100"
	firm_list = [i - math.log(num_firms,exp) for i in firm_list]
	plt.plot(sizes_list,firm_list,color='black',label='Year 25',linestyle='dashed',alpha=0.7,lw=2)
	plt.scatter(sizes_list,firm_list, color='black',s=20,alpha=0.7)#,markersize=1,)
	
	#y_tick_labels = {0:r'$2^0$',-2:r'$2^{-2}$',-4:r'$2^{-4}$',-6:r'$2^{-6}$',-8:r'$2^{-8}$',-10:r'$2^{-10}$',-12:r'$2^{-12}$',-14:r'$2^{-14}$'}
	y_tick_labels = {0:r'$2^0$',-4:r'$2^{-4}$',-8:r'$2^{-8}$',-12:r'$2^{-12}$'}#,-14:r'$2^{-14}$'}
	#x_tick_labels = {8:r"$2^8$",10:r"$2^{10}$",12:r"$2^{12}$",14:r"$2^{14}$",16:r"$2^{16}$",18:r"$2^{18}$",20:r"$2^{20}$",22:r"$2^{22}$"}
	#x_tick_labels = {12:r"$2^{12}$",13:r"$2^{13}$",14:r"$2^{14}$",15:r"$2^{15}$",16:r"$2^{16}$",17:r"$2^{17}$",18:r"$2^{18}$",19:r"$2^{19}$",20:r"$2^{20}$"}
	#x_tick_labels = {8:r"$2^8$",10:r"$2^{10}$",12:r"$2^{12}$",14:r"$2^{14}$",16:r"$2^{16}$"}
	x_tick_labels = {12:r"$2^{0}$",14:r"$2^{2}$",16:r"$2^{4}$",18:r"$2^{6}$",20:r"$2^{8}$"}

	sizes_raw = sizes[-1]
	sizes_raw = [i * mul for i in sizes_raw]
	[firm_list,sizes_list] = zipf(sizes_raw,exp,max_size=maxS,min_size=minS)
	print firm_list, "time 100"
	firm_list = [i - math.log(num_firms,exp) for i in firm_list]
	plt.plot(sizes_list,firm_list,color='black',label='Year 100',linestyle='solid',lw=2)
	plt.scatter(sizes_list,firm_list, color='black',s=20)#,markersize=1,)
	
	plt.ylim(ymax=1)
	plt.xlabel("Sizes",fontsize=labelFont)
	plt.ylabel(r"Cumulative probability $p(X\geq x)$",fontsize=labelFont)
	plt.suptitle("Size distribution of firms",fontsize=titleFont)
	plt.legend(loc=3,fontsize=labelFont-2)
	plt.ticklabel_format(style='plain',axis='both',useOffset=False,fontsize=ticksFont)

	plt.xticks(x_tick_labels.keys(), x_tick_labels.values(),fontsize=ticksFont)
	plt.yticks(y_tick_labels.keys(), y_tick_labels.values(),fontsize=ticksFont)
	plt.grid()


	save_name = network + "_" + "size_dist.png"
	child_directory = 'plots_realShocks'
	if not os.path.exists(child_directory):
	    os.makedirs(child_directory)    
	os.chdir(child_directory)
	plt.savefig(save_name,bbox_inches='tight')
	plt.close()    
	os.chdir('..')
  
def return_firmsList_degDist(distribution_list):
    d = sorted(distribution_list)
    cum_d = []
    for i in xrange(1, len(d)-1):
        cum_d.append(d[i]+d[i+1])
    log_cum_d = [math.log(i,10) for i in cum_d]
    log_cum_d.reverse()
    num_firms = range(1,len(d)-1)
    log_num_firms = [math.log(i,10) for i in num_firms]
    return log_num_firms, log_cum_d
    #return num_firms, cum_d

def degree_distribution(num_firms,network,labelFont,titleFont,ticksFont):
	labelFont += 8
	titleFont += 6
	ticksFont += 8

	in_file = "in_dist" + network + str(num_firms) + ".txt"
	out_file = "out_dist" + network + str(num_firms) + ".txt"

	with open(in_file,'r') as file:
	    for line in file:
	        in_dist = ast.literal_eval(line)

	with open(out_file, 'r') as file:
	    for line in file:
	        out_dist = ast.literal_eval(line)


	print len(out_dist), "num in out dist"


	#exp = 10
	exp = 2
	max_s = 10000
	min_s = 1

	#y_tick_labels = {0:r'$10^0$',-1:r'$10^{-1}$',-2:r'$10^{-2}$',-3:r'$10^{-3}$',-4:r'$10^{-4}$',-5:r'$10^{-5}$',-6:r'$10^{-6}$'}
	#y_tick_labels = {0:r'$2^0$',-2:r'$2^{-2}$',-4:r'$2^{-4}$',-6:r'$2^{-6}$',-8:r'$2^{-8}$',-10:r'$2^{-10}$',-12:r'$2^{-12}$',-14:r'$2^{-14}$',-14:r'$2^{-16}$,'-18:r'$2^{-18}$'}
	y_tick_labels = {0:r'$2^0$',-4:r'$2^{-4}$',-8:r'$2^{-8}$',-12:r'$2^{-12}$',-16:r'$2^{-16}$'}#,-20:r'$2^{-20}$'}
	#x_tick_labels = {0:r"$10^0$",1:r"$10^1$",2:r"$10^2$",3:r"$10^3$"}
	#x_tick_labels = {0:r"$2^0$",1:r"$2^1$",2:r"$2^2$",3:r"$2^3$",4:r"$2^4$",5:r"$2^5$",6:r"$2^6$",7:r"$2^7$"}
	x_tick_labels = {0:r"$2^0$",2:r"$2^2$",4:r"$2^4$",6:r"$2^6$",6:r"$2^8$"}


	[firm_list,deg_list] = CCDF(list_sizes=out_dist[0],logV=exp,max_size=max_s,min_size=min_s)
	print max(deg_list), "max degree"
	firm_list = [i - math.log(num_firms,exp) for i in firm_list]
	plt.plot(deg_list,firm_list,color='salmon',label='Year 0',lw=2,marker='.',markersize=0.75)
	plt.scatter(deg_list, firm_list, color='salmon',marker='.',s=0.5)

	[firm_list,deg_list] = CCDF(list_sizes=out_dist[3],logV=exp,max_size=max_s,min_size=min_s)
	print max(deg_list), "max degree"
	firm_list = [i - math.log(num_firms,exp) for i in firm_list]
	plt.plot(deg_list,firm_list,color='black',label='Year 25',lw=2,marker='.',markersize=0.75,linestyle='dashed',alpha=0.7)
	plt.scatter(deg_list, firm_list, color='black',marker='.',s=0.5,alpha=0.7)

	"""
	[firm_list,deg_list] = CCDF(list_sizes=out_dist[7],logV=exp,max_size=max_s,min_size=min_s)
	print max(deg_list), "max degree"
	firm_list = [i - math.log(num_firms,exp) for i in firm_list]
	plt.plot(deg_list,firm_list,color='grey',label='Year 50',lw=1,marker='.',markersize=0.75)
	plt.scatter(deg_list, firm_list, color='grey',marker='.',s=0.5)
	"""

	[firm_list,deg_list] = CCDF(list_sizes=out_dist[-1],logV=exp,max_size=max_s,min_size=min_s)
	print max(deg_list), "max degree"
	firm_list = [i - math.log(num_firms,exp) for i in firm_list]
	plt.plot(deg_list,firm_list, color='black',label='Year 100',lw=2,marker='.',markersize=0.75)
	plt.scatter(deg_list,firm_list,color='black',marker='.',s=0.5)#,markersize=1,)
	#plt.xlim(xmin=1)
	plt.xlabel("Out degree",fontsize=labelFont)
	plt.ylabel(r"Cumulative probability $p(X\geq x)$",fontsize=labelFont)
	plt.suptitle("Degree distribution of the production network",fontsize=titleFont)
	plt.legend(loc=3,fontsize=labelFont-2)
	plt.ticklabel_format(style='plain',axis='both',useOffset=False,fontsize=ticksFont)

	plt.ylim(ymax=1)
	plt.xticks(x_tick_labels.keys(), x_tick_labels.values(),fontsize=ticksFont)
	plt.yticks(y_tick_labels.keys(), y_tick_labels.values(),fontsize=ticksFont)
	plt.grid()

	save_name = network + "_" + "deg_dist.png"
	child_directory = 'plots_realShocks'
	if not os.path.exists(child_directory):
	    os.makedirs(child_directory)    
	os.chdir(child_directory)
	plt.savefig(save_name,bbox_inches='tight')
	plt.close()    
	os.chdir('..')

def equilibrium_volatility(exponent,list_sizes,firmVol):
	e = (exponent-1)/exponent
	equi_vol = collections.OrderedDict({})
	for num_firms in list_sizes:
		vol = 0.1 / (num_firms ** e)
		equi_vol[num_firms] = vol
	return equi_vol

def return_volatility(variable, file,quarterly,change):
    all_data = {}
    data = pd.read_csv(file)
    data = pd.DataFrame(data)
    rows = data.shape[0]
    for r in xrange(rows):
        num_firms = data.iloc[r]['n']
        all_data[num_firms] = []

    rows = data.shape[0]
    if quarterly:
        for r in xrange(rows):
            num_firms = data.iloc[r]['n']
            gdp = ast.literal_eval(data.iloc[r][variable])
            yearly_gdp = []
            #years = int(len(gdp)/4)
            years = int(len(gdp)/12)
            #print years, "years"
            for y in xrange(years):  
                count = 0             
                #for q in xrange(4):
                for q in xrange(12):
                    #c = y * 4 + q
                    c = y * 12 + q
                    #print c 
                    g = gdp[c]
                    count += g
                yearly_gdp.append(count)            
            norm = np.mean(yearly_gdp)
            yearly_gdp = [i/norm for i in yearly_gdp]
            if change:
                series = []
                for j in xrange(len(yearly_gdp)-1):
                    first = yearly_gdp[j]
                    second = yearly_gdp[j+1]
                    ratio = first/second
                    series.append(ratio)
            else:
                series = [math.log(i,10) for i in yearly_gdp]    
            #vol = np.std(yearly_gdp,ddof=1)
            vol = np.std(series)               
            all_data[num_firms].append(vol)            
    else:        
    	for r in xrange(rows):
    	    num_firms = data.iloc[r]['n']
    	    gdp = ast.literal_eval(data.iloc[r][variable])
    	    norm = np.mean(gdp)
    	    gdp = [i/norm for i in gdp]
            if change:
                series = []
                for j in xrange(len(gdp)-1):
                    first = gdp[j]
                    second = gdp[j+1]
                    ratio = first/second
                    series.append(ratio)
            else:
                series = [math.log(i,10) for i in gdp]    
    	    #vol = np.std(gdp,ddof=1)
            vol = np.std(series)    
    	    all_data[num_firms].append(vol)
    #print all_data, quarterly    
    return all_data

def mean_vol(data_dict):
	mean_vol_dict = {}
	for n in data_dict:
		list_vol = data_dict[n]
		mean_val = np.mean(list_vol)
		mean_vol_dict[n] = mean_val
	return mean_vol_dict

def error_vol(data_dict):
	error_vol_dict = {}
	for n in data_dict:
		vals = data_dict[n]
		error_val =  stats.sem(vals)
		error_vol_dict[n] = error_val
	return error_vol_dict

def sort_dict(data_dict):
	sorted_dict = collections.OrderedDict({})
	vals = data_dict.keys()
	vals = sorted(vals)
	for v in vals:
		value = data_dict[v]
		sorted_dict[v] = value
	return sorted_dict

def log_fun(list_vals):
	log_list = [math.log(i,10) for i in list_vals]
	return log_list

def plot_aggregate_vol(labelFont,titleFont,ticksFont):
    fixed_network = ['n_economy_powerlaw_CD1.csv','n_economy_powerlaw_CD2.csv','n_economy_powerlaw_CD3.csv']
    endo_network = ['n_economy_ER_endo1.csv','n_economy_ER_endo2.csv','n_economy_ER_endo3.csv']
    #endo_network = ['n_economy_SF_endo1.csv','n_economy_SF_endo2.csv']
    

    vol_fixed = {}
    vol_endo = {}

    error_fixed = {}
    error_endo = {}


    for file in fixed_network:
    	vol = return_volatility(variable="sumOutput", file=file,quarterly=True,change=False)
    	#vol = return_volatility(variable="gdp_equilibrium_prices", file=file,quarterly=True,change=False)
    	vol = mean_vol(data_dict=vol)
    	error = error_vol(data_dict=vol)
    	for n in vol:
    		vol_fixed[n] = vol[n]
    		error_fixed[n] = error[n]


    for file in endo_network:
    	vol = return_volatility(variable="gdp", file=file,quarterly=True,change=True)
        #vol = return_volatility(variable="sumOutput", file=file,quarterly=True,change=True)
        #vol = return_volatility(variable="weighted_consumption", file=file,quarterly=True,change=True)
        
    	vol = mean_vol(data_dict=vol)
    	error = error_vol(data_dict=vol)
    	for n in vol:
    		vol_endo[n] = vol[n]
    		error_endo[n] = error[n]


    vol_fixed = sort_dict(vol_fixed)
    vol_endo = sort_dict(vol_endo)

    error_fixed = sort_dict(error_fixed)
    error_endo = sort_dict(error_endo)

    equi_vol = equilibrium_volatility(exponent=1.61,list_sizes=[10,100,1000,10000,100000,1000000],firmVol=0.1)



    log_vol_fixed = log_fun(vol_fixed.values())
    log_vol_endo = log_fun(vol_endo.values())
    log_equi_vol = log_fun(equi_vol.values())



    lab_firms = {1:100,2:1000,3:10000,4:100000,5:1000000}
    xVals = lab_firms.keys()
    tick_labels = {1:r'$10^2$',2:r'$10^3$',3:r'$10^4$',4:r'$10^5$',5:r'$10^6$'}
    y_tick_labels = {-4:r"$10^{-4}$",-3.5:" ",-3:r"$10^{-3}$",-2.5:" ",-2:r"$10^{-2}$",-1.5:r" ",-1:r"$10^{-1}$",0:r"$10^{0}$"}

    fig, ax = plt.subplots()
    ax.tick_params(axis='both', labelsize=ticksFont)



    #plt.scatter(xVals,log_vol_endo,color='black')
    #plt.plot(xVals,log_vol_endo,color='black',label='Non-equilibrium volatility',linestyle='--')
    
    print len(xVals), "VALS"
    print len(log_equi_vol), "VALS"
    print len(log_vol_fixed), "VALS"

    plt.scatter(xVals,log_vol_endo,color='black')
    plt.plot(xVals,log_vol_endo,color='black',label='Non-equilibrium volatility',linestyle='-')
    #plt.plot(xVals,log_vol_endo,color='black',linestyle='--')
    
    #plt.scatter(xVals[:-1],log_vol_endo,color='black')
    #plt.plot(xVals[:-1],log_vol_endo,color='black',label='Non-equilibrium volatility',linestyle='--')

    plt.scatter(xVals,log_equi_vol[1:],color='grey',alpha=0.5)
    plt.plot(xVals,log_equi_vol[1:],color='grey',label='Equilibrium volatility',alpha=0.5)

    #plt.scatter(xVals,log_vol_fixed,color='royalblue')
    #plt.plot(xVals,log_vol_fixed,color='royalblue',label='Disequilibrium volatility')


    plt.ylim(ymin=-4)
    plt.ylim(ymax=0)
    #ax.hlines(y=0.875,xmax=limit+5,xmin=20,label='Direct',color='royalblue',linestyle='--',lw=1.5)
    plt.hlines(y=-0.5,xmax=6,xmin=-1, linestyle='dotted',lw=0.5)
    plt.xlim(xmax=6,xmin=0)
    plt.xticks(tick_labels.keys(), tick_labels.values(),fontsize=(labelFont+2))
    plt.yticks(y_tick_labels.keys(), y_tick_labels.values(),fontsize=(labelFont+2))
    plt.grid()

    labelFont += 2
    titleFont += 2
    plt.suptitle("The scaling of aggregate volatility", fontsize=titleFont)
    plt.xlabel("Number of firms", fontsize=labelFont)
    plt.ylabel("Standard deviation of changes in GDP", fontsize=labelFont)
    #plt.ticklabel_format(style='plain',axis='both',useOffset=False)
    #plt.legend(loc=3,fontsize=labelFont-2)
    plt.text(4.5,-1.8,'Non-equilibrium',color='black')
    plt.text(4.5,-3,'Equilibrium',color='grey')

    print "saved "
    saveName =  'aggregate_volatility.png'

    child_directory = 'plots_realShocks'
    if not os.path.exists(child_directory):
        os.makedirs(child_directory)    
    os.chdir(child_directory)
    plt.savefig(saveName, bbox_inches='tight')
    plt.close()
    os.chdir('..')      

def convergence():
    legendSize = 18
    fontSize_title = 18
    fontSize_xlabel = 16
    fontSize_ylabel = 16
    fontSize_ticks = 16

    file = 'one_time_productivity_shock_std_economy_powerlaw_CD_convergence.csv'
    plots.convergence_mean_error(csv_file=file,
                                 thresholds=[2],
                                 legend=False,
                                 legendSize=legendSize,
                                 legend_location=None,
                                 fontSize_title=fontSize_title,
                                 fontSize_xlabel=fontSize_xlabel,
                                 fontSize_ylabel=fontSize_ylabel,
                                 fontSize_ticks=fontSize_ticks,
                                 child_directory='plots_realShocks',
                                 digits_yAxis=False,
                                 maxXticks=5,
                                 maxYticks=5,
                                 colors=['black'],
                                 title_text='Half life of productivity shock',
                                 xlabel=r'Standard deviation of shocks $\gamma$',
                                 ylabel='Quarters',
                                 ymin=0,
                                 ymax=6,
                                 xmin=0,
                                 xmax=1.1,
                                 save_name=None,
                                 cutOff=None)

    file = 'powerlaw_exponent_economy_powerlaw_CD_convergence.csv'
    plots.convergence_mean_error(csv_file=file,
                                 thresholds=[2],
                                 legend=False,
                                 legendSize=legendSize,
                                 legend_location=1,
                                 fontSize_title=fontSize_title,
                                 fontSize_xlabel=fontSize_xlabel,
                                 fontSize_ylabel=fontSize_ylabel,
                                 fontSize_ticks=fontSize_ticks,
                                 child_directory='plots_realShocks',
                                 digits_yAxis=False,
                                 maxXticks=5,
                                 maxYticks=5,
                                 colors=['midnightblue'],
                                 title_text='Half life of productivity shock',
                                 xlabel=r'Powerlaw exponent',
                                 ylabel='Quarters',
                                 ymin=0,
                                 ymax=10,
                                 xmin=1.0,
                                 xmax=2.0,
                                 save_name=None,
                                 cutOff=None)

    file = 'n_economy_SF_CD_endoStep_convergence.csv'
    tick_labels = {1:r'$10^1$',2:r'$10^2$',3:r'$10^3$',4:r'$10^4$'}
    plots.convergence_mean_error3(csv_file=file,
                                 thresholds=[2],
                                 legend=False,
                                 legendSize=legendSize,
                                 legend_location=1,
                                 fontSize_title=fontSize_title,
                                 fontSize_xlabel=fontSize_xlabel,
                                 fontSize_ylabel=fontSize_ylabel,
                                 fontSize_ticks=fontSize_ticks,
                                 child_directory='plots_realShocks',
                                 digits_yAxis=False,
                                 maxXticks=5,
                                 maxYticks=5,
                                 colors=['midnightblue'],
                                 title_text='Half life of change in production network',
                                 xlabel=r'Number of firms',
                                 ylabel='Quarters',
                                 ymin=0,
                                 ymax=14,
                                 xmin=None,
                                 xmax=4.5,
                                 save_name=None,
                                 cutOff=None,
                                 logScale='x',
                                 xticksLabel=tick_labels,
                                 yticksLabel=None)

    file = 'n_economy_SF_CD_endo_stop_convergence.csv'
    plots.convergence_mean_error3(csv_file=file,
                                 thresholds=[2],
                                 legend=False,
                                 legendSize=legendSize,
                                 legend_location=1,
                                 fontSize_title=fontSize_title,
                                 fontSize_xlabel=fontSize_xlabel,
                                 fontSize_ylabel=fontSize_ylabel,
                                 fontSize_ticks=fontSize_ticks,
                                 child_directory='plots_realShocks',
                                 digits_yAxis=False,
                                 maxXticks=5,
                                 maxYticks=5,
                                 colors=['midnightblue'],
                                 title_text='Time to convergence to equilibrium',
                                 xlabel=r'Number of firms',
                                 ylabel='Quarters',
                                 ymin=0,
                                 ymax=36,
                                 xmin=None,
                                 xmax=4.5,
                                 save_name=None,
                                 cutOff=None,
                                 logScale='x',
                                 xticksLabel=tick_labels,
                                 yticksLabel=None)

def convergence_time_series():
    #file = 'powerlaw_exponent_economy_powerlaw_CD.csv'
    file = 'one_time_productivity_shock_std_economy_powerlaw_CD.csv'
    data = pd.read_csv(file)
    data = pd.DataFrame(data)
    rows = data.shape[0]
    series = []
    for r in xrange(rows):
        d = ast.literal_eval(data.iloc[r]['mean_price_change'])    
        d = list(d)
        #d = d[5:31]
        d = d[5:31]
        d = [math.log(i,10) for i in d]
        series.append(d)
    for s in series:
        plt.plot(s,color='grey')
    plt.plot(s,color='grey',label=r'$10^{5}$ firms')

    file = 'one_time_productivity_shock_std_economy_powerlaw_CD_million.csv'
    data = pd.read_csv(file)
    data = pd.DataFrame(data)
    rows = data.shape[0]
    series = []
    for r in xrange(rows):
        d = ast.literal_eval(data.iloc[r]['mean_price_change'])    
        d = list(d)
        #d = d[5:31]
        d = d[5:31]
        d = [math.log(i,10) for i in d]
        series.append(d)
    for s in series:
        plt.plot(s,color='salmon')
    plt.plot(s,color='salmon',label=r'$10^{6}$ firms')
    



    #add one line with 10^6 firms
    # also change the file from which 10^5 firms is taken...

    labelFont = 16
    titleFont = 18
    ticksFont = 14

    plt.suptitle("Time to equilibrium convergence", fontsize=titleFont)
    plt.xlabel("Months", fontsize=labelFont)
    plt.ylabel(r"Distance to equilibrium $ln(\delta)$", fontsize=labelFont)
    #plt.ticklabel_format(style='plain',axis='both',useOffset=False)
    plt.legend(loc=1,fontsize=labelFont-2)
    plt.grid()
    saveName =  'convergence_time_series.png'

    child_directory = 'plots_realShocks'
    if not os.path.exists(child_directory):
        os.makedirs(child_directory)    
    os.chdir(child_directory)
    plt.savefig(saveName, bbox_inches='tight')
    plt.close()
    os.chdir('..')  
    
def plot():
	labelFont = 14
	titleFont = 16
	ticksFont = 14

	#num_firms = 100000
	transient = 240


	child_directory = 'data_realShocks'
	if not os.path.exists(child_directory):
	    os.makedirs(child_directory)    
	os.chdir(child_directory)

	#heat_map()
	
	#write_size_degree_dat(num_firms=100000)
	#write_US_gdpGrowth()
	#tail_risk(transient,num_firms,labelFont,titleFont,ticksFont)
	#write_USsizes()

	#deg_dist_US("first",labelFont,titleFont,ticksFont)
	#deg_dist_US("second",labelFont,titleFont,ticksFont)

	#goodness_fit()
	num_firms = 100000

	firm_volatility(transient,num_firms,labelFont,titleFont,ticksFont)
	
	


	#size_changes(num_firms,labelFont,titleFont,ticksFont)
	#link_changes(num_firms=num_firms,mean_degree=10,labelFont=labelFont,titleFont=titleFont,ticksFont=ticksFont)
	#convergence_distribution(num_firms,labelFont,titleFont,ticksFont)    

	#size_distribution(num_firms,'ER',labelFont,titleFont,ticksFont)
	#degree_distribution(num_firms,'ER',labelFont,titleFont,ticksFont)
	

	
	plot_aggregate_vol(labelFont,titleFont,ticksFont)
	
	#convergence()
	#convergence_time_series()
	
	#degree_distribution(num_firms,'SF',labelFont,titleFont,ticksFont)
	#size_distribution(num_firms,'SF',labelFont,titleFont,ticksFont)
	

	os.chdir('..')
	

plot()