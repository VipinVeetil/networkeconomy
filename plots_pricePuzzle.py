from __future__ import division
import pandas as pd
import matplotlib.pyplot as plt
import random as random
import numpy as np
from math import log
import math
import ast
from itertools import chain
from scipy import stats
from matplotlib.ticker import FormatStrFormatter
import collections
import os
import csv
import plots
import cPickle
import ast


def heatMap():
    plots.countourSurface(csv_file='pce_theta_s.csv',
                          x_name='s',
                          y_name='exponent',
                          z_name='priceLevel',
                          x_label=r'Size of shock (percent)',
                          y_label=r'$\theta$',
                          title=r'Price Level change (percent)',
                          save_name=None,
                          fontSize_title=24,
                          fontSize_xlabel=22,
                          fontSize_ylabel=22,
                          fontSize_ticks=18,
                          child_directory='plots_pricePuzzle',
                          zeroYValue=False,
                          maxYValue=False)


def time_series():
    fontSize_supTitle = 20
    fontSize_xlabel = 16
    fontSize_ylabel = 16
    fontSize_ticks = 16

    exponent = ['small']
    file = 'economyPickle_firms_small.cPickle'
    E = cPickle.load(open(file, "rb"))
    PCE = getattr(E, 'PCE')
    CPI = getattr(E, 'CPI')
    normPCE = PCE[0]
    normCPI = CPI[0]

    PCE = [i/normPCE for i in PCE]
    CPI = [i/normCPI for i in CPI]

    begin = 1
    end = 27

    PCE = PCE[begin:end]
    CPI = CPI[begin:end]

    new_PCE = []
    subvalues = PCE[2:]
    for i in xrange(3, 22):
      if i%3 == 0:
        v0 = subvalues[i]
        v1 = subvalues[i+1]
        v2 = subvalues[i+2]
        al = [v0,v1,v2]
        m = np.mean(al)
        new_PCE.append(m)

    new_CPI = []
    subvalues = CPI[2:]
    for i in xrange(3, 22):
      if i%3 == 0:
        v0 = subvalues[i]
        v1 = subvalues[i+1]
        v2 = subvalues[i+2]
        al = [v0,v1,v2]
        m = np.mean(al)
        new_CPI.append(m)



    one = PCE[0]
    two = PCE[1]
    three = PCE[2]

    early_PCE = [one, two, three]
    early_PCE += new_PCE


    one = CPI[0]
    two = CPI[1]
    three = CPI[2]

    early_CPI = [one, two, three]
    early_CPI += new_CPI

    timeSteps = xrange(len(early_PCE))
    timeSteps = [i-2 for i in timeSteps]

    plt.scatter(timeSteps, early_PCE, color='black', label='PCE', marker='o')
    plt.plot(timeSteps, early_PCE, color='black', linestyle='-')

    plt.scatter(timeSteps, early_CPI, color='grey', label='CPI', marker='*',s=50)
    plt.plot(timeSteps, early_CPI, color='grey', linestyle='-')


    baseTitle = 'Time series of the price-level'
    baseXLabel = 'Quarters'
    baseYLabel = 'Price level (normalized)'
    plt.suptitle(baseTitle, fontsize=fontSize_supTitle)
    plt.xlabel(baseXLabel, fontsize=fontSize_xlabel)
    plt.ylabel(baseYLabel, fontsize=fontSize_ylabel)
    plt.tick_params(axis='both', labelsize=fontSize_ticks)
    plt.ticklabel_format(style='plain',axis='both',useOffset=False)
    plt.legend(loc=1,fontsize=fontSize_xlabel)
    plt.grid()
    plt.ylim([0.997,1.003])

    child_directory = 'plots_pricePuzzle'
    if not os.path.exists(child_directory):
        os.makedirs(child_directory)
    os.chdir(child_directory)

    saveName =  'PCE_CPI_timeSeries.png'
    plt.savefig(saveName, bbox_inches='tight')
    plt.close()


    os.chdir('..')

    



def timeSeries_firms():
    fontSize_supTitle = 20
    fontSize_xlabel = 16
    fontSize_ylabel = 16
    fontSize_ticks = 16

    #exponent = ['big', 'small']
    exponent = ['small']
    
    for e in exponent:
        file = 'economyPickle_firms_' +  str(e) +'.cPickle'
        E = cPickle.load(open(file, "rb"))
        PCE = getattr(E, 'PCE')
        CPI = getattr(E, 'CPI')
        #CPI = getattr(E, 'CPI')
        #Demand = getattr(E, 'consumer_demand')
        #Wealth = getattr(E, 'wealth')
        #agents_data = getattr(E, 'households')
        #hh = agents_data[-1]
        #hh_wealth = getattr(hh, 'wealth_time_series')

        #CPI = getattr(E, 'CPI')
        #CPIXH = getattr(E, 'CPIsansHousing')
        #normCPI = CPI[0]
        normPCE = PCE[0]
        #normDemand = Demand[0]
        #normahh_wealth = hh_wealth[0]
        #normWealth = Wealth[0]
        #normCPI = CPI[20]
        #normCPIXH = CPIXH[20]
        begin=1
        end=27
        minus = 6
        timeSteps = range(begin-minus,end-minus)
        PCE = [i/normPCE for i in PCE]
        #CPI = [i/normCPI for i in CPI]
        #Demand = [i/normDemand for i in Demand]
        #hh_wealth = [i/normahh_wealth for i in hh_wealth]
        #Wealth = [i/normWealth for i in Wealth]
        #CPI = [i/normCPI for i in CPI]
        #CPIXH = [i / normCPIXH for i in CPIXH]
        PCE = PCE[begin:end]
        #Demand = Demand[begin:end]
        #hh_wealth = hh_wealth[begin:end]
        #Wealth = Wealth[begin:end]
        #CPI = CPI[begin:end]
        #CPIXH = CPIXH[begin:end]

        #Data = {'PCE': PCE,'Demand': Demand,'hhWealth':hh_wealth,'Wealth':Wealth}
        #Data = {'PCE': PCE}
        #Data = {'PCE': PCE,'CPI': CPI}
        Data = {'PCE': PCE,'CPI':CPI}
        colors = {'CPI':'cornflowerblue','PCE': 'black', 'Demand': 'blue', 'hhWealth': 'cornflowerblue','Wealth':'red'}
        markers = {'PCE': 'o', 'CPI': '*', 'CPIXH': 's', 'Demand':'.', 'hhWealth':'*','Wealth':'*'}
        labels = {'PCE': 'PCE', 'CPI': 'CPI', 'Demand': 'HH Demand', 'hhWealth': 'HH Wealth', 'Wealth': 'Economy Wealth'}

        """
        for name in Data:
            color = colors[name]
            marker = markers[name]
            values = Data[name]
            lab = labels[name]
            plt.scatter(timeSteps, values, color=color, label=lab, marker=marker)
            plt.plot(timeSteps, values, color=color, linestyle='-')
        """

        #baseTitle = 'Price level time series'
        #baseTitle = 'Two Products'
        baseTitle = 'Time Series'
        baseXLabel = 'Quarters'
        baseYLabel = 'Price level (normalized)'
        child_directory = 'plots_pricePuzzle'
        if not os.path.exists(child_directory):
            os.makedirs(child_directory)
        os.chdir(child_directory)
        plt.suptitle(baseTitle, fontsize=fontSize_supTitle)
        plt.xlabel(baseXLabel, fontsize=fontSize_xlabel)
        plt.ylabel(baseYLabel, fontsize=fontSize_ylabel)
        plt.tick_params(axis='both', labelsize=fontSize_ticks)
        plt.ticklabel_format(style='plain',axis='both',useOffset=False)
        plt.legend(loc=1,fontsize=fontSize_xlabel)
        plt.grid()
        plt.ylim([0.996,1.005])

        saveName =  'timeSeries_firms' + str(e) + '.png'
        plt.savefig(saveName, bbox_inches='tight')
        plt.close()
        

        name = 'PCE'
        #name = 'CPI'
        color = colors[name]
        marker = markers[name]
        values = Data[name]
        lab = labels[name]

        
        #print len(values)
        subvalues = values[2:]

        new_vals = []
        for i in xrange(3, 22):
            if i%3 == 0:
                v0 = subvalues[i]
                v1 = subvalues[i+1]
                v2 = subvalues[i+2]
                al = [v0,v1,v2]
                m = np.mean(al)
                new_vals.append(m)

        one = values[0]
        two = values[1]
        three = values[2]

        early = [one, two, three]
        early += new_vals

        print early

        timeSteps = xrange(len(early))
        timeSteps = [i-2 for i in timeSteps]


        plt.scatter(timeSteps, early, color=color, label=lab, marker=marker)
        plt.plot(timeSteps, early, color=color, linestyle='-')
        baseTitle = 'Time Series'
        baseXLabel = 'Quarters'
        baseYLabel = 'Price level (normalized)'
        plt.suptitle(baseTitle, fontsize=fontSize_supTitle)
        plt.xlabel(baseXLabel, fontsize=fontSize_xlabel)
        plt.ylabel(baseYLabel, fontsize=fontSize_ylabel)
        plt.tick_params(axis='both', labelsize=fontSize_ticks)
        plt.ticklabel_format(style='plain',axis='both',useOffset=False)
        plt.legend(loc=1,fontsize=fontSize_xlabel)
        plt.grid()
        plt.ylim([0.997,1.003])

        saveName =  'new_timeSeries_firms' + str(e) + '.png'
        plt.savefig(saveName, bbox_inches='tight')
        plt.close()

        print "done new time series"
        os.chdir('..')

def meanError():
    legend = None
    legendSize = None

    fontSize_title = 20
    fontSize_xlabel = 16
    fontSize_ylabel = 16
    fontSize_ticks = 16

    names = ['normalized_PCE_s_economy_Firms_', 'normalized_PCE_monetaryShock_exponent_economy_Firms_']
    title = {'normalized_PCE_s_economy_Firms_': 'Variation in size of shock',
             'normalized_PCE_monetaryShock_exponent_economy_Firms_': 'Variation in distribution of shock'}
    xlabel = {'normalized_PCE_s_economy_Firms_': 'Long-run price change (percent)',
              'normalized_PCE_monetaryShock_exponent_economy_Firms_': r'$\theta$'}
    ylabel = {'normalized_PCE_s_economy_Firms_': 'Short-run price change (percent)',
              'normalized_PCE_monetaryShock_exponent_economy_Firms_': 'Short-run price change (percent)'}
    varName = {'normalized_PCE_s_economy_Firms_': 's',
               'normalized_PCE_monetaryShock_exponent_economy_Firms_': 'monetaryShock_exponent'}

    allfiles = ['normalized_PCE_monetaryShock_exponent_economy_Firms_CD_static.csv',
                'normalized_PCE_s_economy_Firms_CD_static.csv']


    plots.mean_error(csv_file='pce_s.csv',
                         statistic='priceLevel',
                         xlabel=r'Size of shock $s$ (percent)',
                         ylabel='Price level change (percent)',
                         title_text='Variation in size of shock',
                         color='black',
                         save_name='meanError_s',
                         positive=False,
                         ymin=None,
                         ymax=None,
                         xmin=None,
                         xmax=None,
                         inverse=False,
                         legend=legend,
                         legendSize=legendSize,
                         fontSize_title=fontSize_title,
                         fontSize_xlabel=fontSize_xlabel,
                         fontSize_ylabel=fontSize_ylabel,
                         fontSize_ticks=fontSize_ticks,
                         child_directory='plots_pricePuzzle',
                         digits_yAxis=False,
                         maxXticks=4,
                         maxYticks=5,
                         delValuesTreshold=False,
                         cutOff=None)


    plots.mean_error(csv_file='pce_exponent.csv',
                     statistic='priceLevel',
                     xlabel=r'$\theta$',
                     ylabel='Price level change (percent)',
                     title_text=r'Variation in $\theta$',
                     color='black',
                     save_name='meanError_theta',
                     positive=True,
                     ymin=None,
                     ymax=None,
                     xmin=None,
                     xmax=None,
                     inverse=False,
                     legend=legend,
                     legendSize=legendSize,
                     fontSize_title=fontSize_title,
                     fontSize_xlabel=fontSize_xlabel,
                     fontSize_ylabel=fontSize_ylabel,
                     fontSize_ticks=fontSize_ticks,
                     child_directory='plots_pricePuzzle',
                     digits_yAxis=False,
                     maxXticks=6,
                     maxYticks=5,
                     delValuesTreshold=False,
                     cutOff=None)

def longShort_firms():
    fontSize_supTitle = 20
    fontSize_xlabel = 16
    fontSize_ylabel = 16
    fontSize_ticks = 16

    names = ['normalized_PCE_s_economy_Firms_', 'normalized_PCE_monetaryShock_exponent_economy_Firms_']
    title = {'normalized_PCE_s_economy_Firms_':'Variation in size of shock',
             'normalized_PCE_monetaryShock_exponent_economy_Firms_':'Variation in distribution of shock'}
    xlabel = {'normalized_PCE_s_economy_Firms_':'Long-run price change (percent)',
             'normalized_PCE_monetaryShock_exponent_economy_Firms_':r'$\theta$'}
    ylabel = {'normalized_PCE_s_economy_Firms_': 'Short-run price change (percent)',
              'normalized_PCE_monetaryShock_exponent_economy_Firms_': 'Short-run price change (percent)'}
    varName = {'normalized_PCE_s_economy_Firms_':'s',
             'normalized_PCE_monetaryShock_exponent_economy_Firms_':'monetaryShock_exponent'}
    for n in names:
        files = {'PCE':n}
        colors = {'PCE':'black'}#, 'CPI':'midnightblue'}#, 'CPIXH':'cornflowerblue'}
        markers = {'PCE':'o'}#, 'CPI':'*'}#, 'CPIXH':'s'}
        baseTitle = title[n]

        baseXLabel = xlabel[n]
        baseYLabel = ylabel[n]



        for name in files:
            csv_file = files[name]
            csv_file += 'CD_static.csv'
            data = pd.read_csv(csv_file)
            data = pd.DataFrame(data)
            rows = data.shape[0]
            short = 8

            xValues = []
            yValues = []
            for r in xrange(rows):
                d = data.iloc[r][:]
                shortValue = d['t'+str(short)]
                shortValue = float(shortValue)
                var = varName[n]
                longValue = d[var]
                longValue = float(longValue)
                if var == 'monetaryShock_exponent':
                    #if longValue<1:
                    xValues.append(longValue)
                    yValues.append(100 * (shortValue-1))
                else:
                    xValues.append(longValue*100)
                    yValues.append(100 * (shortValue - 1))
            color = colors[name]
            marker = markers[name]
            plt.scatter(xValues, yValues, color=color,label=name,marker=marker)
            #plt.plot(xValues, yValues, color=color,linestyle=':')
        child_directory = 'plots_pricePuzzle'
        if not os.path.exists(child_directory):
            os.makedirs(child_directory)
        os.chdir(child_directory)
        plt.suptitle(baseTitle, fontsize = fontSize_supTitle)
        plt.xlabel(baseXLabel, fontsize=fontSize_xlabel)
        plt.ylim(ymin=0)
        plt.ylabel(baseYLabel,fontsize=fontSize_ylabel)
        plt.tick_params(axis='both', labelsize=fontSize_ticks)
        plt.ticklabel_format(style='plain', axis='both', useOffset=False)
        #plt.locator_params(axis='x',nbins=7)
        #plt.locator_params(axis='y',nbins=7)
        minX = min(xValues)
        maxX = max(xValues)

        #plt.xlim([minX,maxX])
        #plt.legend()
        plt.grid()
        saveName = 'LongShort' + n
        plt.savefig(saveName, bbox_inches='tight')
        plt.close()
        os.chdir('..')

def Isticky():
	file = 'normalized_PCE_var_prob_price_change_economy_network_CD_static.csv'
	data = pd.read_csv(file)
	data = pd.DataFrame(data)
	rows = data.shape[0]

	time_series_list = []

	count = 9
	v = 0.3
	for r in xrange(rows):
	    val = data.iloc[r][0]
	    #if 0.4999999 <= val <= 0.5000001:
	    if v-0.000001 <= val <= v + 0.000001:
	    # use 1 instead of these values
	        if count > 0:
	            d = list(data.iloc[r][1:])
	            time_series_list.append(d)
	            count -= 1

	quarterly_series_list = []


	for series in time_series_list:		
		howMany = 34
		series = series[2:]

		temp = [1,1]
		for i in xrange(howMany):
			if i % 3 == 0:
				month1 = series[i]
				month2 =  series[i+1]
				month3 =  series[i+2]
				mean_val = (month1 + month2 + month3) /3
				temp.append(mean_val)
		quarterly_series_list.append(temp)


	file_name = "Isticky_instances"
	column_names = ['param','PCE_quarterly']

	with open('%s.csv' % file_name, 'wb') as data_csv:
	    writer_data = csv.writer(data_csv, delimiter=',')
	    writer_data.writerow(column_names)

	    for ser in quarterly_series_list:
	        data = [v,ser]
	        writer_data.writerow(data)
	time_end = len(ser)
	plots.time_series_instances(csv_file="Isticky_instances.csv", 
	                            variable_name="PCE_quarterly", 
	                            variable_name_plot="PCE", 
	                            n_rows=2, 
	                            n_columns=2, 
	                            time_begin=0, 
	                            time_end=time_end, 
	                            time_shift=1,
	                            xlabel='Quarters', 
	                            ylabel='PCE (Normalized)', 
	                            title='PCE time series (exogenous price-stickiness)', 
	                            fontSize_supTitle=18, 
	                            fontSize_title=14, 
	                            fontSize_xlabel=14, 
	                            fontSize_ylabel=14,
	                            fontSize_ticks=12, 
	                            save_name=None, 
	                            color='black', 
	                            maxXticks=4, 
	                            maxYticks=4, 
	                            child_directory="plots_pricePuzzle")



	file = 'normalized_PCE_var_prob_price_change_economy_network_CD_static.csv'
	data = pd.read_csv(file)
	data = pd.DataFrame(data)
	rows = data.shape[0]
	data_dict = {}

	

	for r in xrange(rows):
		val = data.iloc[r]["var_prob_price_change"]
		data_dict[val] = []

	for r in xrange(rows):
		val = data.iloc[r]["var_prob_price_change"]
		d = list(data.iloc[r][:])
		max_price = max(d)
		data_dict[val].append(max_price)

	num = len(data_dict[val])

	

	
	child_directory = 'plots_pricePuzzle'
	os.chdir(child_directory)

	plt.xlabel(r'$\gamma$')
	plt.ylabel('Maximum PCE')
	plt.title("Exogeneous price stickiness")
	plt.ticklabel_format(useOffset=False, useLocale=True, useMathText=True)
	plt.grid()
	png_name = "stickyI_param"
	plt.savefig('%s.png' % png_name,bbox_inches='tight')
	plt.close()


	width = 0.01
	values = data_dict.keys()
	values = sorted(values)
	x_temp = []
	y_temp = []


	vals = data_dict.keys()
	vals = sorted(vals)

	
	new_data_dict = {}
	count = 0
	for k in vals:
		count += 1
		if count % 2 != 0:
			new_data_dict[k] = data_dict[k]
			


	num = 100
	widths = tuple([width] * num)
	plt.boxplot(new_data_dict.values(),positions = new_data_dict.keys(),widths=widths)
	
	plt.xlabel(r'Heterogeneity $\gamma$',fontsize=14)
	plt.ylabel('Maximum PCE',fontsize=14)
	plt.title("Exogeneous price stickiness",fontsize=18)
	plt.xlim(xmin=-0.05,xmax=0.45)
	plt.ticklabel_format(useOffset=False, useLocale=True, useMathText=True)
	plt.ylim(ymin=0.9990,ymax=1.003)
	plt.grid()
	png_name = "boxPlot_stickyI_param"
	plt.savefig('%s.png' % png_name,bbox_inches='tight')
	plt.close()
	os.chdir('..')

def IIsticky():
	file = 'normalized_PCE_price_sensitivity_economy_network_CD_static.csv'
	data = pd.read_csv(file)
	data = pd.DataFrame(data)
	rows = data.shape[0]

	# select rows with sensitivity at 0.5

	time_series_list = []

	count = 9
	v = 0.1
	for r in xrange(rows):
	    val = data.iloc[r][0]
	    #if 0.4999999 <= val <= 0.5000001:
	    if v-0.000001 <= val <= v + 0.000001:
	    # use 1 instead of these values
	        if count > 0:
	            d = list(data.iloc[r][1:])
	            time_series_list.append(d)
	            count -= 1

	quarterly_series_list = []


	for series in time_series_list:
		#howMany = len(series)-25
		howMany = 34
		series = series[2:]

		temp = [1,1]
		for i in xrange(howMany):
			if i % 3 == 0:
				month1 = series[i]
				month2 =  series[i+1]
				month3 =  series[i+2]
				mean_val = (month1 + month2 + month3) /3
				temp.append(mean_val)
		quarterly_series_list.append(temp)


	file_name = "IIsticky_instances"
	column_names = ['param','PCE_quarterly']

	with open('%s.csv' % file_name, 'wb') as data_csv:
	    writer_data = csv.writer(data_csv, delimiter=',')
	    writer_data.writerow(column_names)

	    for ser in quarterly_series_list:
	        data = [v,ser]
	        writer_data.writerow(data)
	time_end = len(ser)
	plots.time_series_instances(csv_file="IIsticky_instances.csv", 
	                            variable_name="PCE_quarterly", 
	                            variable_name_plot="PCE", 
	                            n_rows=2, 
	                            n_columns=2, 
	                            time_begin=0, 
	                            time_end=time_end, 
	                            time_shift=1,
	                            xlabel='Quarters', 
	                            ylabel='PCE (Normalized)', 
	                            title='PCE time series (endogeneous price-stickiness)', 
	                            fontSize_supTitle=18, 
	                            fontSize_title=14, 
	                            fontSize_xlabel=14, 
	                            fontSize_ylabel=14,
	                            fontSize_ticks=12, 
	                            save_name=None, 
	                            color='black', 
	                            maxXticks=4, 
	                            maxYticks=4, 
	                            child_directory="plots_pricePuzzle")



	file = 'normalized_PCE_price_sensitivity_economy_network_CD_static.csv'
	data = pd.read_csv(file)
	data = pd.DataFrame(data)
	rows = data.shape[0]
	data_dict = {}

	# no need to show heatmap over time steps.
	# just plot max values for each parameter combination 

	for r in xrange(rows):
		val = data.iloc[r]["price_sensitivity"]
		data_dict[val] = []

	for r in xrange(rows):
		val = data.iloc[r]["price_sensitivity"]
		d = list(data.iloc[r][:])
		max_price = max(d)
		data_dict[val].append(max_price)

	num = len(data_dict[val])

	x_values = []
	y_values = []
	for val in data_dict:
		if val <= 0.5:
			temp = [val] * num
			x_values += temp
			y_values += data_dict[val]

	#plt.scatter(x_values,y_values,color='black',alpha=0.5,s=1)
	child_directory = 'plots_pricePuzzle'
	os.chdir(child_directory)

	width = 0.025
	values = data_dict.keys()
	values = sorted(values)
	x_temp = []
	y_temp = []



	new_data_dict = {}

	for k in data_dict:
		if k <=0.5:
			new_data_dict[k] = data_dict[k]



	num = 100
	widths = tuple([width] * num)
	plt.boxplot(new_data_dict.values(),positions = new_data_dict.keys(),widths=widths)
	

	plt.xlabel(r'Scaling $\psi$',fontsize=14)
	plt.ylabel('Maximum PCE',fontsize=14)
	plt.title("Endogeneous price-stickiness",fontsize=18)
	plt.xlim(xmin=0,xmax=0.55)
	plt.ticklabel_format(useOffset=False, useLocale=True, useMathText=True)
	plt.ylim(ymin=0.9990,ymax=1.003)
	plt.grid()
	png_name = "boxPlot_stickyII_param"
	plt.savefig('%s.png' % png_name,bbox_inches='tight')
	plt.close()
	os.chdir('..')


def all():
    child_directory = 'data_pricePuzzle'

    if not os.path.exists(child_directory):
        os.makedirs(child_directory)
    os.chdir(child_directory)
    #meanError()
    #heatMap()

    time_series()
    #Isticky()
    #IIsticky()

    os.chdir('..')

all()

