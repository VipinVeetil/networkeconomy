from __future__ import division
import main
import cPickle
import parameters as p
import run
import write_network
import csv
import os
import matplotlib.pyplot as plt
from collections import OrderedDict
import copy

def distributions():
  iterations = 100
  cores = 10
  transientTimeSteps = 1
  time_steps = 500

  baseParameters={'probability_weights_change':1,
                'production_function':'CES',
                'price_stickiness_type':'probabilistic',
                #'probability_price_change':0.5,
                'monetaryShock_exponent':1,
                'load_pickle':True,
                'record_transient_data':True,
                'monetary_shock_always': True,
                'network_from_file':False,
                'household_preference_homogeneous': False,
                'representative_household': True,
                'transient_time_steps': transientTimeSteps,
                'time_steps': time_steps,
                'z':0.9}              

  
  var = {'economy': ['sum_output_equilibrium_prices','wealth'],'firm': None, 'household': None}

  variables = {'probability_price_change':var}
  iterations_parameters = {'probability_price_change':iterations}
  cores_parameters = {'probability_price_change':cores}
  other_parameters = {'probability_price_change':baseParameters}

  parameter_ranges = {'probability_price_change':[0.25,0.26]}
  parameter_increments = {'probability_price_change':0.1}

  main.main(network_name = 'SF', 
               production_function = 'CES_distribution', 
               time_series = {'economy':True, 'firm':False, 'household': False},
               parameter_names=['probability_price_change'],
               parameter_ranges=parameter_ranges,
               parameter_increments=parameter_increments,
               variables=variables,
               other_parameters_= other_parameters,
               iterations_=iterations_parameters,
               cores_=cores_parameters,
               model_name='network_economy',
               model_url='url',
               paper_name='monetaryPlucking',
               paper_url='url')


def convergence():
  iterations = 30
  cores = 15
  # measure the convergence of the system for different values of sigma
  transientTimeSteps = 500
  time_steps = 500

  baseParameters={'probability_weights_change':1,
                  'production_function':'CES',                  
                  'load_pickle':False,
                  'record_transient_data':True,
                  'network_from_file':False,
                  'household_preference_homogeneous': False,
                  'representative_household': True,
                  'transient_time_steps': transientTimeSteps,
                  'time_steps': time_steps}
  
  var = {'economy': ['mean_price_change'],'firm': None, 'household': None}

  variables = {'sigma':var}
  iterations_parameters = {'sigma':iterations}
  cores_parameters = {'sigma':cores}
  other_parameters = {'sigma':baseParameters}

  parameter_ranges = {'sigma':[0.01,0.51]}
  parameter_increments = {'sigma':0.01}


  main.main(network_name = 'SF', 
               production_function = 'CES', 
               time_series = {'economy':True, 'firm':False, 'household': False},
               parameter_names=['sigma'],
               parameter_ranges=parameter_ranges,
               parameter_increments=parameter_increments,
               variables=variables,
               other_parameters_= other_parameters,
               iterations_=iterations_parameters,
               cores_=cores_parameters,
               model_name='network_economy',
               model_url='url',
               paper_name='monetaryPlucking',
               paper_url='url')

  
    
def sweep():     
    parameter_sweep(production_function='CES',
                    price_stickiness_type='probabilistic',
                    iterations=10,
                    cores=10,
                    parameter_names=['probability_price_change'],
                    parameter_ranges={'probability_price_change':[0.1,0.91]},
                    parameter_increments={'probability_price_change':0.1})   


        
def parameter_sweep(production_function,
                    price_stickiness_type,
                    iterations,
                    cores,
                    parameter_names,
                    parameter_ranges,
                    parameter_increments):
    
    
    transientTimeSteps = 0
    time_steps = 500
    

    baseParameters={'probability_weights_change':1,
                    'production_function':production_function,
                    'price_stickiness_type':price_stickiness_type,
                    'probability_price_change':0.5,
                    'monetaryShock_exponent':1,
                    'load_pickle':True,
                    'record_transient_data':True,
                    'monetary_shock_always': True,
                    'network_from_file':False,
                    'household_preference_homogeneous': False,
                    'representative_household': True,
                    'transient_time_steps': transientTimeSteps,
                    'time_steps': time_steps,
                    'z':0.9}
                
    

    var = {'economy': ['sumOutput', 'wealth', 'welfare_mean','sum_output_equilibrium_prices'],'firm': None, 'household': None}
        
    variables = {}
    iterations_parameters = {}
    cores_parameters = {}
    other_parameters = {}
    base = {}
     
    for n in parameter_names:
        variables[n] = var
        iterations_parameters[n] = iterations
        cores_parameters[n] = cores
        other_parameters[n] = baseParameters
        baseCopy = copy.deepcopy(baseParameters)
        del baseCopy[n]
        base[n] = baseCopy 
    
   
      
    nameParam = parameter_names[0]
    
    production_function =  str(production_function)
    
    """
    directory_name = 'data' + '_' + 'monetaryPlucking'
    if not os.path.exists(directory_name):
        os.makedirs(directory_name)
    os.chdir(directory_name)
    """
    main.main(network_name = 'SF', 
               production_function = production_function, 
               time_series = {'economy':True, 'firm':False, 'household': False},
               parameter_names=parameter_names,
               parameter_ranges=parameter_ranges,
               parameter_increments=parameter_increments,
               variables=variables,
               other_parameters_= other_parameters,
               iterations_=iterations_parameters,
               cores_=cores_parameters,
               model_name='network_economy',
               model_url='url',
               paper_name='monetaryPlucking',
               paper_url='url')



    #os.chdir('..')
    



def economyPickle(production_function,
                  price_stickiness_type,
                  monetaryShock_exponent,
                  probability_price_change,
                  probability_weights_change,
                  network_from_file,
                  num_firms):


    parameters = p.Parameters()
    parameters.write_pickle = True
    
    parameters.production_function = production_function
    parameters.price_stickiness_type = price_stickiness_type
    parameters.probability_price_change = probability_price_change
    parameters.monetaryShock_exponent = monetaryShock_exponent
    parameters.probability_weights_change=probability_weights_change
    parameters.network_from_file = False
    
    parameters.n = num_firms
    parameters.household_preference_homogeneous = False

    parameters.transient_time_steps = 100
    parameters.time_steps = 100
    
    parameters.monetary_shock_always = True
    parameters.monetary_shock_mean = 0
    parameters.monetary_shock_variance = 0.01
    
    parameters.monetaryShock_exponent = 1 

    parameters.data_time_series['economy'] = True
    parameters.data_time_series['firm'] = True
    
    #parameters.record_variables['economy']['PCE'] = True
    #parameters.record_variables['economy']['finalOutput'] = True
    parameters.record_variables['economy']['sumOutput'] = True
    parameters.record_variables['economy']['sum_output_equilibrium_prices'] = True
    parameters.record_variables['economy']['welfare_mean'] = True
    parameters.record_variables['economy']['wealth'] = True
    #parameters.record_variables['economy']['finalOutput_equilibrium_prices'] = True
    
    parameters.monetaryShock_stochastic=True
    parameters.randomWeightShare = 0
    parameters.z = 0.9

    run_instance = run.Run(parameters)
    run_instance.create_economy()
    run_instance.transient()
    run_instance.time_steps()
    economy = run_instance.samayam.economy
    
    if probability_price_change < 1 and probability_weights_change == 1:
        a = 'stickyPrice'
    elif probability_price_change == 1 and probability_weights_change < 1:
        a = 'stickyWeight'
    elif probability_price_change == 1 and probability_weights_change == 1:
        a = 'flexible'
        
    if network_from_file:
      file_name = 'economyPickle_networkUS' + a
    else:
      file_name = 'economyPickle_' + a
    
   
    directory_name = 'data' + '_' + 'monetaryPlucking'
    if not os.path.exists(directory_name):
        os.makedirs(directory_name)
    os.chdir(directory_name)

    with open('%s.cPickle' % file_name, 'wb') as econ:
        cPickle.dump(economy, econ, protocol=cPickle.HIGHEST_PROTOCOL)
    os.chdir('..')



def pickle_economy():    

    """    
    economyPickle(production_function='CES',
                  price_stickiness_type='probabilistic',
                  monetaryShock_exponent=1,
                  probability_price_change=0.5,
                  probability_weights_change=1,
                  network_from_file=False,
                  num_firms=10000)
    
    print "sticky prices"
    """

    economyPickle(production_function='CD',
                  price_stickiness_type='probabilistic',
                  monetaryShock_exponent=1,
                  probability_price_change=0.5,
                  probability_weights_change=1,
                  network_from_file=True,
                  num_firms=1000)
    
    print "network"




def all():
  pickle_economy()
  #sweep()
  #convergence()
  #distributions()

all()