from __future__ import division
import os.path, sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir))
import os
import processData
import pandas as pd
import numpy as np
import csv
import collections
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
import scipy.stats as stats

def regress():
    csv_file = 'normalized_sum_output_equilibrium_prices_probability_price_change_economy_SF_CES.csv'
    data = pd.read_csv(csv_file)
    data = pd.DataFrame(data)
    rows = data.shape[0]
    cols = data.shape[1]

    file_name = 'regression.csv'

    with open('%s' % file_name, 'wb') as data_csv:
        writer_data = csv.writer(data_csv, delimiter=',')
        writer_data.writerow(['prob price change', 'slopeTrue', 'interceptTrue', 'pValueTrue','rValueTrue','stderrTrue','slopeFalse', 'interceptFalse', 'pValueFalse','rValueFalse','stderrFalse'])

        for r in xrange(rows):
            slopes = []
            intercepts = []
            pValues = []

            turning_points = []
            l = []
            v = data.iloc[r,0]
            for i in xrange(1,cols-1):
                if data.iloc[r,i] > data.iloc[r,i-1]:
                    l.append(1)            
                elif data.iloc[r,i] < data.iloc[r,i-1]:
                    l.append(-1)
                


            for j in xrange(1,len(l)):
                if l[j] != l[j-1]:
                    turning_points.append(j)

            #print l
            #print "turning_points", turning_points
            turning_points_outputs = []

            for t in turning_points:
                output = data.iloc[r,t]
                turning_points_outputs.append(output)

            output_differences = []

            for i in xrange(0, len(turning_points_outputs)-1):
                q0 = turning_points_outputs[i]
                q1 = turning_points_outputs[i+1]
                diff = q1-q0
                output_differences.append(diff)


            recessions = []
            booms = []
            for i in xrange(0,len(output_differences)):
                if output_differences[i] < 0:
                    recessions.append(i)
                if output_differences[i] > 0:
                    booms.append(i)

            first_recession = min(recessions)
            first_boom = min(booms)


            # x and y variables for regression on boom, with x as regress
            x = []
            y = []
            count = 0
            for i in xrange(first_recession,len(output_differences)-1):
                if count == 0:
                    x.append(output_differences[i])
                    y.append(output_differences[i+1])
                    count += 1
                else:
                    count = 0


            #print output_differences
            #regressor = LinearRegression()  
            #regressor.fit(x, y) 
            res_pluckTrue = stats.linregress(x,y)
            
            #plt.scatter(x,y)
            #plt.xlim([-0.0005,0.0005])
            #plt.ylim([-0.0005,0.0005])
            #plt.show()


            x = []
            y = []
            count = 0
            for i in xrange(first_boom,len(output_differences)-1):
                if count == 0:
                    x.append(output_differences[i])
                    y.append(output_differences[i+1])
                    count += 1
                else:
                    count = 0


            #print output_differences
            #regressor = LinearRegression()  
            #regressor.fit(x, y) 
            res_pluckFalse = stats.linregress(x,y)
            #print res
            #plt.scatter(x,y)
            #plt.xlim([-0.0005,0.0005])
            #plt.ylim([-0.0005,0.0005])
            #plt.show()

            true_slope = res_pluckTrue.slope
            true_intercept = res_pluckTrue.intercept
            true_p = res_pluckTrue.pvalue
            true_r = res_pluckTrue.rvalue
            true_stdErr = res_pluckTrue.stderr

            false_slope = res_pluckFalse.slope
            false_intercept = res_pluckFalse.intercept
            false_p = res_pluckFalse.pvalue
            false_r = res_pluckFalse.rvalue
            false_stdErr = res_pluckFalse.stderr

            #print res_pluckTrue

            writer_data.writerow([v, res_pluckTrue.slope, res_pluckTrue.intercept, res_pluckTrue.pvalue, true_r, true_stdErr,
                res_pluckFalse.slope, res_pluckFalse.intercept, res_pluckFalse.pvalue, false_r, false_stdErr])















def distribution():
    csv_file = 'normalized_sum_output_equilibrium_prices_probability_price_change_economy_SF_CES_distribution.csv'
    data = pd.read_csv(csv_file)
    data = pd.DataFrame(data)

    all_increase = collections.OrderedDict({})
    all_decrease = collections.OrderedDict({})
    param = [0.25]

    values = np.arange(0,0.01,0.00001)
   


    rows = data.shape[0]

    for p in param:
        dict_vals_increase = collections.OrderedDict({})
        dict_vals_decrease = collections.OrderedDict({})
        for v in values:
            dict_vals_increase[v] = 0
            dict_vals_decrease[v] = 0

        for r in xrange(rows):
            if data.iloc[r,0] == p:
                d = data.iloc[r,8:]
                for v in values:
                    for i in d:
                        if (i-1)>0:
                            if (i-1) > v:
                                dict_vals_increase[v] += 1                            
                        elif (i-1)<v:
                            if (1-i) > v:
                                dict_vals_decrease[v] += 1

        all_increase[p] = dict_vals_increase
        all_decrease[p] = dict_vals_decrease




    # write the dictionary and its values of increase and decrease

    file_name = 'distribution.csv'

    with open('%s' % file_name, 'wb') as data_csv:
        writer_data = csv.writer(data_csv, delimiter=',')
        writer_data.writerow(['prob price change', 'increaseDecrease'] + list(values))
        for p in param:
            dist_increase = all_increase[p] 
            dist_decrease = all_decrease[p]
            

            vals_increase = dist_increase.values()

            prob_increase = [i/vals_increase[0] for i in vals_increase]

            vals_decrease = dist_decrease.values()
            prob_decrease = [i/vals_decrease[0] for i in vals_decrease]

            writer_data.writerow([p, 'increase'] + prob_increase)
            writer_data.writerow([p, 'decrease'] + prob_decrease)



    #plt.plot(list(values[0:30]),prob_decrease[0:30], color='pink')
    #plt.plot(list(values[0:30]),prob_increase[0:30], color='blue')
    #plt.show()


def convergence():
    file = 'sigma_economy_SF_CES.csv'
    processData.convergence_time(csv_file=file,
                                         thresholds=[5,10,12],
                                         variable='mean_price_change',
                                         save_name=None)



def normalizeOutputIndex_firms():    
    files = ['probability_price_change_economy_SF_CES.csv',
            'probability_price_change_economy_SF_CES_distribution.csv']

    transientTime = 0
    for csv_file in files:
        variables = ['sum_output_equilibrium_prices']
        normPosition = 0
        processData.normalize(csv_file, variables, normPosition, transientTime)


def process():
    directory_name = 'data_monetaryPlucking'
    os.chdir(directory_name)
    
    convergence()
    normalizeOutputIndex_firms()
    distribution()
    regress()

    os.chdir('..')

process()