from __future__ import division
import pandas as pd
import matplotlib.pyplot as plt
import random as random
import numpy as np
from math import log
import math
import ast
from itertools import chain
from scipy import stats
from matplotlib.ticker import FormatStrFormatter
import collections
import os
import csv
import plots
import cPickle

def distribution():
    fontSize_supTitle = 20
    fontSize_xlabel = 16
    fontSize_ylabel = 16
    fontSize_ticks = 16
    legendSize = 12

    file = 'distribution.csv'
    data = pd.read_csv(file)
    data = pd.DataFrame(data)
    rows = data.shape[0]

    xVals = list(data.head())
    xVals = xVals[2:]
    xVals = [float(i) for i in xVals]

    increase = list(data.iloc[0,2:])
    decrease = list(data.iloc[1,2:])

    #increase = increase[0:1000]
    #decrease = decrease[0:1000]
    #xVals = xVals[0:1000]

    plt.scatter(xVals,increase,label='increase',marker='.',color='royalblue',s=1)
    plt.scatter(xVals,decrease,label='decrease',marker='.',color='salmon',s=1)

    baseTitle = 'Cumulative distribution of ouput changes'
    baseXLabel = 'Change in output (proportion)'
    baseYLabel = 'Probability'
    child_directory = 'plots_monetaryPlucking'
    if not os.path.exists(child_directory):
        os.makedirs(child_directory)
    os.chdir(child_directory)
    plt.suptitle(baseTitle, fontsize=fontSize_supTitle)
    plt.xlabel(baseXLabel, fontsize=fontSize_xlabel)    
    plt.ylabel(baseYLabel, fontsize=fontSize_ylabel)
    plt.tick_params(axis='both', labelsize=legendSize)
    plt.ticklabel_format(style='plain',axis='both',useOffset=False)
    plt.legend(loc=5,fontsize=fontSize_xlabel,markerscale=10.)
    plt.grid()
    #plt.xlim([0,0.004])          
    saveName = 'distribution.png'
    plt.savefig(saveName, bbox_inches='tight')
    plt.close()
    os.chdir('..')







def regression():
    fontSize_supTitle = 20
    fontSize_xlabel = 16
    fontSize_ylabel = 16
    fontSize_ticks = 16
    legendSize = 12

    file = 'regression.csv'
    data = pd.read_csv(file)
    data = pd.DataFrame(data)
    rows = data.shape[0]

    param = []
    true_pVal = []
    false_pVal = []

    true_slope = []
    false_slope = []

    true_rVal = []
    false_rVal = []

    for r in xrange(rows):
        p = data.iloc[r,0]

        trueP = data.iloc[r]["pValueTrue"]
        falseP = data.iloc[r]["pValueFalse"]

        trueSlope = data.iloc[r]["slopeTrue"]
        falseSlope = data.iloc[r]["slopeFalse"]

        trueR = data.iloc[r]["rValueTrue"]    
        falseR = data.iloc[r]["rValueFalse"]

       # trueR = [i**2 for i in trueR]
        #falseR = [i**2 for i in falseR]



        param.append(p)

        true_pVal.append(trueP)
        false_pVal.append(falseP) 

        true_slope.append(trueSlope)
        false_slope.append(falseSlope)

        true_rVal.append(trueR)
        false_rVal.append(falseR) 


    true_rVal = [i ** 2 for i in true_rVal]
    false_rVal = [i ** 2 for i in false_rVal]


    
    plt.scatter(param,false_pVal,color='salmon',label='boom on recession')
    plt.scatter(param,true_pVal,color='royalblue', label='recession on boom')
    
    baseTitle = 'P values of plucking regressions'
    baseXLabel = 'Probability of price change'
    baseYLabel = 'P value'
    child_directory = 'plots_monetaryPlucking'
    if not os.path.exists(child_directory):
        os.makedirs(child_directory)
    os.chdir(child_directory)
    plt.suptitle(baseTitle, fontsize=fontSize_supTitle)
    plt.xlabel(baseXLabel, fontsize=fontSize_xlabel)
    plt.ylabel(baseYLabel, fontsize=fontSize_ylabel)
    plt.tick_params(axis='both', labelsize=legendSize)
    plt.ticklabel_format(style='plain',axis='both',useOffset=False)
    plt.legend(loc=5,fontsize=fontSize_xlabel)
    plt.grid()
    #plt.ylim([0.999,1.001])          
    saveName = 'pValues.png'
    plt.savefig(saveName, bbox_inches='tight')
    plt.close()
    os.chdir('..')


    plt.scatter(param,true_rVal,color='royalblue', label='recession on boom')
    plt.scatter(param,false_rVal,color='salmon',label='boom on recession')
    
    baseTitle = r'$R^2$ values of plucking regressions'
    baseXLabel = 'Probability of price change'
    baseYLabel = 'R squared'
    child_directory = 'plots_monetaryPlucking'
    if not os.path.exists(child_directory):
        os.makedirs(child_directory)
    os.chdir(child_directory)
    plt.suptitle(baseTitle, fontsize=fontSize_supTitle)
    plt.xlabel(baseXLabel, fontsize=fontSize_xlabel)
    plt.ylabel(baseYLabel, fontsize=fontSize_ylabel)
    plt.tick_params(axis='both', labelsize=legendSize)
    plt.ticklabel_format(style='plain',axis='both',useOffset=False)
    plt.legend(loc=5,fontsize=fontSize_xlabel)
    plt.grid()
    #plt.ylim([0.999,1.001])          
    saveName = 'rValues.png'
    plt.savefig(saveName, bbox_inches='tight')
    plt.close()
    os.chdir('..')

    plt.scatter(param,true_slope,color='royalblue', label='recession on boom')
    plt.scatter(param,false_slope,color='salmon',label='boom on recession')
    
    baseTitle = 'Slope of plucking regressions'
    baseXLabel = 'Probability of price change'
    baseYLabel = 'Slope'
    child_directory = 'plots_monetaryPlucking'
    if not os.path.exists(child_directory):
        os.makedirs(child_directory)
    os.chdir(child_directory)
    plt.suptitle(baseTitle, fontsize=fontSize_supTitle)
    plt.xlabel(baseXLabel, fontsize=fontSize_xlabel)
    plt.ylabel(baseYLabel, fontsize=fontSize_ylabel)
    plt.tick_params(axis='both', labelsize=legendSize)
    plt.ticklabel_format(style='plain',axis='both',useOffset=False)
    plt.legend(loc=5,fontsize=fontSize_xlabel)
    plt.grid()
    #plt.ylim([0.999,1.001])          
    saveName = 'slope.png'
    plt.savefig(saveName, bbox_inches='tight')
    plt.close()
    os.chdir('..')





def convergence():
    legendSize = 18
    fontSize_title = 20
    fontSize_xlabel = 16
    fontSize_ylabel = 16
    fontSize_ticks = 16

    file = 'sigma_economy_SF_CES_convergence.csv'
    xlabel = r'CES exponent $\sigma$'

    plots.convergence_mean_error(csv_file=file,
                             thresholds=[5,10,12],
                             legend=True,
                             legendSize=legendSize,
                             legend_location=9,
                             fontSize_title=fontSize_title,
                             fontSize_xlabel=fontSize_xlabel,
                             fontSize_ylabel=fontSize_ylabel,
                             fontSize_ticks=fontSize_ticks,
                             child_directory='plots_monetaryPlucking',
                             digits_yAxis=False,
                             maxXticks=5,
                             maxYticks=5,
                             colors=['salmon', 'royalblue', 'midnightblue'],
                             title_text='Convergence',
                             xlabel=xlabel,
                             ylabel='Time steps',
                             ymin=None,
                             ymax=None,
                             xmin=None,
                             xmax=None,
                             save_name=None,
                             cutOff=None)

def time_series_outputs():
    fontSize_supTitle = 20
    fontSize_xlabel = 16
    fontSize_ylabel = 16
    fontSize_ticks = 16
    
    files = ['economyPickle_stickyPrice.cPickle',
            'economyPickle_networkUSstickyPrice.cPickle']
    
    
    
    names = {'economyPickle_stickyPrice.cPickle':'sticky prices',
            'economyPickle_networkUSstickyPrice.cPickle':'USEconomy'}


    variables = ['sum_output_equilibrium_prices']
    
    timeSteps = range(1,101)
    
    for file in files:
        n = names[file]
        for var in variables:        
            E = cPickle.load(open(file, "rb"))
            out = getattr(E, var)
            print var
            norm_out = out[0]
            values = [i/norm_out for i in out]            
            plt.scatter(timeSteps, values, label='Aggregate output',color='royalblue')
            plt.plot(timeSteps, values, linestyle='-',color='royalblue')
            
            baseTitle = 'Time Series'
            baseXLabel = 'Time steps'
            baseYLabel = 'Output (normalized)'
            child_directory = 'plots_monetaryPlucking'
            if not os.path.exists(child_directory):
                os.makedirs(child_directory)
            os.chdir(child_directory)
            plt.suptitle(baseTitle, fontsize=fontSize_supTitle)
            plt.xlabel(baseXLabel, fontsize=fontSize_xlabel)
            plt.ylabel(baseYLabel, fontsize=fontSize_ylabel)
            plt.tick_params(axis='both', labelsize=fontSize_ticks)
            plt.ticklabel_format(style='plain',axis='both',useOffset=False)
            plt.legend(loc=1,fontsize=fontSize_xlabel)
            plt.grid()
            #plt.ylim([0.999,1.001])          
            saveName = 'timeSeries_' + var + '_' + n +'.png'
            plt.savefig(saveName, bbox_inches='tight')
            plt.close()
            os.chdir('..')



        

def all():
    child_directory = 'data_monetaryPlucking'

    if not os.path.exists(child_directory):
        os.makedirs(child_directory)
    os.chdir(child_directory)
    distribution()
    convergence()
    time_series_outputs()
    regression()
    os.chdir('..')

all()