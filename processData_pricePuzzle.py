from __future__ import division
import os.path, sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir))
import os
import processData
import pandas as pd
import csv

def writeChanges():
    csv_file = 'normalized_PCE_monetaryShock_exponent_economy_network_CD_static.csv'
    data = pd.read_csv(csv_file)
    data = pd.DataFrame(data)
    rows = data.shape[0]
    with open('pce_exponent.csv','w') as file:
        writer_data = csv.writer(file, delimiter=',')
        writer_data.writerow(['exponent', 'priceLevel'])
        for r in xrange(rows):
            exponent = data.iloc[r]['monetaryShock_exponent']
            levels = [(data.iloc[r]['t3'] - 1), (data.iloc[r]['t4'] - 1), (data.iloc[r]['t5'] - 1),
                      (data.iloc[r]['t6'] - 1), (data.iloc[r]['t7'] - 1)]
            priceLevel = max(levels)
            priceLevel = 100 * priceLevel


            writer_data.writerow([exponent, priceLevel])

    csv_file = 'normalized_PCE_s_economy_network_CD_static.csv'

    data = pd.read_csv(csv_file)
    data = pd.DataFrame(data)
    rows = data.shape[0]
    with open('pce_s.csv', 'w') as file:
        writer_data = csv.writer(file, delimiter=',')
        writer_data.writerow(['s', 'priceLevel'])
        for r in xrange(rows):
            s = 100 * data.iloc[r]['s']
            levels = [(data.iloc[r]['t3'] - 1),(data.iloc[r]['t4'] - 1),(data.iloc[r]['t5'] - 1),
                      (data.iloc[r]['t6'] - 1),(data.iloc[r]['t7'] - 1)]
            priceLevel = max(levels)
            priceLevel = 100 * priceLevel
            #priceLevel = 100 * (data.iloc[r]['t10'] - 1)
            writer_data.writerow([s, priceLevel])

    """
    csv_file = 'normalized_PCE_multiParam_smonetaryShock_exponent_economy_Firms_CD_static.csv'
    data = pd.read_csv(csv_file)
    data = pd.DataFrame(data)
    rows = data.shape[0]
    with open('pce_theta_s.csv', 'w') as file:
        writer_data = csv.writer(file, delimiter=',')
        writer_data.writerow(['s','exponent','priceLevel'])
        for r in xrange(rows):
            s = 100 * data.iloc[r]['s']
            exponent = data.iloc[r]['monetaryShock_exponent']
            priceLevel = 100 * (data.iloc[r]['t8'] - 1)
            writer_data.writerow([s, exponent, priceLevel])
    """

def write_firm_prices_firms():
    stableTime = 0
    #names = ['economyPickle_firms_small.cPickle','economyPickle_firms_big.cPickle']
    names = ['economyPickle_firms_small.cPickle']
    for file_name in names:
        processData.write_pickled_economy(pickle_file=file_name,
              agents_variables={'firms': ['price_time_series']},
              normalize=True,
              stable_time_step=stableTime)



def normalizePriceIndex_firms(transientTime):
    files = ['s_economy_network_CD_static.csv','monetaryShock_exponent_economy_network_CD_static.csv','var_prob_price_change_economy_network_CD_static.csv','price_sensitivity_economy_network_CD_static.csv','sigma_economy_network_CES_static.csv']
    for csv_file in files:
        variables = ['PCE']
        normPosition = 0
        processData.normalize(csv_file, variables, normPosition, transientTime)

    """
    processData.normalize_multiParam(csv_file='multiParam_smonetaryShock_exponent_economy_CD_static.csv',
                                     parameters=['s', 'monetaryShock_exponent'],
                                     variables=['PCE'],
                                     normPosition=normPosition,
                                      transientTime=transientTime)
    """

def process():
    directory_name = 'data_pricePuzzle'
    os.chdir(directory_name)
    transient = 0
    normalizePriceIndex_firms(transient)
    #writeChanges()
    #write_firm_prices_firms()
    #print "prices written"

    os.chdir('..')


process()

