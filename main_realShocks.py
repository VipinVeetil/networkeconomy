from __future__ import division
import main
import cPickle
import parameters as p
import run 
import write_network
import csv
import os
import matplotlib.pyplot as plt
from collections import OrderedDict
import copy

def two_param(param_name,num_firms):

  iterations = 1

  num_firms = num_firms
  transientTimeSteps = 240
  timeSteps = 1200
  pVariables = {'economy': ['links_changed','sumOutput','out_of_market_firms','gdp'],'firm': None,'household':None}
  
  baseOthers={'n':num_firms,
              'network_from_file': False,
              'household_preference_homogeneous': False,
              'representative_household': True,
              'transient_time_steps': transientTimeSteps,
              'time_steps': timeSteps,
              'firm_productivity_homogeneous':False,
              'alpha':0.3,              
              #'production_function':'CD',
              'production_function':'CES',
              'price_stickiness_type':'linear',
              'linear_price_stickiness_old_share':0.9}
  baseOthers['network_type'] = 'ER'
  baseOthers['d'] = 5
  baseOthers['rewiring_endogenous'] = True
  baseOthers['probability_firm_input_seller_change'] = 0.5
  baseOthers['probability_firm_entry'] = 1
  baseOthers['rewiring_transient'] = True
  baseOthers['connect_firms_without_buyers'] = True
  baseOthers['connect_firms_without_sellers'] = True

  iterations = iterations
  cores = iterations

  #parameter_names = [('linear_price_stickiness_old_share','probability_firm_input_seller_change'),('d','probability_firm_input_seller_change')]
  parameter_names = param_name
  parameter_ranges = {('linear_price_stickiness_old_share','probability_firm_input_seller_change'):{'linear_price_stickiness_old_share':[0.5,0.91],'probability_firm_input_seller_change':[0.1, 1.01]},
  ('d','probability_firm_input_seller_change'):{'d':[2,11],'probability_firm_input_seller_change':[0.1, 1.01]}}

  parameter_increments = {('linear_price_stickiness_old_share','probability_firm_input_seller_change'):{'linear_price_stickiness_old_share':0.1,'probability_firm_input_seller_change':0.2},
  ('d','probability_firm_input_seller_change'):{'d':2,'probability_firm_input_seller_change':0.2}}


  variables = {('linear_price_stickiness_old_share','probability_firm_input_seller_change'):pVariables,
  ('d','probability_firm_input_seller_change'):pVariables}

  other_parameters = {('linear_price_stickiness_old_share','probability_firm_input_seller_change'):baseOthers,
  ('d','probability_firm_input_seller_change'):baseOthers}

  iterations_parameters = {('linear_price_stickiness_old_share','probability_firm_input_seller_change'):iterations,
  ('d','probability_firm_input_seller_change'):iterations}

  cores_parameters = {('linear_price_stickiness_old_share','probability_firm_input_seller_change'):cores,
  ('d','probability_firm_input_seller_change'):cores}
  
  
  network = 'SF'
  fun = 'endo' + str(num_firms)


  main.main(network_name = network, production_function=fun,  time_series={'economy':True, 'firm':False, 'household': False},
                  parameter_names=parameter_names,
                  parameter_ranges=parameter_ranges,
                  parameter_increments=parameter_increments,
                  variables=variables,
                  other_parameters_=other_parameters,
                  iterations_=iterations_parameters,
                  cores_=cores_parameters,
                  model_name='network_economy',
                  model_url='url',
                  paper_name='realShocks',
                  paper_url='url')

def endo_stop():
  transientTimeSteps = 50
  timeSteps = 200
  pVariables = {'economy': ['mean_price_change','links_changed'],'firm': None,'household':None}

  network_type = 'SF'
  baseOthers={'network_type': network_type,
              'd':10,
              'alpha':0.3,
              #'production_function' : 'CD',
              'production_function' : 'CES',
              'household_preference_homogeneous': False,
              'representative_household': True,
              'transient_time_steps': transientTimeSteps,
              'time_steps': timeSteps,
              'stochastic_productivity': False,
              'firm_productivity_homogeneous':False,
              'rewiring_endogenous':True,
              'probability_firm_input_seller_change':1,
              'probability_firm_entry':1,
              'rewiring_stop':100,
              'rewiring_transient':True,
              'connect_firms_without_buyers':True,
              'connect_firms_without_sellers':True}  

  iterations = 10
  cores = 10

  parameter_names = ['n']
  num_firms = [10,100,1000,10000,100000]

  parameter_ranges = {'n':None}
  parameter_increments = {'n':num_firms}
  variables = {'n':pVariables}
  other_parameters = {'n':baseOthers}
  iterations_parameters = {'n':iterations}
  cores_parameters = {'n':cores}

  if network_type == 'powerlaw':
    network = 'powerlaw'
  else:
    network = 'SF'
  #fun = 'CD_endo_stop'
  fun = 'CES_endo_stop'

  main.main(network_name = network, production_function=fun,  time_series={'economy':True, 'firm':False, 'household': False},
                  parameter_names=parameter_names,
                  parameter_ranges=parameter_ranges,
                  parameter_increments=parameter_increments,
                  variables=variables,
                  other_parameters_=other_parameters,
                  iterations_=iterations_parameters,
                  cores_=cores_parameters,
                  model_name='network_economy',
                  model_url='url',
                  paper_name='realShocks',
                  paper_url='url')  

def endo_step():
  transientTimeSteps = 25
  timeSteps = 25
  pVariables = {'economy': ['mean_price_change','links_changed'],'firm': None,'household':None}

  network_type = 'SF'
  baseOthers={'network_type': network_type,
              'd':10,
              'alpha':0.3,
              #'production_function' : 'CD',
              'production_function' : 'CES',
              'household_preference_homogeneous': False,
              'representative_household': True,
              'transient_time_steps': transientTimeSteps,
              'time_steps': timeSteps,
              'stochastic_productivity': False,
              'firm_productivity_homogeneous':False,
              'rewiring_endogenous':True,
              'probability_firm_input_seller_change':1,
              'probability_firm_entry':1,
              'rewiring_timeStep':1,
              'rewiring_transient':False,
              'connect_firms_without_buyers':True,
              'connect_firms_without_sellers':True}  

  iterations = 10
  cores = 5

  parameter_names = ['n']
  num_firms = [10,100,1000,10000,100000]

  parameter_ranges = {'n':None}
  parameter_increments = {'n':num_firms}
  variables = {'n':pVariables}
  other_parameters = {'n':baseOthers}
  iterations_parameters = {'n':iterations}
  cores_parameters = {'n':cores}

  if network_type == 'powerlaw':
    network = 'powerlaw'
  else:
    network = 'SF'
  #fun = 'CD_endoStep'
  fun = 'CES_endoStep'

  main.main(network_name = network, production_function=fun,  time_series={'economy':True, 'firm':False, 'household': False},
                  parameter_names=parameter_names,
                  parameter_ranges=parameter_ranges,
                  parameter_increments=parameter_increments,
                  variables=variables,
                  other_parameters_=other_parameters,
                  iterations_=iterations_parameters,
                  cores_=cores_parameters,
                  model_name='network_economy',
                  model_url='url',
                  paper_name='realShocks',
                  paper_url='url')  

def one_time_productivity_deg_variation():
  transientTimeSteps = 50
  timeSteps = 30
  pVariables = {'economy': ['welfare_mean','sumOutput','gdp_equilibrium_prices','mean_price_change'],'firm': None,'household':None}

  network_type = 'powerlaw'
  baseOthers={'network_type': network_type,
              'alpha':0.3,
              'n':100000,
              #'production_function' : 'CD',
              'production_function' : 'CES',
              'household_preference_homogeneous': False,
              'representative_household': True,
              'transient_time_steps': transientTimeSteps,
              'time_steps': timeSteps,
              'stochastic_productivity': False,
              'firm_productivity_homogeneous':False,
              'one_time_productivity_shock':True,
              'one_time_productivity_shock_time_step':5,
              'one_time_productivity_shock_std':0.1}  


  iterations = 10
  cores = 10

  parameter_names = ['powerlaw_exponent']

  parameter_ranges = {'powerlaw_exponent':[1.1,1.91]}
  parameter_increments = {'powerlaw_exponent':0.05}
  variables = {'powerlaw_exponent':pVariables}
  other_parameters = {'powerlaw_exponent':baseOthers}
  iterations_parameters = {'powerlaw_exponent':iterations}
  cores_parameters = {'powerlaw_exponent':cores}

  if network_type == 'powerlaw':
    network = 'powerlaw'
  else:
    network = 'SF'
  #fun = 'CD'
  fun = 'CES'

  main.main(network_name = network, production_function=fun,  time_series={'economy':True, 'firm':False, 'household': False},
                  parameter_names=parameter_names,
                  parameter_ranges=parameter_ranges,
                  parameter_increments=parameter_increments,
                  variables=variables,
                  other_parameters_=other_parameters,
                  iterations_=iterations_parameters,
                  cores_=cores_parameters,
                  model_name='network_economy',
                  model_url='url',
                  paper_name='realShocks',
                  paper_url='url')  

def one_time_productivity():
  transientTimeSteps = 50
  timeSteps = 30
  pVariables = {'economy': ['welfare_mean','sumOutput','gdp_equilibrium_prices','mean_price_change'],'firm': None,'household':None}

  network_type = 'powerlaw'
  baseOthers={'network_type': network_type,
              'alpha':0.3,
              'n':10000,
              'powerlaw_exponent':1.61,
              #'production_function' : 'CD',
              'production_function' : 'CES',
              'household_preference_homogeneous': False,
              'representative_household': True,
              'transient_time_steps': transientTimeSteps,
              'time_steps': timeSteps,
              'stochastic_productivity': False,
              'firm_productivity_homogeneous':False,
              'one_time_productivity_shock':True,
              'one_time_productivity_shock_time_step':5}  


  iterations = 10
  cores = 10

  parameter_names = ['one_time_productivity_shock_std']

  parameter_ranges = {'one_time_productivity_shock_std':[0.1,1.01]}
  parameter_increments = {'one_time_productivity_shock_std':0.1}
  variables = {'one_time_productivity_shock_std':pVariables}
  other_parameters = {'one_time_productivity_shock_std':baseOthers}
  iterations_parameters = {'one_time_productivity_shock_std':iterations}
  cores_parameters = {'one_time_productivity_shock_std':cores}

  if network_type == 'powerlaw':
    network = 'powerlaw'
  else:
    network = 'SF'
  #fun = 'CD'
  fun = 'CES'

  main.main(network_name = network, production_function=fun,  time_series={'economy':True, 'firm':False, 'household': False},
                  parameter_names=parameter_names,
                  parameter_ranges=parameter_ranges,
                  parameter_increments=parameter_increments,
                  variables=variables,
                  other_parameters_=other_parameters,
                  iterations_=iterations_parameters,
                  cores_=cores_parameters,
                  model_name='network_economy',
                  model_url='url',
                  paper_name='realShocks',
                  paper_url='url')  

def one_time_productivity_million():
  transientTimeSteps = 75
  timeSteps = 40
  pVariables = {'economy': ['welfare_mean','sumOutput','gdp_equilibrium_prices','mean_price_change'],'firm': None,'household':None}

  network_type = 'powerlaw'
  baseOthers={'network_type': network_type,
              'alpha':0.3,
              'n':1000000,
              'powerlaw_exponent':1.6,
              #'production_function' : 'CD',
              'production_function' : 'CES',
              'household_preference_homogeneous': False,
              'representative_household': True,
              'transient_time_steps': transientTimeSteps,
              'time_steps': timeSteps,
              'stochastic_productivity': False,
              'firm_productivity_homogeneous':False,
              'one_time_productivity_shock':True,
              'one_time_productivity_shock_time_step':5}  


  iterations = 1
  cores = 1

  parameter_names = ['one_time_productivity_shock_std']

  parameter_ranges = {'one_time_productivity_shock_std':[0.1,0.11]}
  parameter_increments = {'one_time_productivity_shock_std':0.1}
  variables = {'one_time_productivity_shock_std':pVariables}
  other_parameters = {'one_time_productivity_shock_std':baseOthers}
  iterations_parameters = {'one_time_productivity_shock_std':iterations}
  cores_parameters = {'one_time_productivity_shock_std':cores}

  if network_type == 'powerlaw':
    network = 'powerlaw'
  else:
    network = 'SF'
  #fun = 'CD_million'
  fun = 'CES_million'

  main.main(network_name = network, production_function=fun,  time_series={'economy':True, 'firm':False, 'household': False},
                  parameter_names=parameter_names,
                  parameter_ranges=parameter_ranges,
                  parameter_increments=parameter_increments,
                  variables=variables,
                  other_parameters_=other_parameters,
                  iterations_=iterations_parameters,
                  cores_=cores_parameters,
                  model_name='network_economy',
                  model_url='url',
                  paper_name='realShocks',
                  paper_url='url')  

def one_time_productivity_hundredThousand():
  transientTimeSteps = 75
  timeSteps = 40
  pVariables = {'economy': ['welfare_mean','sumOutput','gdp_equilibrium_prices','mean_price_change'],'firm': None,'household':None}

  network_type = 'powerlaw'
  baseOthers={'network_type': network_type,
              'alpha':0.3,
              'n':100000,
              'powerlaw_exponent':1.6,
              #'production_function' : 'CD',
              'production_function' : 'CES',
              'household_preference_homogeneous': False,
              'representative_household': True,
              'transient_time_steps': transientTimeSteps,
              'time_steps': timeSteps,
              'stochastic_productivity': False,
              'firm_productivity_homogeneous':False,
              'one_time_productivity_shock':True,
              'one_time_productivity_shock_time_step':5}  


  iterations = 100
  cores = 10

  parameter_names = ['one_time_productivity_shock_std']

  parameter_ranges = {'one_time_productivity_shock_std':[0.1,0.11]}
  parameter_increments = {'one_time_productivity_shock_std':0.1}
  variables = {'one_time_productivity_shock_std':pVariables}
  other_parameters = {'one_time_productivity_shock_std':baseOthers}
  iterations_parameters = {'one_time_productivity_shock_std':iterations}
  cores_parameters = {'one_time_productivity_shock_std':cores}

  if network_type == 'powerlaw':
    network = 'powerlaw'
  else:
    network = 'SF'
  #fun = 'CD_hundredThousand'
  fun = 'CES_hundredThousand'

  main.main(network_name = network, production_function=fun,  time_series={'economy':True, 'firm':False, 'household': False},
                  parameter_names=parameter_names,
                  parameter_ranges=parameter_ranges,
                  parameter_increments=parameter_increments,
                  variables=variables,
                  other_parameters_=other_parameters,
                  iterations_=iterations_parameters,
                  cores_=cores_parameters,
                  model_name='network_economy',
                  model_url='url',
                  paper_name='realShocks',
                  paper_url='url')  

def idiosyncratic(num_firms, iterations,rewiringEndogenous):
  transientTimeSteps = 240
  timeSteps = 1200
  if rewiringEndogenous:
    pVariables = {'economy': ['links_changed','sumOutput','out_of_market_firms','gdp'],'firm': None,'household':None}
  else:
    pVariables = {'economy': ['sumOutput','gdp_equilibrium_prices','sum_output_equilibrium_prices','gdp'],'firm': None,'household':None}


  baseOthers={'network_from_file': False,
              'household_preference_homogeneous': False,
              'representative_household': True,
              'transient_time_steps': transientTimeSteps,
              'time_steps': timeSteps,
              'firm_productivity_homogeneous':False,
              'alpha':0.3,              
              'production_function':'CD'}
              #'production_function':'CES'}
              #'price_stickiness_type':'linear',
              #'linear_price_stickiness_old_share':0.9}
  # several margins to play around with to reduce vol, i.e.'probability_firm_input_seller_change' is hte big one
  # d, which is the mean degree, less of of d means more role of the hh to stabilize the system
  # firm_entry prob, less of it means less shocks, but that also means more firms outside the system
  #  
  if rewiringEndogenous:
    baseOthers['price_stickiness_type'] = 'linear'
    baseOthers['linear_price_stickiness_old_share'] = 0.9
    baseOthers['network_type'] = 'ER'
    baseOthers['d'] = 10
    baseOthers['rewiring_endogenous'] = True
    baseOthers['probability_firm_input_seller_change'] = 0.5
    baseOthers['probability_firm_entry'] = 1
    baseOthers['rewiring_transient'] = True
    baseOthers['connect_firms_without_buyers'] = True
    baseOthers['connect_firms_without_sellers'] = True
  else:
    baseOthers['network_type'] = 'powerlaw'
    baseOthers['powerlaw_exponent'] = 1.61
    baseOthers['stochastic_productivity'] = True
    baseOthers['productivity_sigma'] = (0.1/(12**0.5))

  iterations = iterations
  cores = iterations

  parameter_names = ['n']
  parameter_ranges = {'n':None}
  parameter_increments = {'n':num_firms}
  variables = {'n':pVariables}
  other_parameters = {'n':baseOthers}
  iterations_parameters = {'n':iterations}
  cores_parameters = {'n':cores}

  if rewiringEndogenous:
    network = 'ER'
    fun = 'endo'
  else:
    network = 'powerlaw'
    #fun = 'CD'
    fun = 'CES'
  
  if len(num_firms) > 1:
    fun += '1'

  if num_firms[-1] == 100000:
    fun += '2'

  if num_firms[-1] == 1000000:
     fun += '3'


  main.main(network_name = network, production_function=fun,  time_series={'economy':True, 'firm':False, 'household': False},
                  parameter_names=parameter_names,
                  parameter_ranges=parameter_ranges,
                  parameter_increments=parameter_increments,
                  variables=variables,
                  other_parameters_=other_parameters,
                  iterations_=iterations_parameters,
                  cores_=cores_parameters,
                  model_name='network_economy',
                  model_url='url',
                  paper_name='realShocks',
                  paper_url='url')

def sim_fixed():
  #num_firms = [[100,1000,10000],[100000], [1000000]]
  num_firms = [[100,1000,10000]]
  iterations = [10,10,1]

  #for i in xrange(3):
  for i in xrange(1):
    n = num_firms[i]
    it = iterations[i]
    idiosyncratic(num_firms=n, iterations=it,rewiringEndogenous=False)

def sim_endo():
  #num_firms = [[10,100,1000,10000],[100000], [1000000]]
  #num_firms = [[100,1000,10000],[100000], [1000000]]
  num_firms = [[100,1000,10000]]
  iterations = [10,10,1]
  

  for i in xrange(1):  
    n = num_firms[i]
    it = iterations[i]
    idiosyncratic(num_firms=n, iterations=it,rewiringEndogenous=True)

def sim_micro_distributions():

  num_firms = 100000
  transientTimeSteps = 240
  timeSteps = 1200

  print num_firms, "num of firms in the sumulation"
  print timeSteps, "num of time steps in the simulation"

  parameters = p.Parameters()

  parameters.data_time_series['economy'] = True 
  parameters.record_variables['economy']['links_changed'] = True
  parameters.record_variables['economy']['degree_distribution_out'] = True
  parameters.record_variables['economy']['degree_distribution_in'] = True
  parameters.record_variables['economy']['size_distribution'] = True
  parameters.record_variables['economy']['mean_firm_size_change'] = True
  parameters.record_variables['economy']['log_deg_distribution'] = True
  parameters.record_variables['economy']['log_size_distribution'] = True
  parameters.record_variables['economy']['gdp'] = True
  parameters.record_variables['economy']['firm_volatility'] = True
  parameters.record_variables['economy']['wealth'] = True
  parameters.record_variables['economy']['priceLevel'] = True


  parameters.record_param = {'log_size_distribution':{'max_size':22,'min_size':8,'logV':2,'mul':100000},'log_deg_distribution':{'max_deg':10000,'min_deg':1,'logV':2}}
  

  parameters.network_type = 'ER'
  parameters.n = num_firms
  parameters.d = 10
  parameters.household_preference_homogeneous = False
  parameters.transient_time_steps = transientTimeSteps
  parameters.time_steps = timeSteps
  parameters.firm_productivity_homogeneous = False
  parameters.alpha = 0.3
  #parameters.production_function = 'CD'
  parameters.production_function = 'CES'
  parameters.rewiring_endogenous = True
  parameters.probability_firm_input_seller_change = 0.5
  parameters.probability_firm_entry = 1
  parameters.rewiring_transient = True
  parameters.connect_firms_without_buyers = True
  parameters.connect_firms_without_sellers = True  
  parameters.price_stickiness_type = 'linear'
  parameters.linear_price_stickiness_old_share = 0.9
  parameters.record_transient_data = True
  

  l = range(0,timeSteps+1,120)
  l = [i-1 for i in l]
  l[0]=0
  parameters.record_distribution_time_steps = l

  run_instance = run.Run(parameters)
  run_instance.create_economy()
  run_instance.transient()
  run_instance.time_steps()
  economy = run_instance.samayam.economy

  # write the distribution in txt files, also write the number of links changed time steries

  in_dist = economy.degree_distribution_in
  out_dist = economy.degree_distribution_out
  links_changed = economy.links_changed
  size_dist = economy.size_distribution
  size_dist_steps = economy.log_size_distribution
  deg_dist_steps = economy.log_deg_distribution
  gdp = economy.gdp
  mean_size_change = economy.mean_firm_size_change
  cv_size_change = economy.cv_firm_size_change
  firm_volatility = economy.firm_volatility
  wealth = economy.wealth
  priceLevel = economy.priceLevel

  print len(links_changed)
  print sum(links_changed)
  print len(size_dist)
  print len(size_dist[0])

  child_directory = 'data_realShocks'
  if not os.path.exists(child_directory):
      os.makedirs(child_directory)    
  os.chdir(child_directory)

  name = 'priceLevel'
  name += parameters.network_type
  name += str(num_firms)
  name += '.txt'

  with open(name, 'w') as file:
    priceLevel = str(priceLevel)
    file.write(priceLevel)

  name = 'wealth'
  name += parameters.network_type
  name += str(num_firms)
  name += '.txt'

  with open(name, 'w') as file:
    wealth = str(wealth)
    file.write(wealth)


  name = 'firm_volatility'
  name += parameters.network_type
  name += str(num_firms)
  name += '.txt'

  with open(name, 'w') as file:
    firm_volatility = str(firm_volatility)
    file.write(firm_volatility)


  

  name = 'mean_size_change'
  name += parameters.network_type
  name += str(num_firms)
  name += '.txt'

  with open(name, 'w') as file:
    mean_size_change = str(mean_size_change)
    file.write(mean_size_change)

  name = 'cv_size_change'
  name += parameters.network_type
  name += str(num_firms)
  name += '.txt'

  with open(name, 'w') as file:
    cv_size_change = str(cv_size_change)
    file.write(cv_size_change)


  name = 'gdp'
  name += parameters.network_type
  name += str(num_firms)
  name += '.txt'

  with open(name, 'w') as file:
    gdp = str(gdp)
    file.write(gdp)

  name = 'size_dist_steps'
  name += parameters.network_type
  name += str(num_firms)
  name += '.txt'

  with open(name, 'w') as file:
    size_dist_steps = str(size_dist_steps)
    file.write(size_dist_steps)


  name = 'deg_dist_steps'
  name += parameters.network_type
  name += str(num_firms)
  name += '.txt'

  with open(name, 'w') as file:
    deg_dist_steps = str(deg_dist_steps)
    file.write(deg_dist_steps)


  name = 'in_dist'
  name += parameters.network_type
  name += str(num_firms)
  name += '.txt'

  with open(name, 'w') as file:
    in_dist = str(in_dist)
    file.write(in_dist)

  name = 'out_dist'
  name += parameters.network_type
  name += str(num_firms)
  name += '.txt'

  with open(name, 'w') as file:
    out_dist = str(out_dist)
    file.write(out_dist)

  name = 'links_changed'
  name += parameters.network_type
  name += str(num_firms)
  name += '.txt'

  with open(name, 'w') as file:
    links_changed = str(links_changed)
    file.write(links_changed)

  name = 'size_dist'
  name += parameters.network_type
  name += str(num_firms)
  name += '.txt'

  with open(name, 'w') as file:
    size_dist = str(size_dist)
    file.write(size_dist)

  os.chdir('..')

def single_productivity_shock():
	# this function is used to produce data to check how long it 
	#takes the economy to return to equilibrium after one productivity shock
	# it implements productivity shocks with different standard deviations
	#one_time_productivity()
	one_time_productivity_hundredThousand()
	print "DONE hundred thousand productivity one time shock"
	#one_time_productivity_million()
	#print "Done one time productivity shock of million firms"


#single_productivity_shock() # varies the standard deviation of a single productivity shock
#one_time_productivity_deg_variation() # varies the mean degree of network with a single productivity shock, data does not show much of relation, so we can ignore this
#sim_fixed()
#print "done sim fixed"
sim_endo()
print "done endo"
#sim_micro_distributions()
#print "completed micro sim with distribution"



#endo_step()
#endo_stop()
#sim_micro_distributions()
#two_param(param_name=[('linear_price_stickiness_old_share','probability_firm_input_seller_change')],num_firms=10000)
#two_param(param_name=[('d','probability_firm_input_seller_change')],num_firms=10000)

