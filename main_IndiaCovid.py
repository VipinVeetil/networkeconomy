from __future__ import division
import main
import cPickle
import parameters as p
import run
import write_network
import csv
import os
import matplotlib.pyplot as plt
from collections import OrderedDict
import copy
import processData


def convergence_simulation():
  variables = {'economy': ['mean_price_change'],'firm':None,'household':None}
  time_steps = 200
  basicParamSetup = {'production_function': 'CD',
                         'network_from_file': True,
                         'representative_household': True,
                         'transient_time_steps': time_steps,
                         'time_steps': int(time_steps/2),
                        'household_preference_homogeneous': False,
                        'Cobb_Douglas_production_function_homogeneous': False,                        
                        'weightsFile':True,
                        'record_transient_data':True}

  main.main(network_name = 'IO', 
    production_function='CD',  
    time_series={'economy':True, 'firm':False, 'household': False},
    parameter_names=['alpha'],
    parameter_ranges={'alpha':[0.1,0.91]},
    parameter_increments={'alpha':0.1},
    variables={'alpha':variables},
    other_parameters_={'alpha':basicParamSetup},
    iterations_={'alpha':3},
    cores_={'alpha':3},
    model_name='network_economy',
    model_url='url',
    paper_name='covidIndia',
    paper_url='url')

def param_var():
  variables = {'economy': ['gdp_equilibrium_prices'],'firm':['total_output','price','output_sold'],'household':None}
  time_steps = 200
  basicParamSetup = {'production_function': 'CD',
                         'network_from_file': True,
                         'representative_household': True,
                         'transient_time_steps': time_steps,
                         'time_steps': int(time_steps/2),
                        'household_preference_homogeneous': False,
                        'Cobb_Douglas_production_function_homogeneous': False,                        
                        'weightsFile':True,
                        'record_transient_data':False,
                        'lockdown_timeSteps': [10,11,12,13,14,15,16,17,18],
                        'lockdown': True,    
                        'lockdown_cutOff':None,
                        'lockdown_inventory_share':1,
                        'price_stickiness_type': 'linear',
                        'linear_price_stickiness_old_share':0.9,
                        'files_directory':'covidIndiaFiles'}



  main.main(network_name = 'network', 
    production_function='CD',  
    time_series={'economy':True, 'firm':True, 'household': False},
    parameter_names=['linear_price_stickiness_old_share'],
    parameter_ranges={'linear_price_stickiness_old_share':[0.0,0.91]},
    parameter_increments={'linear_price_stickiness_old_share':0.05},
    variables={'linear_price_stickiness_old_share':variables},
    other_parameters_={'linear_price_stickiness_old_share':basicParamSetup},
    iterations_={'linear_price_stickiness_old_share':2},
    cores_={'linear_price_stickiness_old_share':2},
    model_name='network_economy',
    model_url='url',
    paper_name='covidIndia',
    paper_url='url')



#convergence_simulation()
param_var()

