from __future__ import division
import pandas as pd
import matplotlib.pyplot as plt
import random as random
import numpy as np
from math import log
import math
import ast
from itertools import chain
from scipy import stats
from matplotlib.ticker import FormatStrFormatter
import collections
import os
import csv
import plots
import cPickle
import pandas as pd
import ast
import copy


def convergence():
    legendSize = 18
    fontSize_title = 18
    fontSize_xlabel = 16
    fontSize_ylabel = 16
    fontSize_ticks = 16

    file = 'labor_mobility_economy_SF_CES_convergence___convergence.csv'
    plots.convergence_mean_error(csv_file=file,
                                 thresholds=[10,12],
                                 legend=True,
                                 legendSize=legendSize,
                                 legend_location=5,
                                 fontSize_title=fontSize_title,
                                 fontSize_xlabel=fontSize_xlabel,
                                 fontSize_ylabel=fontSize_ylabel,
                                 fontSize_ticks=fontSize_ticks,
                                 child_directory='plots_realShocks',
                                 digits_yAxis=False,
                                 maxXticks=5,
                                 maxYticks=5,
                                 colors=['salmon',  'midnightblue'],
                                 title_text='Convergence',
                                 xlabel=r'Job-finding rate $\phi$',
                                 ylabel='Time steps',
                                 ymin=None,
                                 ymax=None,
                                 xmin=None,
                                 xmax=None,
                                 save_name=None,
                                 cutOff=None)

def printFullEmpMultiple():
    data = pd.read_csv("normalized_gdp_equilibrium_prices_labor_mobility_economy_networkUS70_CES_laborMobFull.csv")
    data = pd.DataFrame(data)
    direct = 0.8747
    v = data.iloc[0][:]
    v = min(v)

    mul = (1-v)/ (1-direct)
    print mul, "full employment multiple"

def one_time_shock_labor_mobility(labelFont,titleFont,ticksFont):
    quarter = 13
    data_dict = {}
    data_dict_gdp = {}
    data = pd.read_csv("labor_mobility_economy_SF_CES.csv")
    data = pd.DataFrame(data)
    rows = data.shape[0]
    for r in xrange(rows):
        jobFinding = float(data.iloc[r]["labor_mobility"])
        data_dict[jobFinding] = []
        data_dict_gdp[jobFinding] = []

    for r in xrange(rows):
        jobFinding = float(data.iloc[r]["labor_mobility"])
        d = data.iloc[r]["unemployment_rate"]
        d_gdp = data.iloc[r]["gdp_equilibrium_prices"]
        d = ast.literal_eval(d)[11:11+quarter]
        d_gdp = ast.literal_eval(d_gdp)[11:11+quarter]
        
        data_dict[jobFinding].append(sum(d)/len(d))
        data_dict_gdp[jobFinding].append(np.mean(d_gdp))

    mean_data_dict = {}
    mean_data_dict_gdp = {}

    errors_dict = {}
    std_vals = []

    errors_dict_gdp = {}
    std_vals_gdp = []


    for rate in data_dict:
        mean_data_dict[rate] = np.mean(data_dict[rate])
        errors_dict[rate] = stats.sem(data_dict[rate])
        std_vals.append(np.std(data_dict[rate]))

        mean_data_dict_gdp[rate] = np.mean(data_dict_gdp[rate])
        errors_dict_gdp[rate] = stats.sem(data_dict_gdp[rate])
        std_vals_gdp.append(np.std(data_dict_gdp[rate]))




    print "min, max, std job finding rate", min(std_vals), max(std_vals)
    print "gdp min, max, std job finding rate", min(std_vals_gdp), max(std_vals_gdp)


    ordered_data_dict = collections.OrderedDict({})
    errors_vals = []

    ordered_data_dict_gdp = collections.OrderedDict({})
    errors_vals_gdp = []


    ks = data_dict.keys()
    ks = sorted(ks)

    

    for k in ks:
        ordered_data_dict[k] = copy.copy(mean_data_dict[k])
        errors_vals.append(errors_dict[k])

        ordered_data_dict_gdp[k] = copy.copy(mean_data_dict_gdp[k])
        errors_vals_gdp.append(errors_dict_gdp[k])



    xVals = ordered_data_dict.keys()
    yVals = ordered_data_dict.values()
    fig, ax = plt.subplots()

    xVals = xVals[4:]
    yVals = yVals[4:]
    errors_vals = errors_vals[4:]

    ax.errorbar(xVals, yVals, errors_vals, linestyle='None', marker='.', color='black', markersize=1, capsize=1)
    plt.plot(xVals,yVals,color='black', linestyle='-')
    plt.scatter(xVals, yVals,color='black',s=5)
    baseTitle = 'Unemployment for different job-finding rate'
    baseXLabel = r'Job-finding rate $\phi$'
    baseYLabel = r'Unemployment rate'
    plt.xlim(xmax=0.21)
    plt.xlim(xmin=0.04)
    plt.plot(xVals, yVals, color='black', linestyle='-')
    plt.scatter(xVals, yVals,color='black',s=2)
    plt.suptitle(baseTitle, fontsize=titleFont)
    plt.xlabel(baseXLabel, fontsize=labelFont)
    plt.ylabel(baseYLabel, fontsize=labelFont)
    plt.tick_params(axis='both', labelsize=ticksFont)
    plt.ticklabel_format(style='plain',axis='both',useOffset=False)
    plt.grid()    
    saveName =  'unemployment_jobFinding_one_shock.png'
    child_directory = 'plots_realShocks'
    if not os.path.exists(child_directory):
        os.makedirs(child_directory)    
    os.chdir(child_directory)
    plt.savefig(saveName, bbox_inches='tight')
    plt.close()
    os.chdir('..')   

    

    xVals = ordered_data_dict_gdp.keys()
    yVals = ordered_data_dict_gdp.values()
    fig, ax = plt.subplots()
    #ax.errorbar(xVals, yVals, errors_vals, linestyle='None', marker='.', color='black', markersize=1, capsize=1)
    plt.plot(xVals,yVals,color='black', linestyle='-')
    plt.scatter(xVals, yVals,color='black',s=5)
    baseTitle = 'GDP for different job-finding rate'
    baseXLabel = r'Job-finding rate $\phi$'
    baseYLabel = r'GDP'
    plt.plot(xVals, yVals, color='black', linestyle='-')
    plt.scatter(xVals, yVals,color='black',s=2)
    plt.suptitle(baseTitle, fontsize=titleFont)
    plt.xlabel(baseXLabel, fontsize=labelFont)
    plt.ylabel(baseYLabel, fontsize=labelFont)
    plt.tick_params(axis='both', labelsize=ticksFont)
    plt.ticklabel_format(style='plain',axis='both',useOffset=False)
    plt.grid()    
    saveName =  'gdp_jobFinding_one_shock.png'
    child_directory = 'plots_realShocks'
    if not os.path.exists(child_directory):
        os.makedirs(child_directory)    
    os.chdir(child_directory)
    plt.savefig(saveName, bbox_inches='tight')
    plt.close()
    os.chdir('..')   
    
def one_time_shock_unemployment(labelFont,titleFont,ticksFont):
    quarter = 13
    unemployment_rate = {}
    data = pd.read_csv("one_time_productivity_shock_std_economy_SF_CES.csv")
    data = pd.DataFrame(data)
    rows = data.shape[0]

    for i in xrange(rows):
        d = data.iloc[i]["unemployment_rate"]
        v = data.iloc[i]["one_time_productivity_shock_std"]
        if v == 0.1:
            yVals = ast.literal_eval(d)
        if v == 0.5:
            yVals0 = ast.literal_eval(d)
        if v == 1:
            yVals1 = ast.literal_eval(d)


    xVals = range(0,len(yVals))
    xVals = [i-12 for i in xVals]


    baseTitle = 'Time series disequilibrium unemployment'
    baseXLabel = r'Time steps (weeks)'
    baseYLabel = r'Unemployment rate'
    plt.plot(xVals, yVals, color='black', linestyle='-',label=r'$\gamma=0.1$')
    plt.scatter(xVals, yVals,color='black',s=2)

    plt.plot(xVals, yVals0, color='salmon', linestyle='-',label=r'$\gamma=0.5$')
    plt.scatter(xVals, yVals0,color='salmon',s=2)

    plt.plot(xVals, yVals1, color='royalblue', linestyle='-',label=r'$\gamma=1$')
    plt.scatter(xVals, yVals1,color='royalblue',s=2)

    

    plt.suptitle(baseTitle, fontsize=titleFont)
    plt.xlabel(baseXLabel, fontsize=labelFont)
    plt.ylabel(baseYLabel, fontsize=labelFont)
    plt.tick_params(axis='both', labelsize=ticksFont)
    plt.ticklabel_format(style='plain',axis='both',useOffset=False)
    plt.legend(loc=1,fontsize=labelFont)
    plt.grid()    
    saveName =  'unemployment_one_shock_time_series.png'
    child_directory = 'plots_covid2'
    if not os.path.exists(child_directory):
        os.makedirs(child_directory)    
    os.chdir(child_directory)
    plt.savefig(saveName, bbox_inches='tight')
    plt.close()
    os.chdir('..')



    for r in xrange(rows):
        std = float(data.iloc[r]["one_time_productivity_shock_std"])
        unemployment_rate[std] = []


    for r in xrange(rows):
        std = float(data.iloc[r]["one_time_productivity_shock_std"])
        d = data.iloc[r]["unemployment_rate"]
        d = ast.literal_eval(d)[11:11+quarter]
        unemployment_rate[std].append(d)


    # add standard error here....

    sum_unemployment_rate = {}

    for std in unemployment_rate:
        sum_unemployment_rate[std] = []
        
    for std in unemployment_rate:
        series =  unemployment_rate[std]
        for s in series:
            val = sum(s)/quarter
            sum_unemployment_rate[std].append(val)

    mean_sum = {}
    allVals= {}
    error_all_data = {}
    std_vals = {}

    for std in unemployment_rate:
        vals = sum_unemployment_rate[std]
        mean_sum[std] = np.mean(vals)
        allVals[std] = vals
        error_val =  stats.sem(vals)
        error_all_data[std] = error_val

        s_val = np.std(vals)
        std_vals[std] = s_val


    std_max = max(std_vals.values())
    std_min = min(std_vals.values())
    print std_min, std_max, "min max standard deviation / error "



    xVals = mean_sum.keys()
    xVals = sorted(xVals)

    yVals = []
    errorVals = []



    for x in xVals:
        yVals.append(mean_sum[x])
        errorVals.append(error_all_data[x])


    fig, ax = plt.subplots()


    baseTitle = 'Disequilibrium unemployment'
    baseXLabel = r'Standard deviation of productivity shock $\gamma$'
    baseYLabel = r'Unemployment rate'
    #plt.plot(xVals, yVals, color='black', linestyle='-',lw=0.25)
    plt.scatter(xVals, yVals,color='black',s=2)

    #ax.errorbar(xVals, yVals, errorVals, linestyle='None', marker='.', color='black', markersize=1, capsize=1)

    plt.suptitle(baseTitle, fontsize=titleFont)
    plt.xlabel(baseXLabel, fontsize=labelFont)
    #plt.ylim(ymin=2)
    #plt.xlim(xmax=0.4)
    plt.ylabel(baseYLabel, fontsize=labelFont)
    plt.tick_params(axis='both', labelsize=ticksFont)
    plt.ticklabel_format(style='plain',axis='both',useOffset=False)
    plt.legend(loc=1,fontsize=labelFont)
    plt.grid()    
    saveName =  'unemployment_one_shock.png'
    child_directory = 'plots_covid2'
    if not os.path.exists(child_directory):
        os.makedirs(child_directory)    
    os.chdir(child_directory)
    plt.savefig(saveName, bbox_inches='tight')
    plt.close()
    os.chdir('..')

    count = 0
    ks = sum_unemployment_rate.keys()
    ks = sorted(ks)
    for std in ks:
        if count%5 == 0:
            vals = allVals[std]
            plt.scatter([std]*len(vals),vals,alpha=0.2,color='black',s=2)
        count += 1

    plt.suptitle(baseTitle, fontsize=titleFont)
    plt.xlabel(baseXLabel, fontsize=labelFont)
    plt.ylabel(baseYLabel, fontsize=labelFont)
    plt.tick_params(axis='both', labelsize=ticksFont)
    plt.ticklabel_format(style='plain',axis='both',useOffset=False)
    plt.legend(loc=1,fontsize=labelFont)
    plt.grid()    
    saveName =  'unemployment_one_shock_allPoints.png'
    child_directory = 'plots_covid2'
    if not os.path.exists(child_directory):
        os.makedirs(child_directory)    
    os.chdir(child_directory)
    plt.savefig(saveName, bbox_inches='tight')
    plt.close()
    os.chdir('..')

def compute_cost(allData,yearQuarter):
    if yearQuarter == 'year':
        end = 73
        begin = 20
    elif yearQuarter == 'quarter':
        end = 35
        begin = 21

    cost = {}
    for v in allData:
        cost[v] = 0
    for v in allData:
        data = allData[v]
        data = data[10:end]
        for val in data:
            c = 1-val
            if c > 0:
                cost[v] += c

    cost_sorted = collections.OrderedDict({})
    vals = cost.keys()
    vals = sorted(vals)
    for v in vals:
        c = cost[v]
        cost_sorted[v] = c / (end-begin)
    return cost_sorted

def second_dip(labelFont,titleFont,ticksFont):
    data = pd.read_csv("normalized_gdp_equilibrium_prices_linear_price_stickiness_old_share_economy_networkUS70_CES.csv")
    data = pd.DataFrame(data)
    rows = data.shape[0]
    dt = {}
    xVals = []
    yVals = []
    for r in range(rows):
        m = data.iloc[r]['t28']
        l = data.iloc[r]['t29']
        a = data.iloc[r]['t30']
        b = data.iloc[r]['t31']
        c = data.iloc[r]['t32']
        d = data.iloc[r]['t33']
        e = data.iloc[r]['t34']
        f = data.iloc[r]['t35']
        g = data.iloc[r]['t36']
        ne = np.mean([b,c,d,e,f])

        #prop = (a-b)/a
        #prop = (a-b)
        #prop = 1-(a-ne)
        #prop = ne/a
        #prop = 1-b
        prop = m-a


        x = data.iloc[r]['linear_price_stickiness_old_share']
        
        xVals.append(x)
        yVals.append(prop)

        dt[x] = prop


    xVals = sorted(xVals)
    newY = []
    for x in xVals:
        a = dt[x]
        newY.append(a)

    baseTitle = 'Size of second dip'
    baseXLabel = r'Price stickiness $\rho$'
    baseYLabel = 'Portion of weekly GDP'

    plt.plot(xVals, newY, color='black', linestyle='-')
    plt.scatter(xVals, newY,color='black',s=3)
    plt.suptitle(baseTitle, fontsize=titleFont)
    plt.xlabel(baseXLabel, fontsize=labelFont)
    plt.ylabel(baseYLabel, fontsize=labelFont)
    plt.tick_params(axis='both', labelsize=ticksFont)
    plt.ticklabel_format(style='plain',axis='both',useOffset=False)


    plt.grid()    
    saveName =  'restart_cost.png'

    child_directory = 'plots_covid2'
    if not os.path.exists(child_directory):
        os.makedirs(child_directory)    
    os.chdir(child_directory)
    plt.savefig(saveName, bbox_inches='tight')
    plt.close()
    os.chdir('..')

def time_series(labelFont,titleFont,ticksFont):
    data = pd.read_csv("normalized_gdp_equilibrium_prices_linear_price_stickiness_old_share_economy_networkUS70_CES.csv")
    #data = pd.read_csv("normalized_gdp_equilibrium_prices_labor_mobility_economy_networkUS70_CES.csv")
    data = pd.DataFrame(data)
    rows = data.shape[0]
    allData = {}
    for r in xrange(rows):
        d = data.iloc[r][1:]
        d = list(d)
        v = data.iloc[r]["linear_price_stickiness_old_share"]
        #v = data.iloc[r]["labor_mobility"]
        allData[v] = d

    #print allData.keys()
    limit = 54
    yVals = allData[0.9]
    yVals = yVals[10:]
    #yVals = allData[0.4]
    
    #yVals = allData[0.3]
    #yVals = allData[0.1]
    xVals = range(len(yVals))
    xVals = [i-10 for i in xVals]

    yVals = yVals[0:limit]

    print min(yVals), "min GDP"
    xVals = xVals[0:limit]

    baseTitle = 'Time series of GDP'
    baseXLabel = 'Weeks'
    baseYLabel = 'GDP (normalized)'

    fig, ax = plt.subplots()

    #print len(xVals)
    
    plt.plot(xVals, yVals, color='black', linestyle='-')
    plt.scatter(xVals, yVals,color='black',s=3)
    plt.suptitle(baseTitle, fontsize=titleFont)
    plt.xlabel(baseXLabel, fontsize=labelFont)
    plt.ylabel(baseYLabel, fontsize=labelFont)
    plt.tick_params(axis='both', labelsize=ticksFont)
    plt.ticklabel_format(style='plain',axis='both',useOffset=False)

    dirY = [0.867,0.867,0.867,0.867,0.867,0.867,0.867]
    dirX = [1,2,3,4,5,6,7]

    a = [1] * 10
    b = [1] * 36
    dirY = a + dirY + b
    dirX = range(len(dirY))
    dirX = [i-9 for i in dirX]
    plt.scatter(dirX, dirY,color='royalblue',s=3)
    plt.plot(dirX, dirY,color='royalblue',linestyle='-',lw=1.5)

    eY = [0.774,0.774,0.774,0.774,0.774,0.774,0.774]
    eX = [1,2,3,4,5,6,7]

    eY = a + eY + b
    eX = range(len(eY))
    eX = [i-9 for i in eX]
    plt.scatter(eX, eY,color='salmon',s=3)
    plt.plot(eX, eY,color='salmon',linestyle='--',lw=1.5)

    plt.axvspan(1, 7, facecolor='grey', alpha=0.2)


    #plt.scatter(eX, eY,color='salmon',s=3)

    #ax.hlines(y=0.867,xmax=limit+5,xmin=-20,label='Direct',color='royalblue',linestyle='--',lw=1.5)
    #ax.hlines(y=0.774,xmax=limit+5,xmin=-20,label='Equilibrium network',color='salmon',linestyle='--',lw=1.5)
    ax.hlines(y=0.867,xmax=7,xmin=1,label='Direct',color='royalblue',linestyle='-',lw=1.5)
    ax.hlines(y=0.774,xmax=7,xmin=1,label='Equilibrium network',color='salmon',linestyle='-',lw=1.5)
    plt.xlim(xmax=limit-5,xmin=-20)
    legendFont = labelFont - 4
    plt.legend(loc=4,fontsize=labelFont-2)
    plt.grid()    
    saveName =  'time_series_output.png'

    child_directory = 'plots_covid2'
    if not os.path.exists(child_directory):
        os.makedirs(child_directory)    
    os.chdir(child_directory)
    plt.savefig(saveName, bbox_inches='tight')
    plt.close()
    os.chdir('..')

    
    data = pd.read_csv("linear_price_stickiness_old_share_economy_networkUS70_CES.csv")
    data = pd.DataFrame(data)
    rows = data.shape[0]
    allData = {}
    for r in xrange(rows):
        d = data.iloc[r]["unemployment_rate"]
        d = ast.literal_eval(d)
        d = list(d)
        v = data.iloc[r]["linear_price_stickiness_old_share"]
        allData[v] = d

    limit = 54
    yVals = allData[0.9]
    yVals = yVals[10:]
    xVals = range(len(yVals))
    xVals = [i-10 for i in xVals]

    yVals = yVals[0:limit]
    xVals = xVals[0:limit]
    xVals = [i+1 for i in xVals]

    baseTitle = 'Time series of unemployment'
    baseXLabel = 'Weeks'
    baseYLabel = 'Unemployment rate'
    plt.plot(xVals, yVals, color='black', linestyle='-')
    plt.scatter(xVals, yVals,color='black',s=3)
    plt.axvspan(1, 7, facecolor='grey', alpha=0.2)
    plt.suptitle(baseTitle, fontsize=titleFont)
    plt.xlabel(baseXLabel, fontsize=labelFont)
    plt.ylabel(baseYLabel, fontsize=labelFont)
    plt.tick_params(axis='both', labelsize=ticksFont)
    plt.ticklabel_format(style='plain',axis='both',useOffset=False)
    plt.legend(loc=1,fontsize=labelFont)
    plt.grid()    
    saveName =  'time_series_unemployment.png'

    child_directory = 'plots_covid2'
    if not os.path.exists(child_directory):
        os.makedirs(child_directory)    
    os.chdir(child_directory)
    plt.savefig(saveName, bbox_inches='tight')
    plt.close()
    os.chdir('..')    

def labor_mobility(labelFont,titleFont,ticksFont):
    data = pd.read_csv("normalized_gdp_equilibrium_prices_labor_mobility_economy_networkUS70_CES.csv")
    data = pd.DataFrame(data)
    rows = data.shape[0]
    allData = {}
    for r in xrange(rows):
        d = data.iloc[r][1:]
        d = list(d)
        v = data.iloc[r]["labor_mobility"]
        allData[v] = d

    costs = compute_cost(allData,'year')

    xVals = costs.keys()
    yVals = costs.values()

    #xVals = xVals[4:]
    #yVals = yVals[4:]

    baseTitle = 'Lockdown cost for different Job-finding rates'
    baseXLabel = r'Weekly job-finding rate $\phi$'
    baseYLabel = 'Proportion of yearly GDP'
    #plt.xlim(xmax=0.21)
    #plt.xlim(xmin=0.04)
    #xVals = xVals[4:]
    #yVals = yVals[4:]
    plt.plot(xVals, yVals, color='black', linestyle='-')
    plt.scatter(xVals, yVals,color='black')
    plt.suptitle(baseTitle, fontsize=titleFont)
    plt.xlabel(baseXLabel, fontsize=labelFont)
    plt.ylabel(baseYLabel, fontsize=labelFont)
    plt.tick_params(axis='both', labelsize=ticksFont)
    plt.ticklabel_format(style='plain',axis='both',useOffset=False)
    plt.legend(loc=1,fontsize=labelFont)
    plt.grid()    
    saveName =  'labor_mobility.png'
    child_directory = 'plots_covid2'
    if not os.path.exists(child_directory):
        os.makedirs(child_directory)    
    os.chdir(child_directory)
    plt.savefig(saveName, bbox_inches='tight')
    plt.close()
    os.chdir('..')

def unemployment_labor_mobility(labelFont,titleFont,ticksFont):
    # average rate of unemployment in the 2nd Quarter, 13 weeks beginning from the lockdown which is week 11, so we go upto week 23


    data = pd.read_csv("labor_mobility_economy_networkUS70_CES.csv")
    data = pd.DataFrame(data)
    rows = data.shape[0]
    allData = {}
    for r in xrange(rows):
        d = data.iloc[r]["unemployment_rate"]
        d = ast.literal_eval(d)
        d = list(d)
        v = data.iloc[r]["labor_mobility"]
        allData[v] = d

    mean_unemplyment_rate = {}
    for v in allData:
        d = allData[v]
        d = d[11:50]
        d = np.mean(d)
        mean_unemplyment_rate[v] = d

    vals = mean_unemplyment_rate.keys()
    vals = sorted(vals)
    ordered_mean_unemplyment_rate = collections.OrderedDict({})
    for v in vals:
        rate = mean_unemplyment_rate[v]
        ordered_mean_unemplyment_rate[v] = rate

    xVals = ordered_mean_unemplyment_rate.keys()
    yVals = ordered_mean_unemplyment_rate.values()


    #xVals = xVals[4:]
    #yVals = yVals[4:]

    print xVals
    print yVals


    baseTitle = 'Unemployment for different job-finding rates'
    baseXLabel = r'Weekly job-finding rate $\phi$'
    baseYLabel = 'Post-lockdown 2020 unemployment rate'
    #plt.xlim(xmax=0.21)
    #plt.xlim(xmin=0.04)
    plt.plot(xVals, yVals, color='black', linestyle='-')
    plt.scatter(xVals, yVals,color='black')
    plt.suptitle(baseTitle, fontsize=titleFont)
    plt.xlabel(baseXLabel, fontsize=labelFont)
    plt.ylabel(baseYLabel, fontsize=labelFont)
    plt.tick_params(axis='both', labelsize=ticksFont)
    plt.ticklabel_format(style='plain',axis='both',useOffset=False)
    plt.legend(loc=1,fontsize=labelFont)
    plt.grid()    
    saveName =  'unemployment_labor_mobility.png'

    child_directory = 'plots_covid2'
    if not os.path.exists(child_directory):
        os.makedirs(child_directory)    
    os.chdir(child_directory)
    plt.savefig(saveName, bbox_inches='tight')
    plt.close()
    os.chdir('..')

def price_stickiness(labelFont,titleFont,ticksFont):
    data = pd.read_csv("normalized_gdp_equilibrium_prices_linear_price_stickiness_old_share_economy_networkUS70_CES.csv")
    data = pd.DataFrame(data)
    rows = data.shape[0]
    allData = {}
    for r in xrange(rows):
        d = data.iloc[r][1:]
        d = list(d)
        v = data.iloc[r]["linear_price_stickiness_old_share"]
        allData[v] = d
    costs = compute_cost(allData,'year')
    xVals = costs.keys()
    yVals = costs.values()
    baseTitle = 'Lockdown cost for different price stickiness'
    baseXLabel = r'Price stickiness $\rho$'
    baseYLabel = 'Proportion of Q2 2020 GDP'
    plt.plot(xVals, yVals, color='black', linestyle='-')
    plt.scatter(xVals, yVals,color='black',s=3)
    plt.suptitle(baseTitle, fontsize=titleFont)
    plt.xlabel(baseXLabel, fontsize=labelFont)
    plt.ylabel(baseYLabel, fontsize=labelFont)
    plt.tick_params(axis='both', labelsize=ticksFont)
    plt.ticklabel_format(style='plain',axis='both',useOffset=False)
    plt.legend(loc=1,fontsize=labelFont)
    plt.grid()    
    saveName =  'price_stickiness.png'
    child_directory = 'plots_covid2'
    if not os.path.exists(child_directory):
        os.makedirs(child_directory)    
    os.chdir(child_directory)
    plt.savefig(saveName, bbox_inches='tight')
    plt.close()
    os.chdir('..')

def price_sensitivity(labelFont,titleFont,ticksFont):
    data = pd.read_csv("normalized_gdp_equilibrium_prices_price_sensitivity_economy_networkUS70_CES.csv")
    data = pd.DataFrame(data)
    rows = data.shape[0]
    allData = {}
    for r in xrange(rows):
        d = data.iloc[r][1:]
        d = list(d)
        v = data.iloc[r]["price_sensitivity"]
        allData[v] = d
    costs = compute_cost(allData,'year')
    xVals = costs.keys()
    yVals = costs.values()
    baseTitle = 'Lockdown cost for different heterogeneity in prices'
    baseXLabel = r'Sensitivity $\psi$'
    baseYLabel = 'Proportion of Q2 2020 GDP'
    plt.plot(xVals, yVals, color='black', linestyle='-')
    plt.scatter(xVals, yVals,color='black',s=3)
    plt.suptitle(baseTitle, fontsize=titleFont)
    plt.xlabel(baseXLabel, fontsize=labelFont)
    plt.ylabel(baseYLabel, fontsize=labelFont)
    plt.tick_params(axis='both', labelsize=ticksFont)
    plt.ticklabel_format(style='plain',axis='both',useOffset=False)
    plt.legend(loc=1,fontsize=labelFont)
    plt.grid()    
    saveName =  'price_sensitivity.png'
    child_directory = 'plots_covid2'
    if not os.path.exists(child_directory):
        os.makedirs(child_directory)    
    os.chdir(child_directory)
    plt.savefig(saveName, bbox_inches='tight')
    plt.close()
    os.chdir('..')

def sigma(labelFont,titleFont,ticksFont):
    data = pd.read_csv("normalized_gdp_equilibrium_prices_sigma_economy_networkUS70_CES.csv")
    data = pd.DataFrame(data)
    rows = data.shape[0]
    allData = {}
    for r in xrange(rows):
        d = data.iloc[r][1:]
        d = list(d)
        v = data.iloc[r]["sigma"]
        allData[v] = d

    labelFont += 1
    titleFont += 1 
    ticksFont += 1

    costs = compute_cost(allData,'year')
    xVals = costs.keys()
    yVals = costs.values()
    baseTitle = 'Lockdown cost for different input complementarity'
    baseXLabel = 'CES exponent'
    baseYLabel = 'Proportion of Q2 2020 GDP'
    plt.plot(xVals, yVals, color='black', linestyle='-')
    plt.scatter(xVals, yVals,color='black')
    plt.suptitle(baseTitle, fontsize=titleFont)
    plt.xlabel(baseXLabel, fontsize=labelFont)
    plt.ylabel(baseYLabel, fontsize=labelFont)
    plt.tick_params(axis='both', labelsize=ticksFont)
    plt.ticklabel_format(style='plain',axis='both',useOffset=False)
    plt.legend(loc=1,fontsize=labelFont)
    plt.grid()    
    saveName =  'sigma.png'
    child_directory = 'plots_covid2'
    if not os.path.exists(child_directory):
        os.makedirs(child_directory)    
    os.chdir(child_directory)
    plt.savefig(saveName, bbox_inches='tight')
    plt.close()
    os.chdir('..')

def lockdown_shock_size(labelFont,titleFont,ticksFont):
    data = pd.read_csv("normalized_gdp_equilibrium_prices_lockdownShock_multiple_economy_networkUS70_CES.csv")
    data = pd.DataFrame(data)
    rows = data.shape[0]
    allData = {}
    for r in xrange(rows):
        d = data.iloc[r][1:]
        d = list(d)
        v = data.iloc[r]["lockdownShock_multiple"]
        allData[v] = d
    
    costs = compute_cost(allData,'year')
    xVals = costs.keys()
    yVals = costs.values()
    baseTitle = 'Lockdown cost for different sizes of lockdown'
    baseXLabel = 'Lockdown size'
    baseYLabel = 'Proportion of Q2 2020 GDP'
    plt.plot(xVals, yVals, color='black', linestyle='-')
    plt.scatter(xVals, yVals,color='black')
    plt.suptitle(baseTitle, fontsize=titleFont)
    plt.xlabel(baseXLabel, fontsize=labelFont)
    plt.ylabel(baseYLabel, fontsize=labelFont)
    #plt.xlim(xmin=0)
    plt.tick_params(axis='both', labelsize=ticksFont)
    plt.ticklabel_format(style='plain',axis='both',useOffset=False)
    plt.legend(loc=1,fontsize=labelFont)
    plt.grid()    
    saveName =  'lockdown_size.png'
    child_directory = 'plots_covid2'
    if not os.path.exists(child_directory):
        os.makedirs(child_directory)    
    os.chdir(child_directory)
    plt.savefig(saveName, bbox_inches='tight')
    plt.close()
    os.chdir('..')

def network_multiple(labelFont,titleFont,ticksFont):
    data = pd.read_csv("normalized_gdp_equilibrium_prices_labor_mobility_economy_networkUS70_CES.csv")
    data = pd.DataFrame(data)
    rows = data.shape[0]
    allData = {}
    for r in xrange(rows):
        d = data.iloc[r][1:]
        d = list(d)
        v = data.iloc[r]["labor_mobility"]
        allData[v] = d

    vs = allData.keys()
    vs = sorted(vs)
    minVals = collections.OrderedDict({})
    directVals = collections.OrderedDict({})

    for v in vs:
        d = allData[v]
        mV  = min(d)
        minVals[v] = mV
        #direct = d[11]            
        direct = 0.8747
        #print d[11]
        directVals[v] = direct

    ratios = collections.OrderedDict({})
    for v in minVals:
        a = minVals[v]
        b = directVals[v]
        r = (1-a)/(1-b)
        ratios[v] = r

    xVals = ratios.keys()
    yVals = ratios.values()

    #xVals = xVals[:-4]
    #yVals = yVals[:-4]

    #xVals = xVals[4:]
    #yVals = yVals[4:]

    #xVals = xVals[0:33]
    #yVals = yVals[0:33]

    baseTitle = 'Network multiple'
    baseXLabel = r'Weekly job-finding rate $\phi$'
    baseYLabel = r'$\omega$'
    plt.plot(xVals, yVals, color='black', linestyle='-')
    plt.scatter(xVals, yVals,color='black')
    plt.suptitle(baseTitle, fontsize=titleFont)
    plt.xlabel(baseXLabel, fontsize=labelFont)
    #plt.xlim(xmax=0.21)
    #plt.xlim(xmin=0.04)
    #plt.ylim(ymin=2)
    #plt.xlim(xmin=0.05)
    plt.ylabel(baseYLabel, fontsize=labelFont)
    plt.tick_params(axis='both', labelsize=ticksFont)
    plt.ticklabel_format(style='plain',axis='both',useOffset=False)
    plt.legend(loc=1,fontsize=labelFont)
    plt.grid()    
    saveName =  'network_multiple.png'
    child_directory = 'plots_covid2'
    if not os.path.exists(child_directory):
        os.makedirs(child_directory)    
    os.chdir(child_directory)
    plt.savefig(saveName, bbox_inches='tight')
    plt.close()
    os.chdir('..')

def inventory():
    saveName = 'inventory.png'
    data = pd.read_csv('normalized_inventory_equi_prices_linear_price_stickiness_old_share_economy_networkUS70_CES.csv')
    #data = pd.read_csv('normalized_inventory_equi_prices_PCE_linear_price_stickiness_old_share_economy_networkUS70_CES.csv')
    data = pd.DataFrame(data)
    d = data.iloc[0]
    d1 = data.iloc[1]
    d2 = data.iloc[2]
    child_directory = 'plots_covid2'
    os.chdir(child_directory)

    limit = 54
    xVals = range(len(d[1:]))
    xVals = [i-10 for i in xVals]

    fig, ax = plt.subplots()
    plt.plot(xVals,d[1:],label=str(d[0]),color='black')
    plt.scatter(xVals,d[1:],color='black',s=1)

    plt.plot(xVals,d1[1:],label=str(d1[0]),color='royalblue')
    plt.scatter(xVals,d1[1:],color='royalblue',s=1)

    plt.plot(xVals,d2[1:],label=str(d2[0]),color='grey')
    plt.scatter(xVals,d2[1:],color='grey',s=1)

    ax.vlines(x=7,ymax=1,ymin=0,color='red',linestyle='-',lw=1.5)
    plt.grid()

    plt.legend()
    plt.savefig(saveName, bbox_inches='tight')
    plt.close()
    os.chdir('..')

def intermediate_share_plot(labelFont,titleFont,ticksFont):
    colors = {'0.9': 'black', '0.6': 'grey', '0.3': 'grey'}
    labels = {'0.9': r'$\rho=0.9$','0.3': r'$\rho=0.3$'}
    data = pd.read_csv("intermediate_share.csv")
    data = pd.DataFrame(data)
    rows = data.shape[0]
    for r in xrange(rows):
        d = data.iloc[r]
        d = list(d)
        v = str(d[0])
        if v == '0.9':
            d = d[50:98]
            xVals = range(-10,len(d)-10)
            plt.plot(xVals,d,color=colors[v],label=labels[v])
            plt.scatter(xVals,d,color=colors[v],s=3)
    baseTitle = 'Intermediate inputs share'
    baseXLabel = 'Weeks'
    baseYLabel = 'Proportion'

    plt.axvspan(1, 7, facecolor='grey', alpha=0.2)
    #fig, ax = plt.subplots()
    plt.suptitle(baseTitle, fontsize=titleFont)
    plt.xlabel(baseXLabel, fontsize=labelFont)
    plt.ylabel(baseYLabel, fontsize=labelFont)
    plt.tick_params(axis='both', labelsize=ticksFont)
    plt.ticklabel_format(style='plain',axis='both',useOffset=False)
    #plt.legend(loc=1,fontsize=labelFont)
    plt.grid()    
    saveName =  'time_series_intermediate.png'    

    child_directory = 'plots_covid2'
    if not os.path.exists(child_directory):
        os.makedirs(child_directory)    
    os.chdir(child_directory)
    plt.savefig(saveName, bbox_inches='tight')
    plt.close()
    os.chdir('..')   

def inventory_share_plot(labelFont,titleFont,ticksFont):
    colors = {'0.9': 'black', '0.6': 'grey', '0.3': 'grey'}
    labels = {'0.9': r'$\rho=0.9$','0.3': r'$\rho=0.3$'}
    data = pd.read_csv("inventory_prop.csv")
    data = pd.DataFrame(data)
    rows = data.shape[0]
    for r in xrange(rows):
        d = data.iloc[r]
        d = list(d)
        v = str(d[0])
        if v != '0.6':
            d = d[50:98]
            xVals = range(-10,len(d)-10)
            plt.plot(xVals,d,color=colors[v],label=labels[v])
            plt.scatter(xVals,d,color=colors[v],s=3)
    baseTitle = 'Inventory share'
    baseXLabel = 'Weeks'
    baseYLabel = 'Proportion'


    #fig, ax = plt.subplots()
    plt.suptitle(baseTitle, fontsize=titleFont)
    plt.xlabel(baseXLabel, fontsize=labelFont)
    plt.ylabel(baseYLabel, fontsize=labelFont)
    plt.tick_params(axis='both', labelsize=ticksFont)
    plt.ticklabel_format(style='plain',axis='both',useOffset=False)
    plt.legend(loc=1,fontsize=labelFont)
    plt.grid()    
    saveName =  'time_series_inventory.png'

    child_directory = 'plots_covid2'
    if not os.path.exists(child_directory):
        os.makedirs(child_directory)    
    os.chdir(child_directory)
    plt.savefig(saveName, bbox_inches='tight')
    plt.close()
    os.chdir('..')   

        
        




        

        #v = data.iloc[r]["priceStickiness"]

def firms_intermediate_share(labelFont,titleFont,ticksFont):    
    data = pd.read_csv("intermediate_share_firms.csv")
    data = pd.DataFrame(data)

    before = list(data.iloc[0])
    before = before[1:]
    before = sorted(before)

    after2 = list(data.iloc[2])
    after2 = after2[1:]
    after2 = sorted(after2)


    #print max(after)
    #print max(after2)

    #plt.plot(before,color='black')
    #plt.plot(after,color='grey')
    #plt.plot(after2,color='blue')

    
    #plt.scatter(range(len(before)),before,color='black')
    #plt.scatter(range(len(after2)),after2,color='royalblue')
    
    #before = [math.log(i) for i in before]
    #after2 = [math.log(i) for i in after2]

    
    log_before = []
    for i in before:
        if i > 0:
            log_before.append(math.log(i,10))

    log_after2 = []
    for i in after2:
        if i > 0:
            log_after2.append(math.log(i,10))
    """
    plt.plot(range(len(log_before)),log_before,color='black')
    plt.plot(range(len(log_after2)),log_after2,color='royalblue')
    """
    plt.hist(log_before,bins=100)
    plt.hist(log_after2,bins=100)

    plt.show()

def pickle_plots(labelFont,titleFont,ticksFont,var,normalize,title,ylabel):
    #file = '0.3priceStickiness.cPickle'
    #E = cPickle.load(open(file, "rb"))
    #var1 = getattr(E, var)

    file = '0.6priceStickiness.cPickle'
    E = cPickle.load(open(file, "rb"))
    var2 = getattr(E, var)


    if normalize:
      #  norm1 = var1[30]
        norm2 = var2[30]
       # var1 = [i/norm1 for i in var1]
        var2 = [i/norm2 for i in var2]

    #var1 = var1[55:105]
    var2 = var2[55:104]


    xVals = range(-5,len(var2)-5)


    #plt.plot(xVals,var1,color='black',label=r'$\rho=0.3$')
    #plt.scatter(xVals,var1,color='black',s=3)

    plt.plot(xVals,var2,color='black')
    plt.scatter(xVals,var2,color='black',s=3)

    plt.axvspan(0, 6, facecolor='grey', alpha=0.2)

    baseTitle = title
    baseXLabel = 'Weeks'
    baseYLabel = ylabel


    plt.suptitle(baseTitle, fontsize=titleFont)
    plt.xlabel(baseXLabel, fontsize=labelFont)
    plt.ylabel(baseYLabel, fontsize=labelFont)
    plt.tick_params(axis='both', labelsize=ticksFont)
    plt.ticklabel_format(style='plain',axis='both',useOffset=False)
    #plt.legend(loc=4,fontsize=labelFont)
    #plt.ylim(ymax=1,ymin=0)
    plt.grid()  

    saveName =  title + '.png'

    child_directory = 'plots_covid2'
    if not os.path.exists(child_directory):
        os.makedirs(child_directory)    
    os.chdir(child_directory)
    plt.savefig(saveName, bbox_inches='tight')
    plt.close()
    os.chdir('..')   

        





def plot():
    labelFont = 18
    titleFont = 22
    ticksFont = 16

    child_directory = 'data_covid2'
    if not os.path.exists(child_directory):
        os.makedirs(child_directory)    
    os.chdir(child_directory)

    """

    pickle_plots(labelFont=labelFont,
        titleFont=titleFont,
        ticksFont=ticksFont,
        var='intermediate_share',
        normalize=False,
        title='Share of intermediate goods',
        ylabel='Proportion')

    pickle_plots(labelFont=labelFont,
        titleFont=titleFont,
        ticksFont=ticksFont,
        var='gdp_equilibrium_prices',
        normalize=True,
        title='GDP',
        ylabel='GDP (normalized)')

    pickle_plots(labelFont=labelFont,
        titleFont=titleFont,
        ticksFont=ticksFont,
        var='sum_output_equilibrium_prices',
        normalize=True,
        title='Output',
        ylabel='Output (normalized)')

    pickle_plots(labelFont=labelFont,
        titleFont=titleFont,
        ticksFont=ticksFont,
        var='inventory_prop',
        normalize=False,
        title='Inventory',
        ylabel='Proportion of total goods')

    """

    #intermediate_share_plot(labelFont=labelFont,titleFont=titleFont,ticksFont=ticksFont)
    #inventory_share_plot(labelFont=labelFont,titleFont=titleFont,ticksFont=ticksFont)

    #firms_intermediate_share(labelFont=labelFont,titleFont=titleFont,ticksFont=ticksFont)

 
    #printFullEmpMultiple()
    #time_series(labelFont=labelFont,titleFont=titleFont,ticksFont=ticksFont)


    #price_sensitivity(labelFont=labelFont,titleFont=titleFont,ticksFont=ticksFont)
    #labor_mobility(labelFont=labelFont,titleFont=titleFont,ticksFont=ticksFont)
    #unemployment_labor_mobility(labelFont=labelFont,titleFont=titleFont,ticksFont=ticksFont)
    #price_stickiness(labelFont=labelFont,titleFont=titleFont,ticksFont=ticksFont)
    #sigma(labelFont=labelFont,titleFont=titleFont,ticksFont=ticksFont)
    #lockdown_shock_size(labelFont=labelFont,titleFont=titleFont,ticksFont=ticksFont)
    #network_multiple(labelFont=labelFont,titleFont=titleFont,ticksFont=ticksFont)
    second_dip(labelFont=labelFont,titleFont=titleFont,ticksFont=ticksFont)
    

    #one_time_shock_unemployment(labelFont=labelFont,titleFont=titleFont,ticksFont=ticksFont)
    
    #convergence()

    
    
    os.chdir('..')


plot()