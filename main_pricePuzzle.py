from __future__ import division
import main
import cPickle
import parameters as p
import run
import write_network
import csv
import os
import matplotlib.pyplot as plt
from collections import OrderedDict
import copy




def firms_parameter_sweep(iterations, cores, networkName):
    transientTimeSteps = 0
    timeSteps = 40 
    pVariables = {'economy': ['PCE', 'CPI','wealth'],'firm': None, 'household': None}
    baseOthers={'load_pickle':True,
        'monetary_shock_time_step': 1,
        'record_transient_data':True,
              'money_injection_agents': "firms",
              'monetary_shock': True,
              'network_from_file': True,
              'household_preference_homogeneous': False,
              'weightsFile': True,
              'sizesFile': True,
              'representative_household': True,
              'transient_time_steps': transientTimeSteps,
              'time_steps': timeSteps,
                's':-0.001,
                'monetaryShock_exponent_fileSizes': False,
                'monetaryShock_exponent':0.9,
                'files_directory':'pricePuzzleFiles'}

    others_s = copy.deepcopy(baseOthers)
    del others_s['s']
    others_monetaryShock_exponent = copy.deepcopy(baseOthers)
    del others_monetaryShock_exponent["monetaryShock_exponent"]

    others_sensitivity = copy.deepcopy(baseOthers)
    others_sensitivity['endogenous_probability_price_change'] = True

    others_sensitivity['price_stickiness_type'] = 'probabilistic'
    

    others_var_prob = copy.deepcopy(baseOthers)
    others_var_prob['price_stickiness_type'] = 'probabilistic'
    others_var_prob['probability_price_change'] = 0.5

    

    #others_monetaryShock_exponent['monetaryShock_exponent_stochastic'] = False

    #parameter_names = [('s', 'monetaryShock_exponent'),'s','monetaryShock_exponent']
    #parameter_names = ['s', 'monetaryShock_exponent']
    #parameter_names = ['monetaryShock_exponent']
    parameter_names = ['price_sensitivity','var_prob_price_change']
    #parameter_names = ['price_sensitivity']
    #parameter_names = ['var_prob_price_change']

    parameter_ranges = {'var_prob_price_change':[0,0.41],'price_sensitivity':[0.05,0.51],'s':[-0.002, 0.0], 'monetaryShock_exponent':[0.8,1.4],('s', 'monetaryShock_exponent'):{'s':[-0.001, 0.0], 'monetaryShock_exponent':[0.9,1.0]}}
    parameter_increments = {'var_prob_price_change':0.02,'price_sensitivity':0.05,'s':0.00001,'monetaryShock_exponent':0.001,('s', 'monetaryShock_exponent'):{'s':0.0001,'monetaryShock_exponent':0.01}}
    variables = {'var_prob_price_change':pVariables,'price_sensitivity':pVariables,'s': pVariables,'monetaryShock_exponent':pVariables,('s', 'monetaryShock_exponent'):pVariables}
    other_parameters = {'var_prob_price_change':others_var_prob,'price_sensitivity':others_sensitivity,'s':others_s,'monetaryShock_exponent':others_monetaryShock_exponent,('s', 'monetaryShock_exponent'):others_monetaryShock_exponent}
    iterations_parameters = {'var_prob_price_change':iterations,'price_sensitivity':iterations,'s':iterations, 'monetaryShock_exponent':iterations,('s', 'monetaryShock_exponent'):cores}
    cores_parameters = {'var_prob_price_change':cores,'price_sensitivity':cores,'s':cores, 'monetaryShock_exponent':cores,('s', 'monetaryShock_exponent'):cores}
    network = networkName
    fun = 'CD' + '_' + 'static'
    main.main(network_name = network, production_function=fun,  time_series={'economy':True, 'firm':False, 'household': False},
                  parameter_names=parameter_names,
                  parameter_ranges=parameter_ranges,
                  parameter_increments=parameter_increments,
                  variables=variables,
                  other_parameters_=other_parameters,
                  iterations_=iterations_parameters,
                  cores_=cores_parameters,
                  model_name='network_economy',
                  model_url='url',
                  paper_name='pricePuzzle',
                  paper_url='url')


def firmsEconomyPickle(size,twoProductFirms,money_injection_agents,monetary_shock,network_from_file,
                    household_preference_homogeneous,weightsFile,sizesFile,
                   representative_household,transient_time_steps,time_steps,
                       monetaryShock_exponent_fileSizes,monetaryShock_exponent):
    parameters = p.Parameters()
    parameters.data_time_series['economy'] = True
    parameters.record_variables['economy']['PCE'] = True
    parameters.record_variables['economy']['CPI'] = True
    parameters.record_variables['economy']['wealth'] = True
    parameters.files_directory = 'pricePuzzleFiles'
    parameters.twoProductFirms = twoProductFirms
    parameters.s = size
    parameters.time_steps = time_steps
    parameters.transient_time_steps = time_steps
    parameters.monetary_shock = True
    parameters.monetary_shock_time_step = 5
    parameters.network_from_file = network_from_file
    parameters.household_preference_homogeneous = household_preference_homogeneous
    parameters.money_injection_agents=money_injection_agents
    parameters.monetary_shock=monetary_shock
    parameters.weightsFile=weightsFile
    parameters.sizesFile=sizesFile
    parameters.representative_household=representative_household
    parameters.transient_time_steps=transient_time_steps
    parameters.monetaryShock_exponent_fileSizes = monetaryShock_exponent_fileSizes
    parameters.monetaryShock_exponent=monetaryShock_exponent
    parameters.write_pickle = True

    run_instance = run.Run(parameters)
    run_instance.create_economy()
    run_instance.transient()
    run_instance.time_steps()
    economy = run_instance.samayam.economy

    if monetaryShock_exponent>1:
        file_name = 'economyPickle' + '_firms_' + 'big'
    else:

        file_name = 'economyPickle' + '_firms_' + 'small'

    directory_name = 'data' + '_' + 'pricePuzzle'
    if not os.path.exists(directory_name):
        os.makedirs(directory_name)
    os.chdir(directory_name)

    with open('%s.cPickle' % file_name, 'wb') as econ:
        cPickle.dump(economy, econ, protocol=cPickle.HIGHEST_PROTOCOL)
    os.chdir('..')

def create_pickle_economy(network_from_file,
                    household_preference_homogeneous,
                          weightsFile,
                          sizesFile,
                   representative_household,
                          transient_time_steps,
                          time_steps,
                          prod_function):
    parameters = p.Parameters()

    #parameters.record_transient_data = True

    parameters.data_time_series['economy'] = True
    parameters.record_variables['economy']['PCE'] = True
    parameters.record_variables['economy']['CPI'] = True
    parameters.record_variables['economy']['wealth'] = True

    parameters.files_directory = 'pricePuzzleFiles'
    parameters.time_steps = time_steps
    parameters.transient_time_steps = transient_time_steps
    parameters.production_function = prod_function

    parameters.network_from_file = network_from_file
    parameters.weightsFile = weightsFile
    parameters.sizesFile = sizesFile

    parameters.household_preference_homogeneous = household_preference_homogeneous
    parameters.representative_household=representative_household

    print "created parameters"

    run_instance = run.Run(parameters)
    run_instance.create_economy()
    print "created economy"

    run_instance.transient()
    run_instance.time_steps()

    economy = run_instance.samayam.economy

    file_name = 'economy'

    with open('%s.cPickle' % file_name, 'wb') as econ:
        cPickle.dump(economy, econ, protocol=cPickle.HIGHEST_PROTOCOL)
    os.chdir('..')




def firms_pickle_economy():
    print 'pickle'
    #exponent = [0.90,1.2]
    exponent = [0.90]
    for e in exponent:
        firmsEconomyPickle(size=-0.001,
                           twoProductFirms=False,
                           money_injection_agents='firms',
                           monetary_shock=True,
                           network_from_file=True,
                           household_preference_homogeneous=False,
                           weightsFile=True,
                           sizesFile=True,
                           representative_household=True,
                           transient_time_steps=500,
                           time_steps=50,
                           monetaryShock_exponent_fileSizes=False,
                           monetaryShock_exponent=e)


def pickleUnshockedEconomy(prod_function):
    create_pickle_economy(network_from_file=True,
                           household_preference_homogeneous=False,
                           weightsFile=True,
                           sizesFile=True,
                           representative_household=True,
                           transient_time_steps=250,
                           time_steps=1,
                           prod_function=prod_function)


def firmSim():
    #firms_pickle_economy()
    #pickleUnshockedEconomy(prod_function='CD')
    
    firms_parameter_sweep(iterations=100, cores=10, networkName='network')

    #firms_parameter_sweep_CES(iterations=1, cores=1, networkName='network')    



firmSim()
