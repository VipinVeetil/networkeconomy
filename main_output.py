from __future__ import division
import main
import cPickle
import parameters as p
import run

import write_network
import csv
import os
import matplotlib.pyplot as plt
from collections import OrderedDict
import copy



def fully_flexible_prices():
    production_function = 'CD'
    price_stickiness_type = False
    iterations = 1
    cores = 1 
    time_steps = 25
    exponent_default = 0.9
    shockSize_default = -0.01
    linear_stickiness_default = 0.5
    probability_price_change_default=0.5
    
    
    exponents = [0.9, 1.1]
    for e in exponents:
        parameter_names = ['s']
        parameter_ranges = {'s':[-0.001, 0.00101]}    
        parameter_increments = {'s':0.0001}         
        singleParameter(production_function=production_function, 
                        price_stickiness_type=price_stickiness_type,
                        iterations=iterations, 
                        cores=cores,  
                        exponent_default=e,
                        shockSize_default=shockSize_default,
                        parameter_names=parameter_names,
                        parameter_ranges=parameter_ranges,
                        parameter_increments=parameter_increments,
                        time_steps=time_steps,
                        linear_stickiness_default=linear_stickiness_default,
                        probability_price_change_default=probability_price_change_default)
        
    

    shockSize_default = [0.01,-0.01]
    for s in shockSize_default:
        parameter_names = ['monetaryShock_exponent']
        parameter_ranges = {'monetaryShock_exponent':[0.9,1.101]}
        parameter_increments = {'monetaryShock_exponent':0.01}
        singleParameter(production_function=production_function, 
                        price_stickiness_type=price_stickiness_type,
                        iterations=iterations, 
                        cores=cores,  
                        exponent_default=exponent_default,
                        shockSize_default=s,
                        parameter_names=parameter_names,
                        parameter_ranges=parameter_ranges,
                        parameter_increments=parameter_increments,
                        time_steps=time_steps,
                        linear_stickiness_default=linear_stickiness_default,
                        probability_price_change_default=probability_price_change_default)

def linear_sticky_prices():
    production_function = 'CD'
    price_stickiness_type = 'linear'
    iterations = 1
    cores = 1 
    linear_stickiness_default = 0.5
    probability_price_change_default = 0.5
    time_steps = 25
    exponent_default = 0.9
    shockSize_default = -0.01
    
    parameter_names = ['s']
    parameter_ranges = {'s':[-0.001, 0.0011]}
    parameter_increments = {'s':0.0001}
    
    singleParameter(production_function=production_function, 
                    price_stickiness_type=price_stickiness_type,
                    iterations=iterations, 
                    cores=cores,  
                    exponent_default=exponent_default,
                    shockSize_default=shockSize_default,
                    parameter_names=parameter_names,
                    parameter_ranges=parameter_ranges,
                    parameter_increments=parameter_increments,
                    time_steps=time_steps,
                    linear_stickiness_default=linear_stickiness_default,
                    probability_price_change_default=probability_price_change_default)


    parameter_names = ['linear_price_stickiness_old_share']
    parameter_ranges = {'linear_price_stickiness_old_share':[0,1.01]}
    parameter_increments = {'linear_price_stickiness_old_share':0.1}

    shockSizes = [0.01, -0.01]
    for s in shockSizes:
        singleParameter(production_function=production_function, 
                        price_stickiness_type=price_stickiness_type,
                        iterations=iterations, 
                        cores=cores,  
                        exponent_default=exponent_default,
                        shockSize_default=s,
                        parameter_names=parameter_names,
                        parameter_ranges=parameter_ranges,
                        parameter_increments=parameter_increments,
                        time_steps=time_steps,                    
                        linear_stickiness_default=linear_stickiness_default,
                        probability_price_change_default=probability_price_change_default)    
    
def prob_sticky_prices():
    iterations = 20
    cores = 10
    production_function = 'CD'
    price_stickiness_type = 'probabilistic'
    linear_stickiness_default = 0.5
    probability_price_change_default=0.5
    exponent_default = 0.9
    shockSize_default = -0.01
    time_steps = 25
    
    parameter_names = ['s']
    parameter_ranges = {'s':[-0.001, 0.0011]}
    parameter_increments = {'s':0.0001}
    
    singleParameter(production_function=production_function, 
                    price_stickiness_type=price_stickiness_type,
                    iterations=iterations, 
                    cores=cores,  
                    exponent_default=exponent_default,
                    shockSize_default=shockSize_default,
                    parameter_names=parameter_names,
                    parameter_ranges=parameter_ranges,
                    parameter_increments=parameter_increments,
                    time_steps=time_steps,
                    linear_stickiness_default=linear_stickiness_default,
                    probability_price_change_default=probability_price_change_default)

    shockSizes = [0.01,-0.01]
    for s in  shockSizes:
        parameter_names = ['probability_price_change']
        parameter_ranges = {'probability_price_change':[0, 1.01]}
        parameter_increments = {'probability_price_change':0.1}
        
        
        singleParameter(production_function=production_function, 
                        price_stickiness_type=price_stickiness_type,
                        iterations=iterations, 
                        cores=cores,  
                        exponent_default=exponent_default,
                        shockSize_default=s,
                        parameter_names=parameter_names,
                        parameter_ranges=parameter_ranges,
                        parameter_increments=parameter_increments,
                        time_steps=time_steps,
                        linear_stickiness_default=linear_stickiness_default,
                        probability_price_change_default=probability_price_change_default)    
        
def prodFunction_substitutes():
    production_function = 'substitutes'
    price_stickiness_type = 'linear'
    iterations = 1
    cores = 1 
    linear_stickiness_default = 0.5
    probability_price_change_default = 0.5
    time_steps = 25
    exponent_default = 0.9
    shockSize_default = -0.01
    
    parameter_names = ['s']
    parameter_ranges = {'s':[-0.001, 0.0011]}
    parameter_increments = {'s':0.0001}
    
    
    singleParameter(production_function=production_function, 
                    price_stickiness_type=price_stickiness_type,
                    iterations=iterations, 
                    cores=cores,  
                    exponent_default=exponent_default,
                    shockSize_default=shockSize_default,
                    parameter_names=parameter_names,
                    parameter_ranges=parameter_ranges,
                    parameter_increments=parameter_increments,
                    time_steps=time_steps,
                    linear_stickiness_default=linear_stickiness_default,
                    probability_price_change_default=probability_price_change_default)
   
def span_single_parameter():
    fully_flexible_prices()
    linear_sticky_prices()
    #prodFunction_substitutes()
    #prob_sticky_prices()
    
def singleParameter(production_function, 
                    price_stickiness_type,
                    iterations, 
                    cores,  
                    exponent_default,
                    shockSize_default,
                    linear_stickiness_default,
                    probability_price_change_default,
                    parameter_names,
                    parameter_ranges,
                    parameter_increments,
                    time_steps):

    
    if exponent_default > 1:
        exp = 'largeTheta'
    else:
        exp = 'smallTheta'
        
    if shockSize_default > 0:
        sh = 'positive'
    else:
        sh = 'negative'
    
    transientTimeSteps = 0
    
    
    baseParameters={'price_stickiness_type':price_stickiness_type,
            's':shockSize_default,
            'monetaryShock_exponent':exponent_default,
            'linear_price_stickiness_old_share': linear_stickiness_default,
            'probability_price_change':probability_price_change_default,
            'load_pickle':True,
            'monetary_shock_time_step': 1,
            'record_transient_data':True,
            'monetary_shock': True,
            'monetaryShock_stochastic':True,
            'network_from_file': True,
            'household_preference_homogeneous': False,
            'representative_household': True,
            'transient_time_steps': transientTimeSteps,
            'time_steps': time_steps,
            'firms_pce_file':True}
           
    


    var = {'economy': ['finalOutput', 'sumOutput', 'wealth', 'welfare_mean','finalOutput_equilibrium_prices'],'firm': None, 'household': None}
        
    variables = {}
    iterations_parameters = {}
    cores_parameters = {}
    other_parameters = {}
    base = {}
     
    for n in parameter_names:
        variables[n] = var
        iterations_parameters[n] = iterations
        cores_parameters[n] = cores
        other_parameters[n] = baseParameters
        baseCopy = copy.deepcopy(baseParameters)
        del baseCopy[n]
        base[n] = baseCopy 
    
   
      
    nameParam = parameter_names[0]
    
    production_function = str(price_stickiness_type) + '_' + str(production_function) + '_' + str(exp)  + '_' + str(sh)  

    main.main(network_name = 'network', 
               production_function = production_function, 
               time_series = {'economy':True, 'firm':False, 'household': False},
               parameter_names=parameter_names,
               parameter_ranges=parameter_ranges,
               parameter_increments=parameter_increments,
               variables=variables,
               other_parameters_= other_parameters,
               iterations_=iterations_parameters,
               cores_=cores_parameters,
               model_name='network_economy',
               model_url='url',
               paper_name='output',
               paper_url='url')


    
def create_pickle_economy():
    
    parameters = p.Parameters()
    
    parameters.write_pickle = True

    parameters.monetary_shock = True
    parameters.monetary_shock_time_step = 5
    
    parameters.network_from_file = True
    parameters.household_preference_homogeneous = False
    parameters.weightsFile =  True
    parameters.sizesFile = True
    
    parameters.transient_time_steps = 100
    parameters.time_steps = 1       
    
    #parameters.fixed_labor_shares = True
    parameters.firms_pce_file = True

    run_instance = run.Run(parameters)
    run_instance.create_economy()
    run_instance.transient()
    run_instance.time_steps()
    


def economyPickle(production_function, 
                  price_stickiness_type,
                  size, 
                  monetaryShock_exponent,
                  linear_price_stickiness_old_share):


    parameters = p.Parameters()
    
    parameters.production_function = production_function
    parameters.price_stickiness_type = price_stickiness_type
    parameters.s = size
    parameters.linear_price_stickiness_old_share = linear_price_stickiness_old_share
    
    parameters.monetary_shock = True
    parameters.monetary_shock_time_step = 15
    parameters.monetaryShock_exponent = monetaryShock_exponent
    
    parameters.network_from_file = True 
    parameters.household_preference_homogeneous = False
    parameters.weightsFile = True
    parameters.sizesFile = True

    parameters.transient_time_steps = 50
    parameters.time_steps = 25
    #parameters.firms_pce_file = True

    parameters.record_transient_data = True
    parameters.set_sectors = True
    
    parameters.data_time_series['economy'] = True
    parameters.data_time_series['firm'] = True
    #parameters.fixed_labor_shares = True
    
    parameters.record_variables['economy']['PCE'] = True
    parameters.record_variables['economy']['finalOutput'] = True
    parameters.record_variables['economy']['sumOutput'] = True
    parameters.record_variables['economy']['welfare_mean'] = True
    parameters.record_variables['economy']['wealth'] = True
    parameters.record_variables['economy']['finalOutput_equilibrium_prices'] = True
    parameters.record_variables['economy']['finalOutput_consumer_equilibrium_prices'] = True
    parameters.record_variables['economy']['mean_price_change'] = True
    parameters.record_variables['economy']['zeta'] = True
    parameters.record_variables['economy']['eta'] = True
    parameters.record_variables['economy']['sectoral_output'] = True
    
    
    parameters.monetaryShock_stochastic=True
    parameters.randomWeightShare = 0
    parameters.z = 0.9


    #parameters.load_pickle = True
    parameters.load_pickle = False

    run_instance = run.Run(parameters)
    run_instance.create_economy()
    run_instance.transient()
    run_instance.time_steps()
    economy = run_instance.samayam.economy
    
    if size>0:
        sh = 'positive'
    else:
        sh = 'negative'
        
    if monetaryShock_exponent > 1:
        ex = 'largeTheta'
    else:
        ex = 'smallTheta'
        
    
    file_name = 'economyPickle_' + sh + '_' + ex + '_' + production_function + '_' + price_stickiness_type
    
   
    directory_name = 'data' + '_' + 'output'
    if not os.path.exists(directory_name):
        os.makedirs(directory_name)
    os.chdir(directory_name)

    with open('%s.cPickle' % file_name, 'wb') as econ:
        cPickle.dump(economy, econ, protocol=cPickle.HIGHEST_PROTOCOL)
    os.chdir('..')




def pickle_economy():
    #shockSize = [0.001, -0.001]
    shockSize = [0.001]
    #exponent = [0.9,1.1]
    exponent = [0.9]
    
    price_stickiness_old_share = 0.5
    
    
    prod_function = ['CD']
    #price_stickiness_type = ['False', 'linear']
    price_stickiness_type = ['False']

    
    
    for e in exponent:
        for s in shockSize:
            for fun in prod_function:
                for t in price_stickiness_type:
                    print e, s, fun, t
                    economyPickle(production_function=fun,
                                  price_stickiness_type=t,
                                  size=s, 
                                  monetaryShock_exponent=e,
                                  linear_price_stickiness_old_share=price_stickiness_old_share)
                    
def sim_output():
    #create_pickle_economy()
    pickle_economy()
    #span_single_parameter()

sim_output()


