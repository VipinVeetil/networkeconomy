from __future__ import division
import pandas as pd
import matplotlib.pyplot as plt
import random as random
import numpy as np
from math import log
import math
import ast
from itertools import chain
from scipy import stats
from matplotlib.ticker import FormatStrFormatter
import collections
import os
import csv
import plots
import cPickle
import ast


def contourJointVariable():
    plots.countourSurface(csv_file='average_meanmultiParam_sg_economy_SF_CD_static_stats_cv.csv',
                x_name='s',
                y_name='g',
                z_name='mean',
                x_label=r'Size of monetary shock $s$',
                y_label=r'Homogeneity of monetary shock $g$',
                title=r'Price variation $v$',
                save_name=None,
                fontSize_title=24,
                fontSize_xlabel=22,
                fontSize_ylabel=22,
                fontSize_ticks=22,
                child_directory='plots_cantillon',
                zeroYValue=True,
                maxYValue=1)

    plots.countourSurface(csv_file='average_meanmultiParam_sg_economy_SF_CD_static_stats_rhoC.csv',
                          x_name='s',
                          y_name='g',
                          z_name='mean',
                          x_label=r'Size of monetary shock $s$',
                          y_label=r'Homogeneity of monetary shock $g$',
                          title=r'Prices below steady state $\rho$',
                          save_name=None,
                          fontSize_title=24,
                          fontSize_xlabel=22,
                          fontSize_ylabel=22,
                          fontSize_ticks=22,
                          child_directory='plots_cantillon',
                          zeroYValue=True,
                          maxYValue=1)

    plots.countourSurface(csv_file='average_meanmultiParam_sd_economy_SF_CD_static_stats_cv.csv',
                          x_name='s',
                          y_name='d',
                          z_name='mean',
                          x_label=r'Size of monetary shock $s$',
                          y_label=r'Degree of production network $d$',
                          title=r'Price variation $v$',
                          save_name=None,
                          fontSize_title=24,
                          fontSize_xlabel=22,
                          fontSize_ylabel=22,
                          fontSize_ticks=22,
                          child_directory='plots_cantillon',
                          zeroYValue=True,
                          maxYValue=None)

    plots.countourSurface(csv_file='average_meanmultiParam_sd_economy_SF_CD_static_stats_rhoC.csv',
                          x_name='s',
                          y_name='d',
                          z_name='mean',
                          x_label=r'Size of monetary shock $s$',
                          y_label=r'Degree of production network $d$',
                          title=r'Prices below steady state $\rho$',
                          save_name=None,
                          fontSize_title=24,
                          fontSize_xlabel=22,
                          fontSize_ylabel=22,
                          fontSize_ticks=22,
                          child_directory='plots_cantillon',
                          zeroYValue=True,
                          maxYValue=10)


    plots.countourSurface(csv_file='average_meanmultiParam_gd_economy_SF_CD_static_stats_cv.csv',
                          x_name='g',
                          y_name='d',
                          z_name='mean',
                          x_label=r'Homogeneity of monetary shock $g$',
                          y_label=r'Degree of production network $d$',
                          title=r'Price variation $v$',
                          save_name=None,
                          fontSize_title=24,
                          fontSize_xlabel=22,
                          fontSize_ylabel=22,
                          fontSize_ticks=22,
                          child_directory='plots_cantillon',
                          zeroYValue=True,
                          maxYValue=10)

    plots.countourSurface(csv_file='average_meanmultiParam_gd_economy_SF_CD_static_stats_rhoC.csv',
                          x_name='g',
                          y_name='d',
                          z_name='mean',
                          x_label=r'Homogeneity of monetary shock $g$',
                          y_label=r'Degree of production network $d$',
                          title=r'Prices below steady state $\rho$',
                          save_name=None,
                          fontSize_title=24,
                          fontSize_xlabel=22,
                          fontSize_ylabel=22,
                          fontSize_ticks=22,
                          child_directory='plots_cantillon',
                          zeroYValue=True,
                          maxYValue=10)




def fourVariable_plot():
    plots.fourVariable_colorPlot(csv_file='multiParam_sgd_economy_SF_CD_static_stats_cv.csv',
                                 parameterNames=['s','g','d'], variable_name='mean', save_name=None)

def multiNetworks_timePlots_SFB():
    fontSize_title = 24
    fontSize_xlabel = 20
    fontSize_ylabel = 22
    fontSize_ticks = 18
    legendSize = 18

    x_label = 'Time steps'
    networks = ['SF', 'B']
    names = {'SF': 'Scale-free Network', 'B': 'Balanced Network'}
    variables = ['cv', 'rhoC', 'phi']
    y_labels = {'cv':r'Price variation $v$', 'rhoC': r'$\rho$', 'phi': r'$\phi$'}
    titles = {'cv': r'Impulse response of $v^t$', 'rhoC': r'Impulse response of $\rho^t$', 'phi': r'Impulse response of $\phi^t$'}
    time_steps = {'cv': [20,50], 'rhoC': [20,50], 'phi': [20,50]}
    time_shift = 4
    parameter_names = ['g', 'theta']
    parameterValues = [{'g':0.1, 'theta':0.5}, {'g':0.5, 'theta':1}]
    colors = {'SF':'cornflowerblue', 'B':'salmon'}
    child_directory = 'plots_cantillon'

    for parameter_values in parameterValues:
        plots.multiNetworks_timePlots(networks, names, variables, time_steps, time_shift, parameter_names,
                            parameter_values,x_label,y_labels, titles,
                            fontSize_title, fontSize_xlabel, fontSize_ylabel, fontSize_ticks,
                            child_directory, colors, legendSize)

def priceDistribution():
    fontSize_supTitle = 23
    fontSize_xlabel = 22
    fontSize_ylabel = 22
    fontSize_ticks = 20

    networks = ['SF', 'B']
    for network in networks:
        file = 'economyPickle_CD_' + network + '_positive.cPickle'
        E = cPickle.load(open(file, "rb"))
        data = getattr(E, 'price_distribution')
        d = data[1]
        d = sorted(d)
        d = [math.log(i,10) for i in d]
        fig, ax = plt.subplots()
        ax.ticklabel_format(useOffset=True, useLocale=True, useMathText=True)
        ax.tick_params(axis='both', labelsize=fontSize_ticks)
        xMax = 7
        xMin = 3
        bins = 100
        title = "Steady State Price Distribution: "
        if network == 'SF':
            title += "Scale-free Network"
            plt.hist(d, bins=bins, color='black')
            plt.xlim([xMin, xMax])

        elif network == 'B':
            title += "Balanced Network"
            bins = 1
            plt.hist(d, bins=bins, color='black')
            plt.xlim([xMin, xMax])

        plt.suptitle(title, fontsize=fontSize_supTitle)
        plt.xlabel('Log Price (base 10)',fontsize=fontSize_xlabel)
        plt.ylabel('Number of firms',fontsize=fontSize_ylabel)
        png_name = 'priceDistributionSS'
        png_name += file[:-17]
        child_directory = 'plots_cantillon'
        if not os.path.exists(child_directory):
            os.makedirs(child_directory)
        os.chdir(child_directory)
        plt.savefig('%s.png' % png_name, bbox_inches='tight')
        plt.close
        os.chdir('..')

def price_timeSeries_histograms(time_begin, time_end, time_shift):
    fontSize_supTitle = 16
    fontSize_title = 16
    fontSize_xlabel = 16
    fontSize_ylabel = 16
    fontSize_ticks = 16
    shock_time = 25

    nameType = 'pickle_data_firms_economyPickle_CD_'
    networks = ['SF', 'B']
    signs = ['positive', 'negative']


    for network in networks:
        for sign in signs:
            basic = nameType + network + '_' + sign + '.csv'
            priceSmoothening = nameType + network + '_' + sign + '_smoothening' + '.csv'
            noisyShock = nameType + network + '_' + sign + '_noisyShock' + '.csv'
            file_names = {'basic':basic, 'smoothening':priceSmoothening, 'noisyShock':noisyShock}
            if sign == 'negative':
                noisyShock = nameType + network + '_' + 'positive_noisyShock' + '.csv'
                file_names['noisyShock'] = noisyShock
            for name in file_names:
                file_name = file_names[name]
                title = 'Price Responses to a ' + sign.capitalize() + ' Monetary Shock'
                fontSize_title_individual = 11
                fontSize_ticks_individual = 12
                plots.time_series_instances(csv_file=file_name,
                                             variable_name='price_time_series',
                                             variable_name_plot=' ',
                                             n_columns=4,
                                             n_rows=4,
                                             time_begin=time_begin,
                                             time_end=time_end,
                                             time_shift=time_shift,
                                             xlabel='Time steps',
                                             ylabel=r'Normalized prices $p_i$',
                                             title=title,
                                             fontSize_supTitle=fontSize_supTitle,
                                             fontSize_title=fontSize_title_individual,
                                             fontSize_xlabel=fontSize_xlabel,
                                             fontSize_ylabel=fontSize_ylabel,
                                             fontSize_ticks=fontSize_ticks_individual,
                                             save_name=None,
                                             color='royalblue',
                                             maxXticks=2,
                                             maxYticks=3,
                                             child_directory='plots_cantillon')

                plots.time_series_bunch(csv_file=file_name,
                                        variable_name='price_time_series',
                                        variable_name_plot='Prices',
                                        numbers_bunch=None,
                                        time_begin=time_begin,
                                        time_end=time_end,
                                        time_shift=time_shift,
                                        xlabel='Time steps',
                                        ylabel=r'Normalized prices $p_i$',
                                        title=title,
                                        fontSize_supTitle=fontSize_supTitle,
                                        fontSize_title=fontSize_title,
                                        fontSize_xlabel=fontSize_xlabel,
                                        fontSize_ylabel=fontSize_ylabel,
                                        fontSize_ticks=fontSize_ticks,
                                        save_name=None,
                                        maxXticks=4,
                                        maxYticks=3,
                                        child_directory='plots_cantillon',
                                        scatter=True,
                                        shock_time=shock_time)

                title = 'Price Distribution after a ' + sign.capitalize() + ' Monetary Shock'
                time_steps = [27, 28, 29, 30, 31, 35]
                if sign == 'positive' and name == noisyShock:
                    xMax = 1.01
                    xMin = 0.99
                else:
                    xMax = 1.1
                    xMin = 0.9

                plots.histogram_time_steps(csv_file=file_name,
                                            variable_name='price_time_series',
                                            time_steps=time_steps,
                                            shock_time = shock_time,
                                            xlabel='Time steps',
                                            ylabel='Proportion of prices',
                                            title=title,
                                            bins=1000,
                                            fontSize_supTitle=fontSize_supTitle,
                                            fontSize_title=fontSize_title,
                                            fontSize_xlabel=fontSize_xlabel,
                                            fontSize_ylabel=fontSize_ylabel,
                                            fontSize_ticks=fontSize_ticks,
                                            save_name=None,
                                            maxXticks=4,
                                            maxYticks=3,
                                            child_directory='plots_cantillon',
                                            xMax=xMax,
                                            xMin=xMin,
                                            vline=1,
                                           shareXAxis=True,
                                           shareYAxis=True)

def timeSeries_positive_negative_shocks(start_time, end_time, time_shift):
    fontSize_title = 18
    fontSize_xlabel = 16
    fontSize_ylabel = 18
    fontSize_ticks = 16

    networks = ['SF', 'B']
    parameters = ['s', 'psi']
    xlabel = 'Time steps'
    for network in networks:
        for parameter_name in parameters:
            file = parameter_name + '_economy_' + network + '_CD_static.csv'
            if parameter_name == 's':
                parameter_values = [0.01, -0.01]
                variableNames = ['cv', 'cp', 'norm', 'normC', 'prop']
            elif parameter_name == 'psi':
                parameter_values = [0.4]
                variableNames = ['cv', 'cp', 'inventoryCarry', 'wealthCarry']


            variables = {'cv': ['cv'], 'cp':['cp'],
                         'norm': ['zeta', 'eta'], 'normC': ['zetaC', 'etaC'], 'prop': ['rhoC', 'omegaC'],
                         'inventoryCarry': ['inventoryCarry'], 'wealthCarry': ['wealthCarry']}
            titles = {'cv': r'Price variation $v$', 'cp': r'Price level $P$',
                      'norm': r'$\zeta$ and $\eta$', 'normC': r'$\zeta_c$ and $\eta_c$', 'prop': r'$\rho$ and $\omega$',
                      'inventoryCarry': r'Inventory $I$', 'wealthCarry': r'Reserves $RW$'}
            ylabels = {'cv': r'$v$', 'cp':r'$P$',
                       'norm': r'$\zeta$, $\eta$', 'normC': r'$\zeta_c$, $\eta_c$', 'prop': r'$\rho$, $\omega$',
                       'inventoryCarry': r'$I$', 'wealthCarry': r'$RW$'}
            var_labels = {'cv': [r'$v$'], 'cp': [r'$P$'],
                          'norm': [r'$\zeta$', r'$\eta$'], 'normC': [r'$\zeta_c$', r'$\eta_c$'], 'prop': [r'$\rho$', r'$\omega$'],
                          'inventoryCarry': [r'$I$'], 'wealthCarry': [r'$RW$']}

            for value in parameter_values:
                if value < 0:
                    save_name = 'negative'
                else:
                    save_name = None
                for var in variableNames:
                    if var == 'cp':
                        normalize = True
                    else:
                        normalize = False
                    names = variables[var]
                    title = titles[var]
                    if value <  0:
                        title += ' after a Negative Monetary Shock'
                    else:
                        title += ' after a Positive Monetary Shock'
                    ylab = ylabels[var]
                    variables_labels = var_labels[var]
                    if var == 'wealthCarry' or var == 'inventoryCarry':
                        s_time = start_time + 400
                        e_time = end_time + 400
                        tick2Digits = False
                    else:
                        s_time = start_time
                        e_time = end_time
                        tick2Digits = True

                    plots.time_series_multiple_variables_specific_paramValue(csv_file=file,
                                                                             variables_names=names,
                                                                             parameter_name=parameter_name ,
                                                                             parameter_value=value,
                                                                             start_time=s_time,
                                                                             end_time=e_time,
                                                                             time_shift=time_shift,
                                                                             title=title,
                                                                             xlabel=xlabel,
                                                                             ylabel=ylab,
                                                                             variables_labels=variables_labels,
                                                                             n=3,
                                                                             save_name=save_name,
                                                                             fontSize_title=fontSize_title,
                                                                             fontSize_xlabel=fontSize_xlabel,
                                                                             fontSize_ylabel=fontSize_ylabel,
                                                                             fontSize_ticks=fontSize_ticks,
                                                                             colors=['royalblue','lightcoral','searoyalblue'],
                                                                             maxXticks=4,
                                                                             maxYticks=3,
                                                                             child_directory='plots_cantillon',
                                                                             tick2Digits=tick2Digits,
                                                                             normalize=normalize)

def mean_error_plots():
    legend = None
    legendSize = None
    fontSize_title = 26
    fontSize_xlabel = 24
    fontSize_ylabel = 24
    fontSize_ticks = 24

    networks = ['SF', 'B']
    parameters = ['s', 'g', 'alpha', 'd', 'n', 'psi', 'gamma', 'theta']
    variables = ['zetaCetaC', 'rhoC', 'omegaC', 'cv']
    shock_types = ['positive', 'negative']

    ylabels = {'zetaCetaC': r'$\phi$',
               'rhoC': r'$\rho$',
               'omegaC': r'$\omega$',
               'cv': r'$v$'}

    titles = {'zetaCetaC': r'Cumulative price decrease $\phi$',
              'rhoC': r'Prices below steady state $\rho$',
              'omegaC': r'Prices above steady state $\omega$',
              'cv': r'Price variation $v$'}

    statistics = {'zetaCetaC': 'ratio',
                  'rhoC': 'mean',
                  'omegaC': 'mean',
                  'cv': 'mean'}

    for network in networks:
        for param in parameters:
            files_dictionary = {}
            delValuesTreshold = None
            for var in variables:
                a = param + '_economy_' + network + '_CD_static_'
                if var == 'zetaCetaC':
                    a += 'ratio_'
                else:
                    a += 'stats_'
                a += var
                a += '.csv'
                files_dictionary[var] = a
                if param == 's':
                    for shock in shock_types:

                        if shock == 'negative':
                            save_name = shock
                            positive = False
                            cutOff = [-0.1,-0.001]

                        else:
                            save_name = None
                            positive = True
                            cutOff = [0.001, 0.1]

                        for name in files_dictionary:
                            file = files_dictionary[name]
                            ylabel = ylabels[name]
                            title = titles[name]
                            statistic = statistics[name]
                            if shock == 'negative' and var == 'zetaCetaC':
                                inverse = True
                                Ylabel = r'$1 - \phi$'
                                Title = r'Cumulative price increase $1 - \phi$'
                                saveName = save_name + '_phiInverse'

                            else:
                                inverse = False
                                Ylabel = ylabel
                                Title = title
                                saveName = save_name
                            plots.mean_error(csv_file=file,
                                             statistic=statistic,
                                             xlabel=r'Size of monetary shock $s$',
                                             ylabel=Ylabel,
                                             title_text=Title,
                                             color='black',
                                             save_name=saveName,
                                             positive=positive,
                                             ymin=None,
                                             ymax=None,
                                             xmin=None,
                                             xmax=None,
                                             inverse=inverse,
                                             legend=legend,
                                             legendSize=legendSize,
                                             fontSize_title=fontSize_title,
                                             fontSize_xlabel=fontSize_xlabel,
                                             fontSize_ylabel=fontSize_ylabel,
                                             fontSize_ticks=fontSize_ticks,
                                             child_directory='plots_cantillon',
                                             digits_yAxis=False,
                                             maxXticks=5,
                                             maxYticks=5,
                                             delValuesTreshold=delValuesTreshold,
                                             cutOff=cutOff)
                else:
                    cutOff = None
                    delValuesTreshold = None
                    if param == 'g':
                        xlabel = r'Homogeneity of monetary shock $g$'
                        xmax = None
                        xmin = None
                        cutOff = None
                    elif param == 'alpha':
                        xlabel = r'Cobb Douglas exponent $\alpha$'
                        xmax = None
                        xmin = None
                        cutOff = None
                        delValuesTreshold = 0.0099999
                    elif param == 'd':
                        xlabel = r'Degree of the production network $d$'
                        xmax = 11
                        xmin = None
                        cutOff = None
                    elif param == 'n':
                        xlabel = r'Number of firms in the economy $n$'
                        xmax = 10500
                        xmin = 500
                        cutOff = None
                    elif param == 'theta':
                        xlabel = r'Diversity in shock distribution $\theta$'
                        xmax = None
                        xmin = None
                        cutOff = None
                    elif param == 'gamma':
                        xlabel = r'Smoothening heterogeneity $\gamma$'
                        xmax = None
                        xmin = None
                        cutOff = None
                    elif param == 'psi':
                        xlabel = r'Smoothening mean $\psi$'
                        xmax = None
                        xmin = None
                        cutOff = None


                    for name in files_dictionary:
                        file = files_dictionary[name]
                        ylabel = ylabels[name]
                        title = titles[name]
                        statistic = statistics[name]
                        save_name = None
                        positive = True
                        plots.mean_error(csv_file=file,
                                         statistic=statistic,
                                         xlabel=xlabel,
                                         ylabel=ylabel,
                                         title_text=title,
                                         color='black',
                                         save_name=save_name,
                                         positive=positive,
                                         ymin=None,
                                         ymax=None,
                                         xmin=xmin,
                                         xmax=xmax,
                                         inverse=False,
                                         legend=legend,
                                         legendSize=legendSize,
                                         fontSize_title=fontSize_title,
                                         fontSize_xlabel=fontSize_xlabel,
                                         fontSize_ylabel=fontSize_ylabel,
                                         fontSize_ticks=fontSize_ticks,
                                         child_directory='plots_cantillon',
                                         digits_yAxis=False,
                                         maxXticks=5,
                                         maxYticks=5,
                                         delValuesTreshold=delValuesTreshold,
                                         cutOff=cutOff)

def convergence():
    legendSize = 18
    fontSize_title = 20
    fontSize_xlabel = 16
    fontSize_ylabel = 16
    fontSize_ticks = 16
    parameters = ['n', 'alpha', 'psi', 'gamma', 'd']
    xlabels = {'n': r'Number of firms $n$', 'alpha': r'Cobb Douglas exponent $\alpha$',
              'psi':r'Smoothening mean $\psi$', 'gamma':r'Smoothening heterogeneity $\gamma$',
              'd': r'Mean degree of production network $d$'}
    networks = ['SF', 'B']
    for network in networks:
        basic_name = '_economy_' + network + '_CD_static_convergence.csv'
        files = {}
        for param in parameters:
            name = param + basic_name
            files[param] = name

        for param in parameters:
            file = files[param]
            xlabel = xlabels[param]
            cutOff = None

            plots.convergence_mean_error(csv_file=file,
                                     thresholds=[5,10,12],
                                     legend=True,
                                     legendSize=legendSize,
                                     legend_location=9,
                                     fontSize_title=fontSize_title,
                                     fontSize_xlabel=fontSize_xlabel,
                                     fontSize_ylabel=fontSize_ylabel,
                                     fontSize_ticks=fontSize_ticks,
                                     child_directory='plots_cantillon',
                                     digits_yAxis=False,
                                     maxXticks=5,
                                     maxYticks=5,
                                     colors=['salmon', 'cornflowerblue', 'midnightblue'],
                                     title_text='Convergence',
                                     xlabel=xlabel,
                                     ylabel='Time steps',
                                     ymin=None,
                                     ymax=None,
                                     xmin=None,
                                     xmax=None,
                                     save_name=None,
                                     cutOff=cutOff)

def color_maps_multiVariable():
    plots.heat_map_simple(csv_file='average_meanmultiParam_sg_economy_SF_CD_static_stats_cv.csv',
                          x_var='s',
                          y_var='g',
                          color_var='mean')

def color_maps():
    fontSize_title = 24
    fontSize_xlabel = 22
    fontSize_ylabel = 22
    fontSize_ticks = 22
    networks = ['SF', 'B']
    names_variables = ['cv', 'rhoC', 'phiC']
    parameter_names = ['gamma', 'theta', 'psi', 'alpha', 'n', 'd', 's', 'g']
    ylabels = {'s': r'Size of monetary shock $s$',
               'g': r'Homogeneity of monetary shock $g$',
               'alpha': r'Cobb Douglas exponent $\alpha$',
               'n': r'Number of firms $n$',
               'd': r'Mean degree of production network $d$',
               'theta': r'Diversity in shock distribution $\theta$',
               'gamma': r'Smoothening heterogeneity $\gamma$',
               'psi': r'Smoothening mean $\psi$'}

    titles = {'cv': r'Price Variation $v^t$',
              'rhoC': r'Prices below steady state $\rho^t$',
              'omegaC': r'Prices above steady state $\omega^t$',
              'phiC': r'Cumulative price decrease $\phi^t$'}
    for param in parameter_names:
        for var in names_variables:
            if param == 'alpha':
                y_max_value = 0.8000001
            elif param == 'sigma':
                y_max_value = 0.40
            else:
                y_max_value = None
            if var == 'cv':
                time_begin = 25
                time_end = 35
            else:
                time_begin = 26
                time_end = 36

            if param == 'alpha':
                positive_threshold = 0.01
            else:
                positive_threshold = None
            for network in networks:
                file = param + '_economy_' + network + '_CD_static.csv'
                if var == 'phi':
                    z_name = None
                    ratio = True
                    ratio_variable_names = ['zeta', 'eta']
                elif var == 'phiC':
                    z_name = None
                    ratio = True
                    ratio_variable_names = ['zetaC', 'etaC']
                else:
                    z_name = var
                    ratio = False
                    ratio_variable_names = None
                ylabel = ylabels[param]
                title = titles[var]
                save_name = var


                plots.heat_map(csv_file=file,
                               y_name=param,
                               z_name=z_name,
                               x_label='Time steps',
                               y_label=ylabel,
                               title=title,
                               ratio=ratio,
                               ratio_variable_names=ratio_variable_names,
                               positive=True,
                               positive_threshold=positive_threshold,
                               save_name=save_name,
                               fontSize_title=fontSize_title,
                               fontSize_xlabel=fontSize_xlabel,
                               fontSize_ylabel=fontSize_ylabel,
                               fontSize_ticks=fontSize_ticks,
                               child_directory='plots_cantillon',
                               time_begin=time_begin,
                               time_end=time_end,
                               y_max_value=y_max_value,
                               inverse=False)

                if param == 's':
                    if var == 'rhoC':
                        z_name = 'omegaC'
                        title = titles[z_name]
                        save_name = z_name
                    else:
                        z_name = var
                        title = titles[var]
                        save_name = var

                    if var == 'phiC':
                        inverse = True
                        title =  r'Cumulative price increase $1 - \phi^t$'
                    else:
                        inverse = False
                        title = title

                    plots.heat_map(csv_file=file,
                                   y_name=param,
                                   z_name=z_name,
                                   x_label='Time steps',
                                   y_label=ylabel,
                                   title=title,
                                   ratio=ratio,
                                   ratio_variable_names=ratio_variable_names,
                                   positive=False,
                                   positive_threshold=positive_threshold,
                                   save_name=save_name,
                                   fontSize_title=fontSize_title,
                                   fontSize_xlabel=fontSize_xlabel,
                                   fontSize_ylabel=fontSize_ylabel,
                                   fontSize_ticks=fontSize_ticks,
                                   child_directory='plots_cantillon',
                                   time_begin=time_begin,
                                   time_end=time_end,
                                   y_max_value=y_max_value,
                                   inverse = inverse)

def all_plots():
    directory_name = 'data_cantillon'
    os.chdir(directory_name)

    time_begin = 15
    time_end = 70
    time_shift = 9

    #color_maps_multiVariable()

    
    print "plotting data Begins"
    multiNetworks_timePlots_SFB()

    price_timeSeries_histograms(time_begin, time_end, time_shift)
    print "histograms"

    timeSeries_positive_negative_shocks(time_begin, time_end, time_shift)
    print "time series positive negative"

    mean_error_plots()
    print "mean error"

    color_maps()
    print "color maps"

    convergence()
    print "convergence"

    priceDistribution()
    print "price distributions"

    contourJointVariable()
    print "joint parameters"

    print "plotting data Ends"
    
    os.chdir('..')



#all_plots()

