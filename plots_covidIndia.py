from __future__ import division
import pandas as pd
import matplotlib.pyplot as plt
import random as random
import numpy as np
from math import log
import math
import ast
from itertools import chain
from scipy import stats
from matplotlib.ticker import FormatStrFormatter
import collections
import os
import csv
import plots
import cPickle
import ast
import matplotlib.pyplot as plt

def con2():

	fontSize_supTitle = 18
	fontSize_xlabel = 14
	fontSize_ylabel = 14
	fontSize_ticks = 12

	data = pd.read_csv('alpha_economy_IO_CD.csv')
	d = data.iloc[0][:]
	d = ast.literal_eval(d[1])
	#print d[1]
	#print len(d[1])
	#print type(d[1])
	#print d
	d = list(d)
	d = d[0:100]
	d = [math.log(i,10) for i in d]
	y_tick_labels = {2:r'$10^2$',-2:r'$10^{-2}$',-6:r'$10^{-6}$',-10:r'$10^{-10}$',-14:r'$10^{-14}$',-18:r'$10^{-18}$'}

	plt.scatter(range(len(d)),d,marker='o',s=5, color='black', edgecolor='black')
	plt.ylabel(r"Mean price change ($\delta)$",fontsize=fontSize_ylabel)
	#plt.xlabel("Annual reciepts in USD (log base 10)", fontsize=fontSize_xlabel)
	plt.yticks(y_tick_labels.keys(), y_tick_labels.values())
	#plt.ylim(ymin=0)
	plt.xlabel("Time steps", fontsize=fontSize_xlabel)
	plt.title("Convergence to equilibrium", fontsize=fontSize_supTitle)
	plt.tick_params(axis='x', labelsize=fontSize_ticks)
	plt.tick_params(axis='y', labelsize=fontSize_ticks+2)
	plt.xlim(xmin=-10,xmax=110)
	plt.grid()
	directory_name = 'plots_covidIndia'
	os.chdir(directory_name)
	plt.savefig("convergence_ts.png", bbox_inches='tight')
	plt.close()
	os.chdir('..')

def convergence():
	file = 'alpha_economy_IO_CD_convergence.csv'
	legendSize = 18
	fontSize_title = 20
	fontSize_xlabel = 16
	fontSize_ylabel = 16
	fontSize_ticks = 16

	plots.convergence_mean_error(csv_file=file,
                                     thresholds=[5,10,12],
                                     legend=True,
                                     legendSize=legendSize,
                                     legend_location=9,
                                     fontSize_title=fontSize_title,
                                     fontSize_xlabel=fontSize_xlabel,
                                     fontSize_ylabel=fontSize_ylabel,
                                     fontSize_ticks=fontSize_ticks,
                                     child_directory='plots_covidIndia',
                                     digits_yAxis=False,
                                     maxXticks=5,
                                     maxYticks=5,
                                     colors=['salmon', 'cornflowerblue', 'midnightblue'],
                                     title_text='Convergence',
                                     xlabel=r'Cobb Douglas exponent $\alpha$',
                                     ylabel='Time steps',
                                     ymin=None,
                                     ymax=None,
                                     xmin=None,
                                     xmax=None,
                                     save_name=None,
                                     cutOff=None)

def time_series(labelFont,titleFont,ticksFont):
	data = pd.read_csv("normalized_gdp_equilibrium_prices_linear_price_stickiness_old_share_economy_network_CD.csv")
	data = pd.DataFrame(data)
	rows = data.shape[0]
	allData = {}
	for r in xrange(rows):
	    d = data.iloc[r][1:]
	    d = list(d)
	    v = data.iloc[r]["linear_price_stickiness_old_share"]
	    allData[v] = d

	#limit = 54
	limit = 36
	yVals = allData[0.0]
	#yVals = allData[0.1]
	xVals = range(len(yVals))
	xVals = [i-10 for i in xVals]

	yVals = yVals[5:limit]
	xVals = xVals[5:limit]

	print min(yVals), "min GDP"


	baseTitle = 'Time series of GDP'
	baseXLabel = 'Weeks'
	baseYLabel = 'GDP (normalized)'

	fig, ax = plt.subplots()



	plt.plot(xVals, yVals, color='black', linestyle='-')
	plt.scatter(xVals, yVals,color='black',s=3)
	plt.suptitle(baseTitle, fontsize=titleFont)
	plt.xlabel(baseXLabel, fontsize=labelFont)
	plt.ylabel(baseYLabel, fontsize=labelFont)
	plt.tick_params(axis='both', labelsize=ticksFont)
	plt.ticklabel_format(style='plain',axis='both',useOffset=False)

	dirY = [0.571]*8
	dirX = [1,2,3,4,5,6,7,8]
	plt.scatter(dirX, dirY,color='royalblue',s=3)





	#eY = [0.774,0.774,0.774,0.774,0.774,0.774,0.774,0.774]
	#eX = [1,2,3,4,5,6,7,8]
	#plt.scatter(eX, eY,color='salmon',s=3)

	ax.hlines(y=0.571,xmax=8,xmin=1,label='Direct',color='royalblue',linestyle='--',lw=1.5)
	legendFont = labelFont - 4
	plt.legend(loc=4,fontsize=labelFont)
	plt.grid()    
	saveName =  'time_series_output.png'

	child_directory = 'plots_covidIndia'
	if not os.path.exists(child_directory):
	    os.makedirs(child_directory)    
	os.chdir(child_directory)
	plt.savefig(saveName, bbox_inches='tight')
	plt.close()
	os.chdir('..')

def sectoral_lockdown_multiple(labelFont,titleFont,ticksFont):
	sectors = collections.OrderedDict({})
	with open("sectors_output.txt",'r') as file:
		for line in file:
			line = line.split(",")
			sec = line[0]
			vals = line[1:]
			vals = [float(i) for i in vals]
			#vals = vals[10:20]
			sectors[sec] = vals
			print sec, vals

	# compute mean weekly output during lockdown
	mean_sectors_output = collections.OrderedDict({})
	for sec in sectors:
		vals = sectors[sec]
		v = min(vals)
		mean_sectors_output[sec] = v

	print mean_sectors_output

	qty_constraint = collections.OrderedDict({})
	with open("lockdown_prop.txt",'r') as file:
		for line in file:
			line = line.split(",")
			sec = line[0]
			val = float(line[1])
			qty_constraint[sec] = val

	ratio = collections.OrderedDict({})


	for sec in qty_constraint:
		actual = mean_sectors_output[sec]
		const = qty_constraint[sec]
		r = actual / const
		ratio[sec] = r





	sector_names = collections.OrderedDict({})
	sector_names[1] = 'Agriculture, hunting, forestry, and fishing'
	sector_names[2] = 'Mining and quarrying'
	sector_names[3] = 'Food, beverages, and tobacco'
	sector_names[4] = 'Textiles and textile products'
	sector_names[5] = 'Leather, leather products, and footwear'
	sector_names[6] = 'Wood and products of wood and cork'
	sector_names[7] = 'Pulp, paper, paper products, printing, and publishing'
	sector_names[8] = 'Coke, refined petroleum, and nuclear fuel'
	sector_names[9] = 'Chemicals and chemical products'
	sector_names[10] = 'Rubber and plastics'
	sector_names[11] = 'Other nonmetallic minerals'
	sector_names[12] = 'Basic metals and fabricated metal'
	sector_names[13] = 'Machinery, nec'
	sector_names[14] = 'Electrical and optical equipment'
	sector_names[15] = 'Transport equipment'
	sector_names[16] = 'Manufacturing, nec; recycling'
	sector_names[17] = 'Electricity, gas, and water supply'
	sector_names[18] = 'Construction'
	sector_names[19] = 'Sale, maintenance, and repair of motor vehicles and motorcycles; retail sale of fuel'
	sector_names[20] = 'Wholesale trade and commission trade, except of motor vehicles and motorcycles'
	sector_names[21] = 'Retail trade, except of motor vehicles and motorcycles; repair of household goods'
	sector_names[22] = 'Hotels and restaurants'
	sector_names[23] = 'Inland transport'
	sector_names[24] = 'Water transport'
	sector_names[25] = 'Air transport'
	sector_names[26] = 'Other supporting and auxiliary transport activities; activities of travel agencies'
	sector_names[27] = 'Post and telecommunications'
	sector_names[28] = 'Financial intermediation'
	sector_names[29] = 'Real estate activities'
	sector_names[30] = 'Renting of M&Eq and other business activities'
	sector_names[31] = 'Education'
	sector_names[32] = 'Health and social work'
	sector_names[33] = 'Other community, social, and personal services'


	vals_dict = {}
	for r in ratio:
		v = ratio[r]
		vals_dict[v] = r

	keys = vals_dict.keys()
	keys = sorted(keys)

	print keys

	xVals = []
	yVals = []

	for k in keys:
		x = vals_dict[k]
		xVals.append(x)
		yVals.append(k)

	with open("sec_multiple.txt",'w') as file:
		for i in xrange(len(yVals)):
			y = yVals[i]
			x = xVals[i]
			pair = [str(x),str(y)]
			pair = ",".join(pair)
			file.write("%s" %pair + '\n')


	
	xVals_dict = collections.OrderedDict({})
	count = 1
	for x in xVals:
		xVals_dict[x] = count
		count += 1


	plt.scatter(xVals_dict.values(),yVals,color='black')
	plt.suptitle("Sectoral network multiple", fontsize=titleFont)
	plt.ylabel("Normalized output", fontsize=labelFont)
	plt.xlabel("Sectors", fontsize=labelFont)
	plt.tick_params(axis='both', labelsize=ticksFont)
	plt.ticklabel_format(style='plain',axis='both',useOffset=False)
	plt.grid()    
	saveName =  'network_multiple.png'

	child_directory = 'plots_covidIndia'
	if not os.path.exists(child_directory):
	    os.makedirs(child_directory)    
	os.chdir(child_directory)
	plt.savefig(saveName, bbox_inches='tight')
	plt.close()
	os.chdir('..')
	



def sectors_series(labelFont,titleFont,ticksFont):
	sectors = collections.OrderedDict({})
	with open("sectors_output.txt",'r') as file:
		for line in file:
			line = line.split(",")
			sec = line[0]
			vals = line[1:]
			vals = [float(i) for i in vals]
			sectors[sec] = vals

	l = 36
	for sec in sectors:
		vals = sectors[sec]
		vals = vals[5:l]
		
		plt.scatter(range(-5,len(vals)-5),vals,s=2,color='grey')
		plt.plot(range(-5,len(vals)-5),vals)

		
		if min(vals) < 0.05:
			print sec, min(vals)

	plt.suptitle("Supply of sectoral output", fontsize=titleFont)
	plt.ylabel("Normalized output", fontsize=labelFont)
	plt.xlabel("Weeks", fontsize=labelFont)
	plt.tick_params(axis='both', labelsize=ticksFont)
	plt.ticklabel_format(style='plain',axis='both',useOffset=False)
	plt.grid()    
	saveName =  'time_series_sectors.png'

	child_directory = 'plots_covidIndia'
	if not os.path.exists(child_directory):
	    os.makedirs(child_directory)    
	os.chdir(child_directory)
	plt.savefig(saveName, bbox_inches='tight')
	plt.close()
	os.chdir('..')

def time_series_price_stickiness(labelFont,titleFont,ticksFont):
	data = pd.read_csv("normalized_gdp_equilibrium_prices_linear_price_stickiness_old_share_economy_network_CD.csv")
	data = pd.DataFrame(data)
	rows = data.shape[0]
	allData = {}
	for r in xrange(rows):
	    d = data.iloc[r][1:]
	    d = list(d)
	    v = data.iloc[r]["linear_price_stickiness_old_share"]
	    allData[v] = d

	limit = 54
	#limit = 36
	yVals = allData[0.0]
	#yVals = allData[0.1]
	xVals = range(len(yVals))
	xVals = [i-10 for i in xVals]

	yVals = yVals[5:limit]
	xVals = xVals[5:limit]

	print min(yVals), "min GDP"


	baseTitle = 'Time series of GDP'
	baseXLabel = 'Weeks'
	baseYLabel = 'GDP (normalized)'

	fig, ax = plt.subplots()



	plt.plot(xVals, yVals, color='black', linestyle='-',label=r'$\rho=0$',lw=2)
	plt.scatter(xVals, yVals,color='black',s=3)

	yVals = allData[0.2]
	yVals = yVals[5:limit]
	plt.plot(xVals, yVals, color='royalblue', linestyle='-',label=r'$\rho=0.2$',lw=2)
	plt.scatter(xVals, yVals,color='royalblue',s=3)

	yVals = allData[0.5]
	yVals = yVals[5:limit]
	

	plt.plot(xVals, yVals, color='salmon', linestyle='-',label=r'$\rho=0.5$',lw=2)
	plt.scatter(xVals, yVals,color='salmon',s=3)

	yVals = allData[0.8]
	yVals = yVals[5:limit]
	

	plt.plot(xVals, yVals, color='green', linestyle='-',label=r'$\rho=0.9$',lw=2)
	plt.scatter(xVals, yVals,color='green',s=3)


	plt.suptitle(baseTitle, fontsize=titleFont)
	plt.xlabel(baseXLabel, fontsize=labelFont)
	plt.ylabel(baseYLabel, fontsize=labelFont)
	plt.tick_params(axis='both', labelsize=ticksFont)
	plt.ticklabel_format(style='plain',axis='both',useOffset=False)
	plt.legend()



	
	legendFont = labelFont - 4
	plt.legend(loc=4,fontsize=labelFont)
	plt.grid()    
	saveName =  'time_series_priceStickiness.png'

	child_directory = 'plots_covidIndia'
	if not os.path.exists(child_directory):
	    os.makedirs(child_directory)    
	os.chdir(child_directory)
	plt.savefig(saveName, bbox_inches='tight')
	plt.close()
	os.chdir('..')

def overshooting(labelFont,titleFont,ticksFont):
	data = pd.read_csv("normalized_gdp_equilibrium_prices_linear_price_stickiness_old_share_economy_network_CD.csv")
	data = pd.DataFrame(data)
	rows = data.shape[0]
	allData = {}
	for r in xrange(rows):
	    d = data.iloc[r][1:]
	    d = list(d)
	    v = data.iloc[r]["linear_price_stickiness_old_share"]
	    allData[v] = d

	overshooting_size = collections.OrderedDict({})
	for v in allData:
		overshooting_size[v] = 0


	for v in allData:
		data = allData[v]
		for i in data:
			if i > 1.001:
				sh = i-1
				overshooting_size[v] += sh

	for v in overshooting_size:
		overshooting_size[v] /= 4.5


	baseTitle = 'Size of overshooting'
	baseXLabel = r'Price stickiness $\rho$'
	baseYLabel = 'Proportion of normal monthly GDP'

	vals = overshooting_size.keys()
	vals = sorted(vals)

	xVals = []
	yVals = []

	for v in vals:
		xVals.append(v)
		yVals.append(overshooting_size[v])


	plt.plot(xVals, yVals, color='black', linestyle='-',lw=1)
	plt.scatter(xVals, yVals,color='black',s=3)


	plt.suptitle(baseTitle, fontsize=titleFont)
	plt.xlabel(baseXLabel, fontsize=labelFont)
	plt.ylabel(baseYLabel, fontsize=labelFont)
	plt.tick_params(axis='both', labelsize=ticksFont)
	plt.ticklabel_format(style='plain',axis='both',useOffset=False)
	

	
	legendFont = labelFont - 4
	plt.legend(loc=4,fontsize=labelFont)
	plt.grid()    
	saveName =  'overshooting_size.png'

	child_directory = 'plots_covidIndia'
	if not os.path.exists(child_directory):
	    os.makedirs(child_directory)    
	os.chdir(child_directory)
	plt.savefig(saveName, bbox_inches='tight')
	plt.close()
	os.chdir('..')
	

	# measure the size of overshooting with respect to price stickiness

def all_plots():
	labelFont = 18
	titleFont = 22
	ticksFont = 16
	directory_name = 'data_covidIndia'
	os.chdir(directory_name)
	#convergence()
	#con2()
	sectoral_lockdown_multiple(labelFont,titleFont,ticksFont)
	time_series(labelFont=labelFont,titleFont=titleFont,ticksFont=ticksFont)
	sectors_series(labelFont,titleFont,ticksFont)
	time_series_price_stickiness(labelFont,titleFont,ticksFont)
	overshooting(labelFont,titleFont,ticksFont)



	os.chdir('..')



all_plots()