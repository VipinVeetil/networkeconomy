from __future__ import division
import run
import numpy as np
import parameters as p
import math
from multiprocessing import Pool
import gc
import copy
import matplotlib.pyplot as plt
import cPickle
gc.enable()
import run
import parameters


p = parameters.Parameters()
p.data_time_series['economy'] = True
p.record_variables['economy']['cp'] = True
p.record_variables['economy']['cg'] = True
p.record_variables['economy']['pg'] = True
p.record_variables['economy']['pp'] = True
p.record_variables['economy']['wealth'] = True
p.record_variables['economy']['welfare_mean'] = True
p.record_variables['economy']['welfare_cv'] = True

p.number_of_firms = 1000
#p.number_of_households = 1000
#p.probability_price_change = 0.75
#p.endogenous_probability_price_change = True
#p.probability_weights_change = 0.1
#p.endogenous_probability_weights_change =  True
p.monetary_shock = True
p.s = -0.001
p.network_type = 'scale_free_directional'
p.network_from_file = True
p.representative_household = True
p.inflation_agents_ID = 'file'
p.all_firms_retail = 'file'
p.money_injection_agents = 'suppliers_retail_firms'
#p.money_injection_agents = 'file'
p.household_preference_homogeneous = False
p.production_function = 'CD'
#p.weights_dynamic = True
p.z = 0.025
p.Cobb_Douglas_exponent_labor = 0.1
p.CES_exponent = -0.5
p.negative_shocks_by_wealth = 'Small'
#p.households_factor = 10
#p.probability_price_change = 0.75



p.time_steps = 50
p.transient_time_steps = 50
p.monetary_shock_time_step = 25

run_instance = run.Run(p)
run_instance.create_economy()
run_instance.transient()
run_instance.time_steps()

consumer_price = []
producer_price = []
consumer_goods = []
producer_goods = []

stable_Cprice = run_instance.samayam.economy.cp[8]
stable_Pprice = run_instance.samayam.economy.pp[8]
stable_Cgoods = run_instance.samayam.economy.cg[8]
stable_Pgoods = run_instance.samayam.economy.pg[8]
print len(run_instance.samayam.economy.cp)
n = run_instance.samayam.economy.number_of_non_retail_firms
print n, "non retail firms"
m = len(run_instance.samayam.economy.firms_ID)
print m, "all firms "

for i in run_instance.samayam.economy.cp:
	consumer_price.append(i / stable_Cprice)

for i in run_instance.samayam.economy.pp:
	producer_price.append(i / stable_Pprice)

for i in run_instance.samayam.economy.cg:
	consumer_goods.append(i / stable_Cgoods)

for i in run_instance.samayam.economy.pg:
	producer_goods.append(i / stable_Pgoods)




x=24
time_steps = range(-x, (len(consumer_price)-x))

print len(time_steps)
print len(consumer_price)

plt.plot(time_steps, consumer_price)
ax = plt.gca()
ax.get_yaxis().get_major_formatter().set_useOffset(False)
plt.draw()
plt.title('US Economy Production Network')
plt.xlabel('Time steps')
plt.ylabel('Consumer Price Index')
plt.text(10, 1.001, '31,783 firms')
plt.text(10, 1.00085, '92,822 linkages')
#plt.legend()
plt.grid()
plt.savefig('consumer_prices_new.png')
plt.close()



plt.plot(consumer_goods, label = 'consumer goods')
plt.plot(producer_goods, label = 'producer goods')
plt.title('goods')
plt.legend()
plt.savefig('goods.png')
plt.close()


plt.plot(producer_price)
plt.title('producer price')
plt.savefig('producer_price.png')
plt.close()

plt.plot(consumer_goods)
plt.title('consumer goods')
plt.savefig('consumer_goods.png')
plt.close()

plt.plot(producer_goods)
plt.title('producer goods')
plt.savefig('producer_goods.png')
plt.close()

plt.plot(run_instance.samayam.economy.welfare_mean)
plt.title('welfare mean')
plt.savefig('welfare_mean.png')
plt.close()


plt.plot(run_instance.samayam.economy.welfare_cv)
plt.title('welfare cv')
plt.savefig('welfare_cv.png')
plt.close()

plt.plot(run_instance.samayam.economy.wealth)
plt.title('wealth')
plt.savefig('wealth.png')
plt.close()

