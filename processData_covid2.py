import os.path, sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir))
import os
import processData
import pandas as pd
import csv
import cPickle
from math import log
import numpy as np
import ast
import matplotlib.pyplot as plt



def convergence():
	file = 'labor_mobility_economy_SF_CES_convergence__.csv'
	processData.convergence_time(csv_file=file,
                                         thresholds=[5,10,12],
                                         variable='mean_price_change',
                                         save_name=None)

def normalize():

	files = ['labor_mobility_economy_networkUS70_CES.csv',\
	'labor_mobility_economy_networkUS70_CES_laborMobFull.csv',\
	'lockdownShock_multiple_economy_networkUS70_CES.csv',\
	'sigma_economy_networkUS70_CES.csv',\
	#'labor_mobility_economy_SF_CES.csv',\
	'linear_price_stickiness_old_share_economy_networkUS70_CES.csv',\
	'price_sensitivity_economy_networkUS70_CES.csv']


	variables = ['gdp_equilibrium_prices']#,'inventory_equi_prices','inventory_equi_prices_PCE']

	#normal_position = 0
	normal_position = 10
	transient_time = 0

	for csv_file in files:		
		processData.normalize(csv_file=csv_file,variables=variables,normPosition=normal_position,transientTime=transient_time)
	
	
	
	normal_position = 0
	transient_time = 0

	file = 'one_time_productivity_shock_std_economy_SF_CES.csv'
	variables = ['gdp_equilibrium_prices','unemployment_rate']
		
	processData.normalize(csv_file=file,variables=variables,normPosition=normal_position,transientTime=transient_time)
	


	"""
	normal_position = 0
	transient_time = 0
	
	file = 'alpha_economy_networkUS70_CES_lockdownProductivityEquilibrium.csv'
	variables = ['gdp_equilibrium_prices']
	processData.normalize(csv_file=file,variables=variables,normPosition=normal_position,transientTime=transient_time)
	
	
	file = 'alpha_economy_networkUS70_CES_lockdownProductivity_disequilibrium.csv'
	processData.normalize(csv_file=file,variables=variables,normPosition=normal_position,transientTime=transient_time)
	"""

def normalize_inventories():
	normValue = 10000

	transientTime = 0
	#files = ['labor_mobility_economy_networkUS70_CES.csv',
	#'linear_price_stickiness_old_share_economy_networkUS70_CES.csv']
	files = ['linear_price_stickiness_old_share_economy_networkUS70_CES.csv']

	variables = ['inventory_equi_prices','inventory_equi_prices_PCE']

	for csv_file in files:
		data = pd.read_csv(csv_file)
		data = pd.DataFrame(data)
		headers = data.columns.values
		parameter_name = headers[0]
		rows = data.shape[0]

		for var in variables:
			file_name = 'normalized_' + var + '_' + csv_file
			with open('%s' % file_name, 'wb') as data_csv:
				writer_data = csv.writer(data_csv, delimiter=',')
				d = ast.literal_eval(data.iloc[0][var])
				d = d[transientTime:]
				times = len(d)
				cols = ['t'+ str(i) for i in range(1,times+1)]
				writer_data.writerow([parameter_name] + cols)
				for r in xrange(rows):
					paramValue = data.iloc[r][parameter_name]
					d = ast.literal_eval(data.iloc[r][var])
					d = list(d)
					d = d[transientTime:]
					#normValue = d[normPosition]
					#normValue = max(d)
					#if normValue > 0:
					d = [float(i)/float(normValue) for i in d]
					writer_data.writerow([paramValue] + d)
					#else:
					#writer_data.writerow([paramValue] + ['na'])

def pickled():
	"""
	file = '0.3priceStickiness.cPickle'
	E = cPickle.load(open(file, "rb"))
	data3 = getattr(E, 'intermediate_share_currentOutput')
	file = '0.6priceStickiness.cPickle'
	E = cPickle.load(open(file, "rb"))
	data6 = getattr(E, 'intermediate_share_currentOutput')
	file = '0.9priceStickiness.cPickle'
	E = cPickle.load(open(file, "rb"))
	data9 = getattr(E, 'intermediate_share_currentOutput')
	
	file_name = 'intermediate_share.csv'
	with open('%s' % file_name, 'wb') as data_csv:
		writer_data = csv.writer(data_csv, delimiter=',')
		writer_data.writerow(['priceStickiness'] + range(len(data3)))
		writer_data.writerow([0.3] + data3)
		writer_data.writerow([0.6] + data6)
		writer_data.writerow([0.9] + data9)


	file = '0.3priceStickiness.cPickle'
	E = cPickle.load(open(file, "rb"))
	data3 = getattr(E, 'inventory_prop')
	file = '0.6priceStickiness.cPickle'
	E = cPickle.load(open(file, "rb"))
	data6 = getattr(E, 'inventory_prop')
	file = '0.9priceStickiness.cPickle'
	E = cPickle.load(open(file, "rb"))
	data9 = getattr(E, 'inventory_prop')
	
	file_name = 'inventory_prop.csv'
	with open('%s' % file_name, 'wb') as data_csv:
		writer_data = csv.writer(data_csv, delimiter=',')
		writer_data.writerow(['priceStickiness'] + range(len(data3)))
		writer_data.writerow([0.3] + data3)
		writer_data.writerow([0.6] + data6)
		writer_data.writerow([0.9] + data9)
	

	file = '0.9priceStickiness.cPickle'
	E = cPickle.load(open(file, "rb"))
	data9 = getattr(E, 'intermediate_share_firms')
	#print data9, "deviations"
	file_name = 'intermediate_share_firms.csv'
	with open('%s' % file_name, 'wb') as data_csv:
		writer_data = csv.writer(data_csv, delimiter=',')
		writer_data.writerow(['time_steps']+range(len(data9[0])))
		count = 0
		counts = [66,67,68]
		for vals in data9:
			if count in counts:
				vals.insert(0,count)
				writer_data.writerow(vals)
			count += 1	


	"""
	file = '0.3priceStickiness.cPickle'
	E = cPickle.load(open(file, "rb"))
	gdp = getattr(E, 'gdp_equilibrium_prices')

	file = '0.3priceStickiness.cPickle'
	E = cPickle.load(open(file, "rb"))
	gdp2 = getattr(E, 'priceLevel')
	
	#intermediate_share_currentOutput

	gdp_fixed = getattr(E, 'gdp_fixed_shareSec_equilibrium_prices')
	output = getattr(E, 'sum_output_equilibrium_prices')
	intermediate_share = getattr(E, 'intermediate_share')
	consumer_demand = getattr(E, 'consumer_demand')
	intermediate_demand = getattr(E, 'intermediate_demand')
	wealth = getattr(E, 'wealth')
	inventory_prop = getattr(E, 'inventory_prop')
	unemployment_rate = getattr(E, 'unemployment_rate')

	total_demand = []

	for i in xrange(len(consumer_demand)):
		c = consumer_demand[i]
		t = intermediate_demand[i]
		total_demand.append(c+t)

	share = []
	for i in xrange(len(consumer_demand)):
		c = consumer_demand[i]
		t = total_demand[i]
		s = c/t
		share.append(1-s)

	#share = share[40:100]
	print max(wealth), "wealth max"
	print min(wealth), "wealth min"
	#plt.scatter(range(len(share)),share,s=2)
	#plt.plot(share)
	#plt.show()
	#plt.plot(gdp)
	#plt.plot(gdp_fixed)
	#plt.plot(consumer_demand,color='black')
	
	#plt.plot(wealth[40:100],color='blue')
	#plt.plot(wealth,color='blue')
	#plt.scatter(range(len(wealth)),wealth)
	#plt.plot(output)

	norm_output = [i/output[8] for i in output]
	norm_gdp = [i/gdp[8] for i in gdp]
	norm_gdp2 = [i/gdp2[8] for i in gdp2]

	norm_gdp = norm_gdp[55:95]
	norm_gdp2 = norm_gdp2[55:95]
	
	share = share[55:95]
	#plt.plot(unemployment_rate)
	#plt.plot(share)
	#plt.scatter(range(len(share)),share)
	#plt.plot(norm_output,color='black')
	#plt.scatter(range(len(norm_output)),norm_output)
	#plt.plot(intermediate_demand,color='blue')
	plt.plot(norm_gdp, color='pink')
	plt.plot(norm_gdp2, color='black')
	plt.scatter(range(len(norm_gdp)),norm_gdp,color='pink')
	plt.scatter(range(len(norm_gdp)),norm_gdp2,color='black')
	#plt.plot(intermediate_share,color='black')
	#plt.scatter(range(len(norm_gdp)),intermediate_share,color='black')
	#plt.plot(inventory_prop, color='red')
	plt.grid()
	plt.show()



	#print len(data3)
	#norm_data = [i/data9[50] for i in data9]
	#plt.plot(data3,color='black')
	#plt.plot(data6,color='grey')
	#plt.plot(data9,color='blue')
	#plt.plot(data6)
	#plt.plot(data6)
	#plt.xlim(xmin=0.9,xmax=1.1)
	#plt.show()


	
#                      [,'inventory_prop']



def process():
	directory_name = 'data_covid2'
	os.chdir(directory_name)
	normalize()
	#normalize_inventories()
	#convergence()
	pickled()
	os.chdir('..')	


process()