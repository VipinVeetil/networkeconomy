from __future__ import division
import os.path, sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir))
import os
import processData
import pandas as pd
import ast

def convergence():
	file = 'alpha_economy_IO_CD.csv'
	processData.convergence_time(csv_file=file,
		thresholds=[5,10,12],
		variable='mean_price_change',
		save_name=None)

def normalize():
	files = ['linear_price_stickiness_old_share_economy_network_CD.csv']
	variables = ['gdp_equilibrium_prices']
	normal_position = 0
	transient_time = 0
	for csv_file in files:		
		processData.normalize(csv_file=csv_file,variables=variables,normPosition=normal_position,transientTime=transient_time)

def normalize_sector_data():
	file = 'linear_price_stickiness_old_share_firm_network_CD.csv'
	data = pd.read_csv(file)
	outputs = ast.literal_eval(data.iloc[1]["total_output"])
	#outputs = ast.literal_eval(data.iloc[1]["output_sold"])
	with open("sectors_output.txt",'w') as file:
		for r in xrange(1,34):
			ss = outputs[r][0]
			norm_output = [i/ss for i in outputs[r]]
			norm_output.insert(0,r)
			norm_output = [str(i) for i in norm_output]
			norm_output = ",".join(norm_output)
			file.write("%s " % norm_output + '\n')

def compute_quarterly_GDP():
	data = pd.read_csv("normalized_gdp_equilibrium_prices_linear_price_stickiness_old_share_economy_network_CD.csv")
	data = pd.DataFrame(data)

	data = data.iloc[0][:]

	q1 = 0
	for r in xrange(1,13):
		i = 't' + str(r)
		val = data[i]
		q1 += val
	q1 += 1
	print q1/13

	q2 = 0
	for r in xrange(13,26):
		i = 't' + str(r)
		val = data[i]
		q2 += val
	print q2/13

	q3 = 0
	for r in xrange(26,39):
		i = 't' + str(r)
		val = data[i]
		q3 += val
	print q3/13

	q4 = 0
	for r in xrange(39,53):
		i = 't' + str(r)
		val = data[i]
		q4 += val
	print q4/13
	
def process():
    directory_name = 'data_covidIndia'
    os.chdir(directory_name)
    #convergence()
    normalize()
    normalize_sector_data()
    compute_quarterly_GDP()
    os.chdir('..')


process()