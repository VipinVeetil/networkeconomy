from __future__ import division
import os.path, sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir))
import os
import processData

def summary_multiParam():
    processData.multiParam_mean(csv_file='multiParam_sg_economy_SF_CD_static_stats_cv.csv',
                    para_names=['s','g'], variable_name='mean')

    processData.multiParam_mean(csv_file='multiParam_sd_economy_SF_CD_static_stats_cv.csv',
                                para_names=['s', 'd'], variable_name='mean')

    processData.multiParam_mean(csv_file='multiParam_gd_economy_SF_CD_static_stats_cv.csv',
                                para_names=['g', 'd'], variable_name='mean')

    processData.multiParam_mean(csv_file='multiParam_sg_economy_SF_CD_static_stats_rhoC.csv',
                                para_names=['s', 'g'], variable_name='mean')

    processData.multiParam_mean(csv_file='multiParam_sd_economy_SF_CD_static_stats_rhoC.csv',
                                para_names=['s', 'd'], variable_name='mean')

    processData.multiParam_mean(csv_file='multiParam_gd_economy_SF_CD_static_stats_rhoC.csv',
                                para_names=['g', 'd'], variable_name='mean')


def write_firm_prices():
    shocks = ['positive', 'negative']
    networks = ['B', 'SF']
    types = [None, 'smoothening', 'noisyShock']
    for shock in shocks:
        for network in networks:
            for type in types:
                if type == 'noisyShock':
                    a = 'positive'
                else:
                    a = shock
                file = 'economyPickle_CD_' + network + '_' + a
                if type is not None:
                    file += '_'
                    file += type
                file += '.cPickle'
                processData.write_pickled_economy(pickle_file=file,
                      agents_variables={'firms': ['price_time_series']},
                      normalize=True,
                      stable_time_step=5)

def v_rhoC_omegaC_statistics(interim_start, interim_end):
    networks_function = ['_economy_SF_CD_static.csv', '_economy_B_CD_static.csv']
    #parameter_names = ['multiParam_sg', 's', 'g', 'd', 'n', 'alpha', 'gamma', 'psi', 'theta']
    #parameter_names = ['multiParam_sgd']
    #parameter_names = ['multiParam_sg']
    parameter_names = ['multiParam_sg', 'multiParam_sd', 'multiParam_gd']
    variables = ['rhoC', 'omegaC', 'cv', 'cp']
    for item in networks_function:
        for param in parameter_names:
            file = param + item
            for var in variables:
                if var == 'cv' or 'var' == 'cp':
                    start = interim_start
                    end = interim_end
                else:
                    start = interim_start + 1
                    end = interim_end + 1

                processData.min_max(csv_file=file,
                            variable_name=var,
                            interim_start_time=start,
                            interim_end_time=end)

def price_norm_ratio(interim_start, interim_end):
    networks_function = ['_economy_SF_CD_static.csv', '_economy_B_CD_static.csv']
    parameter_names = ['s', 'g', 'd', 'n', 'alpha', 'gamma', 'psi', 'theta']
    variables = {'zetaC': 'etaC'}
    start = interim_start + 1
    end = interim_end + 1
    threshold = None
    for item in networks_function:
        for param in parameter_names:
            file = param + item
            for var in variables:
                denominator = variables[var]
                processData.sum_ratio(
                csv_file=file,
                v1=var,
                v2=denominator,
                interim_start=start,
                interim_end=end,
                save_name=None,
                threshold = threshold)

def convergence():
    networks_function = ['_economy_SF_CD_static.csv', '_economy_B_CD_static.csv']
    parameter_names = ['d', 'n', 'alpha', 'gamma', 'psi']
    for item in networks_function:
        for param in parameter_names:
            file = param + item
            processData.convergence_time(csv_file=file,
                                         thresholds=[5,10,12],
                                         variable='mean_price_change',
                                         save_name=None)

def process():
    directory_name = 'data_cantillon'
    os.chdir(directory_name)
    begin = 25
    end = 30

    print "process data Begins"
    #write_firm_prices()
    print "prices written"

    v_rhoC_omegaC_statistics(begin, end)
    print "v rho omega written"

    #price_norm_ratio(begin, end)
    print "price norm written"

    #convergence()
    print "convergence written"
    print "process data Ends"

    summary_multiParam()

    os.chdir('..')


process()