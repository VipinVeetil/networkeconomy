from __future__ import division
import os.path, sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir))
import os
import processData
import pandas as pd
import csv
import cPickle
from math import log
import numpy as np
import ast
import matplotlib.pyplot as plt

def return_changes(list_vals):
	m = np.mean(list_vals)
	list_vals = [i/m for i in list_vals]
	changes = []
	for j in xrange(len(list_vals)-1):
		v0 = list_vals[j]
		v1 = list_vals[j+1]
		changes.append(v1/v0)
	return changes

def return_yearly(list_vals):
	gdp = []
		
	count = 0
	year_gdp = 0
	for i in xrange(len(list_vals)):
		val = list_vals[i]
		year_gdp += val
		count += 1
		if count == 12:
			gdp.append(year_gdp)
			year_gdp = 0
			count = 0
	return gdp

def multi():
	file = ["multiParam_dprobability_firm_input_seller_change_economy_SF_endo100000.csv",
	'multiParam_linear_price_stickiness_old_shareprobability_firm_input_seller_change_economy_SF_endo100000.csv']
	for f in file:
		if f == "multiParam_dprobability_firm_input_seller_change_economy_SF_endo100000.csv":
			first_param = "d"
			second_param = "probability_firm_input_seller_change"
		elif f == "multiParam_linear_price_stickiness_old_shareprobability_firm_input_seller_change_economy_SF_endo100000.csv":
			first_param = "linear_price_stickiness_old_share"
			second_param = "probability_firm_input_seller_change"

		data = pd.read_csv(f)
		data = pd.DataFrame(data)
		rows = data.shape[0]
		name = "multi" + "_" + "gdp" + "_" + f
		with open(name,'wb') as data_csv:			
			writer_data = csv.writer(data_csv, delimiter=',')
			writer_data.writerow([first_param,second_param,'vol'])	
			name = first_param + second_param + '.dat'					
			with open(name,'w') as file:
				#l = [first_param,second_param,'vol']
				#l = " ".join(l)
				#file.write('%s ' % l + '\n')							
				for r in xrange(rows):
					gdp = data.iloc[r]["gdp"]
					gdp = ast.literal_eval(gdp)
					gdp = return_yearly(gdp)				
					changes = return_changes(gdp)
					vol = np.std(changes)				
					first = data.iloc[r][first_param]
					second = data.iloc[r][second_param]
					l = [first,second,vol]
					#print l
					writer_data.writerow(l)



					l = [str(i) for i in l]
					l = " ".join(l)
					file.write('%s ' % l + '\n')					

def convergence_endo_stop():
	#file = 'n_economy_SF_CD_endo_stop.csv'
	file = 'n_economy_SF_CES_endo_stop.csv'
	processData.convergence_time1(csv_file=file,
                                         thresholds=range(1,11),
                                         variable='mean_price_change',
                                         save_name=None,
                                         beginStep=100)

def convergence_endoStep():
	#file = 'n_economy_SF_CD_endoStep.csv'
	file = 'n_economy_SF_CES_endoStep.csv'
	processData.convergence_time1(csv_file=file,
                                         thresholds=range(1,11),
                                         variable='mean_price_change',
                                         save_name=None,
                                         beginStep=2)

def convergence():
	#file = 'one_time_productivity_shock_std_economy_powerlaw_CD.csv'
	file = 'one_time_productivity_shock_std_economy_powerlaw_CES.csv'
	processData.convergence_time1(csv_file=file,
                                         thresholds=range(1,11),
                                         variable='mean_price_change',
                                         save_name=None,
                                         beginStep=6)

def convergence_deg_variation():
	#file = 'powerlaw_exponent_economy_powerlaw_CD.csv'
	file = 'powerlaw_exponent_economy_powerlaw_CES.csv'
	processData.convergence_time1(csv_file=file,
                                         thresholds=range(1,11),
                                         variable='mean_price_change',
                                         save_name=None,
                                         beginStep=6)

def normalize():

	files = ['.csv']

	# which are the files to be normalized here ... 


	variables = ['gdp_equilibrium_prices']

	normal_position = 0
	transient_time = 0

	for csv_file in files:		
		processData.normalize(csv_file=csv_file,variables=variables,normPosition=normal_position,transientTime=transient_time)
	
	
	normal_position = 0
	transient_time = 0

	file = 'one_time_productivity_shock_std_economy_SF_CES.csv'
	#variables = ['gdp_equilibrium_prices','unemployment_rate']
	variables = ['gdp_equilibrium_prices']
		
	processData.normalize(csv_file=file,variables=variables,normPosition=normal_position,transientTime=transient_time)
	



	normal_position = 0
	transient_time = 0
	
	file = 'alpha_economy_networkUS70_CES_lockdownProductivityEquilibrium.csv'
	variables = ['gdp_equilibrium_prices']
	processData.normalize(csv_file=file,variables=variables,normPosition=normal_position,transientTime=transient_time)
	
	file = 'alpha_economy_networkUS70_CES_lockdownProductivity_disequilibrium.csv'
	processData.normalize(csv_file=file,variables=variables,normPosition=normal_position,transientTime=transient_time)
	
def process():
	directory_name = 'data_realShocks'
	os.chdir(directory_name)
	#multi()

	#normalize()
	#convergence()
	#convergence_deg_variation()
	#convergence_endoStep()
	#convergence_endo_stop()
	os.chdir('..')	


process()