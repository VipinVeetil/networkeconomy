from __future__ import division
import main
import cPickle
import parameters as p
import run
import write_network
import csv
import os
import matplotlib.pyplot as plt
from collections import OrderedDict
import copy

def parameter_sweep(network, production_function, weights_dynamic, n, time_steps, iterations, cores):
    if weights_dynamic:
        s = 'dynamic'
    else:
        s = 'static'
    fun = production_function + '_' + s
    parameter_names = [('s', 'g'), ('s','d'),('g','d'),'d', 's', 'g', 'n', 'alpha', 'gamma', 'psi', 'theta']

    household_preference_homogeneous = False
    Cobb_Douglas_production_function_homogeneous = False
    variables = {'economy': ['mean_price_change', 'cp', 'cv', 'wealth', 'zeta', 'eta',
                             'zetaC', 'etaC', 'rho', 'omega', 'rhoC', 'omegaC', 'welfare_mean',
                             'welfare_cv', 'inventoryCarry', 'wealthCarry'],
                 'firm': None, 'household': None}



    if network == 'B':
        household_preference_homogeneous = True
        Cobb_Douglas_production_function_homogeneous = True

    basicParamSetup = {'production_function': production_function,
                         'weights_dynamic': weights_dynamic,
                         'n': n,
                         'network_from_file': False,
                         'network_type': network,
                         'representative_household': True,
                         'transient_time_steps': time_steps,
                         'time_steps': time_steps,
                         'monetary_shock': True,
                         'monetary_shock_time_step': int(time_steps/4),
                        'household_preference_homogeneous': household_preference_homogeneous,
                        'Cobb_Douglas_production_function_homogeneous': Cobb_Douglas_production_function_homogeneous,
                      'connect_firms_without_buyers': True,
                      'connect_firms_without_sellers': True
                      }
    main.main(network_name = network, production_function=fun,  time_series={'economy':True, 'firm':False, 'household': False},
                  parameter_names=parameter_names,
                  parameter_ranges={('s', 'g'):{'s':[0,0.101],'g':[0.01, 1.01]},
                                    ('s', 'd'): {'s': [0, 0.101], 'd': [2,11]},
                                    ('g', 'd'): {'g':[0.01, 1.01], 'd': [2, 11]},
                                    ('s', 'g','d'):{'s':[0,0.101],'g':[0.01, 1.01], 'd':[2,11]},
                                    'd' : [2, 11],
                                    's': [-0.1, 0.101],
                                    'g': [0.01, 1.01],
                                    'n':[1000, 10001],
                                    'alpha':[0.1, 0.91],
                                    'gamma':[0, 0.201],
                                    'psi':[0.2, 0.501],
                                    'theta':[0, 1.01]},
                  parameter_increments={('s', 'g'):{'s':0.01,'g':0.1},
                                        ('s', 'd'): {'s': 0.01, 'd': 1},
                                        ('g', 'd'): {'g':0.1, 'd': 1},
                                        ('s', 'g','d'):{'s':0.01,'g':0.1,'d':1},
                                        'd':1,
                                        's': 0.001,
                                        'g': 0.01,
                                        'n':1000,
                                        'alpha': 0.01,
                                        'gamma':0.01,
                                        'psi':0.01,
                                        'theta':0.01},
                  variables={('s', 'g'):variables,
                             ('s', 'd'): variables,
                             ('g', 'd'): variables,
                            ('s', 'g','d'):variables,
                             'd':variables,
                            's': variables,
                            'g': variables,
                             'n': variables,
                             'alpha':variables,
                             'gamma': variables,
                             'psi': variables,
                             'theta': variables},
                  other_parameters_={('s', 'g'):basicParamSetup,
                                     ('s', 'd'): basicParamSetup,
                                     ('g', 'd'): basicParamSetup,
                      ('s', 'g','d'):basicParamSetup,
                      'd':{'production_function': production_function,
                                                                     'weights_dynamic': weights_dynamic,
                                                                     'n': n,
                                                                     'network_from_file': False,
                                                                     'representative_household': True,
                                                                     'transient_time_steps': time_steps,
                                                                     'time_steps': time_steps,
                                                                     'monetary_shock': True,
                                                                     'monetary_shock_time_step': int(time_steps/4),
                                                                     'household_preference_homogeneous':household_preference_homogeneous,
                                                                    'Cobb_Douglas_production_function_homogeneous':Cobb_Douglas_production_function_homogeneous,
                                          'connect_firms_without_buyers':True,
                                          'connect_firms_without_sellers': True},
                                        's': basicParamSetup ,
                                     'g': basicParamSetup ,
                                     'n': basicParamSetup ,
                                     'alpha': basicParamSetup ,
                                     'gamma': {'record_transient_data': True,
                                           'production_function': production_function,
                                           'weights_dynamic': weights_dynamic,
                                           'n': n,
                                           'network_from_file': False,
                                           'network_type': network,
                                           'representative_household': True,
                                           'transient_time_steps': int(4 * time_steps),
                                           'time_steps': time_steps,
                                           'monetary_shock': True,
                                           'monetary_shock_time_step': int(time_steps / 4),
                                           'wealthReserves': True,
                                               'smoothenPrice':True,
                                               'household_preference_homogeneous': household_preference_homogeneous,
                                               'Cobb_Douglas_production_function_homogeneous': Cobb_Douglas_production_function_homogeneous,
                                               'connect_firms_without_buyers': True,
                                               'connect_firms_without_sellers': True
                                               },
                                     'psi': {'record_transient_data': True,
                                               'production_function': production_function,
                                               'weights_dynamic': weights_dynamic,
                                               'n': n,
                                               'network_from_file': False,
                                               'network_type': network,
                                               'representative_household': True,
                                               'transient_time_steps': int(4 * time_steps),
                                               'time_steps': time_steps,
                                               'monetary_shock': True,
                                               'monetary_shock_time_step': int(time_steps / 4),
                                               'wealthReserves': True,
                                               'smoothenPrice': True,
                                             'gamma':0.1,
                                             'household_preference_homogeneous': household_preference_homogeneous,
                                             'Cobb_Douglas_production_function_homogeneous': Cobb_Douglas_production_function_homogeneous,
                                             'connect_firms_without_buyers': True,
                                             'connect_firms_without_sellers': True
                                             },
                                     'theta': {'production_function': production_function,
                                               'weights_dynamic': weights_dynamic,
                                               'n': n,
                                               'network_from_file': False,
                                               'network_type': network,
                                               'representative_household': True,
                                               'transient_time_steps':  time_steps,
                                               'time_steps': time_steps,
                                               'monetary_shock': True,
                                               'monetary_shock_time_step': int(time_steps / 4),
                                               'monetaryShock_weightsDistribution': True,
                                               'g': 1,
                                               'money_injection_agents': 'all',
                                               'wealthReserves': False,
                                               'smoothenPrice': False,
                                               'household_preference_homogeneous': household_preference_homogeneous,
                                               'Cobb_Douglas_production_function_homogeneous': Cobb_Douglas_production_function_homogeneous,
                                               'connect_firms_without_buyers': True,
                                               'connect_firms_without_sellers': True
                                               },
                                     },
                  iterations_={('s', 'g'):5,
                               ('s', 'd'): 5,
                               ('g', 'd'): 5,
                                ('s', 'g','d'):5,
                               'd':iterations,
                                's': iterations,
                               'g': iterations,
                               'n': iterations,
                               'alpha': iterations,
                               'gamma':iterations,
                               'psi':iterations,
                                'theta':iterations},
                  cores_={('s', 'g'):5,
                          ('s', 'd'): 5,
                          ('g', 'd'): 5,
                        ('s', 'g','d'):5,
                          'd': cores,
                            's': cores,
                          'g': cores,
                          'n': cores,
                          'alpha': cores,
                          'r': cores,
                          'gamma':cores,
                          'psi':cores,
                          'theta':cores},
                  model_name='network_economy',
                  model_url='url',
                  paper_name='cantillon',
                  paper_url='url')


def economy_pickle(network, production_function, weights_dynamic, n, time_steps, network_from_file, injection, otherParameters):
    parameters = p.Parameters()
    for system in parameters.data_time_series:
        parameters.data_time_series[system] = True
    for system in parameters.record_variables:
        for variable in parameters.record_variables[system]:
            parameters.record_variables[system][variable] = True

    parameters.s = injection
    parameters.network_type = network
    parameters.production_function = production_function
    parameters.weights_dynamic = weights_dynamic
    parameters.n = n
    parameters.time_steps = time_steps
    parameters.transient_time_steps = time_steps
    parameters.representative_household = True
    parameters.monetary_shock = True
    parameters.monetary_shock_time_step = int(time_steps / 4)
    parameters.network_from_file = network_from_file
    parameters.representative_household = True
    parameters.household_preference_homogeneous = False
    parameters.Cobb_Douglas_production_function_homogeneous = False
    parameters.connect_firms_without_buyers = True
    parameters.connect_firms_without_sellers = True
    if network == 'B':
        parameters.household_preference_homogeneous = True
        parameters.Cobb_Douglas_production_function_homogeneous = True

    if otherParameters == 'wealthCarry':
        parameters.transient_time_steps = 200
        parameters.firm_wealthCarry = True
        parameters.household_wealthCarry = True

    elif otherParameters == 'toyEconomy':
        parameters.network_from_file = True
        parameters.inflation_agents_ID = [1]
        parameters.household_preference_homogeneous = True
        parameters.Cobb_Douglas_production_function_homogeneous = True

    elif otherParameters == 'noisyShock':
        parameters.smoothenPrice = True
        parameters.wealthReserves = True
        parameters.money_injection_agents = 'all'
        parameters.g = 1
        parameters.monetaryShock_weightsDistribution = True
        parameters.theta = 1
        parameters.s = abs(injection)

    elif otherParameters == 'smoothening':
        parameters.smoothenPrice = True
        parameters.wealthReserves = True



    run_instance = run.Run(parameters)
    run_instance.create_economy()
    run_instance.transient()
    run_instance.time_steps()
    economy = run_instance.samayam.economy
    if injection > 0:
        n = 'positive'
    else:
        n = 'negative'
    if network_from_file:
        file_name = 'economyPickle' + '_' + production_function + '_' + 'file' + '_' + n
    else:
        file_name = 'economyPickle' + '_' + production_function + '_' + network + '_' + n

    if otherParameters is not False:
        file_name += '_'
        file_name += otherParameters

    directory_name = 'data' + '_' + 'cantillon'
    if not os.path.exists(directory_name):
        os.makedirs(directory_name)
    os.chdir(directory_name)


    with open('%s.cPickle' % file_name, 'wb') as econ:
        cPickle.dump(economy, econ, protocol=cPickle.HIGHEST_PROTOCOL)
    os.chdir('..')

def single_firm_injections(network, production_function, weights_dynamic, n, time_steps, d):
    print 'single firm injections', network, production_function
    directory_name = 'data' + '_' + 'cantillon'
    if not os.path.exists(directory_name):
        os.makedirs(directory_name)
    os.chdir(directory_name)
    parameters = p.Parameters()
    for system in parameters.data_time_series:
        parameters.data_time_series[system] = True
    for system in parameters.record_variables:
        for variable in parameters.record_variables[system]:
            parameters.record_variables[system][variable] = True
    parameters.network_type = network
    parameters.production_function = production_function
    parameters.weights_dynamic = weights_dynamic
    parameters.n = n
    parameters.time_steps = time_steps
    parameters.transient_time_steps = time_steps
    parameters.representative_household = True
    parameters.monetary_shock = True
    parameters.monetary_shock_time_step = int(time_steps / 4)
    parameters.d = d
    parameters.money_injection_agents = 'single_firm'
    parameters.s = 0.01

    write_network.theoretical_network(network_type=network,
                                      n=n,
                                      d=d)
    print "done writing"
    file_name = 'single_firm_injections' + '_' + network
    with open('%s.csv' % file_name, 'wb') as data_csv:
        writer_data = csv.writer(data_csv, delimiter=',')
        writer_data.writerow(['Firm ID'] + ['In_degree'] + ['Out_degree'] +
                             ['cv'] + ['eta'] + ['zeta'])
        for ID in xrange(1, n + 1):
            if ID % 100 == 0:
                print ID
            parameters.money_injection_firm_ID = ID
            run_instance = run.Run(parameters)
            run_instance.create_economy()
            run_instance.transient()
            run_instance.time_steps()
            firm = run_instance.samayam.economy.firms[ID]
            In_degree = firm.number_of_input_sellers
            Out_degree = firm.number_of_output_buyers
            cv = run_instance.samayam.economy.v
            eta = run_instance.samayam.economy.eta
            zeta = run_instance.samayam.economy.zeta

            writer_data.writerow([ID] + [In_degree] + [Out_degree] + [cv] + [eta] + [zeta])
    os.chdir('..')


def simulations():
    networks = ['SF', 'B']
    fun = 'CD'
    n = 1000
    time_steps = 100
    iterations = 100
    cores = 5
    weights_dynamic = False
    for network in networks:
        print network, 'network'
        parameter_sweep(network, fun, weights_dynamic, n, time_steps, iterations, cores)






def pickle_economy():
    print 'pickle'
    networks = ['SF', 'B']
    #types = [False, 'noisyShock', 'smoothening']
    types = ['noisyShock']
    injections = [-0.01, 0.01]
    n = 10000
    for injection in injections:
        for network in networks:
            print network, injection
            for t in types:
                economy_pickle(network=network,
                                production_function='CD',
                                weights_dynamic=False,
                                n=n,
                                time_steps=100,
                                network_from_file= False,
                                injection=injection,
                               otherParameters=t)

def toy_economy():
    with open('network.txt', 'w') as network:
        a = [str(1), str(2)]
        a = ",".join(a)
        b = [str(2), str(3)]
        b = ",".join(b)
        c = [str(3), str(1)]
        c = ",".join(c)
        network.write('%s ' % a + '\n')
        network.write('%s ' % b + '\n')
        network.write('%s ' % c + '\n')

    para = p.Parameters()
    para.monetary_shock_time_step = 10
    para.s = 0.01
    para.data_time_series['economy'] = True
    para.data_time_series['firm'] = True
    para.data_time_series['household'] = True
    para.monetary_shock = True
    para.network_from_file = True

    for var in para.record_variables['economy']:
        para.record_variables['economy'][var] = True
    for var in para.record_variables['firm']:
        para.record_variables['firm'][var] = True
    for var in para.record_variables['household']:
        para.record_variables['household'][var] = True


    inflation_agents_ID = {'One': [1], 'Two': [1, 2], 'Three': [1, 2, 3], 'All': 'All'}
    for item in inflation_agents_ID:
        if item == 'All':
            para.g = 1
            para.money_injection_agents = 'all'
            save_name = 'allAgents'
        else:
            IDS = inflation_agents_ID[item]
            para.inflation_agents_ID = IDS
            l = len(IDS)
            save_name = str(l) + 'Firms'
        run_instance = run.Run(para)
        run_instance.create_economy()
        run_instance.transient()
        run_instance.time_steps()
        f1 = run_instance.samayam.economy.firms[1].price_time_series
        ss = f1[0]
        f1 = [i/ss for i in f1]
        f2 = run_instance.samayam.economy.firms[2].price_time_series
        ss = f2[0]
        f2 = [i/ss for i in f2]
        f3 = run_instance.samayam.economy.firms[3].price_time_series
        ss = f3[0]
        f3 = [i/ss for i in f3]

        a = 8
        b = 30
        x_ran = range(-2, b - a - 2)
        fontSize_supTitle = 22
        fontSize_xlabel = 20
        fontSize_ylabel = 20
        fontSize_ticks = 20
        legend_size = 18
        fig, ax = plt.subplots()
        ax.ticklabel_format(useOffset=True, useLocale=True, useMathText=True)
        ax.tick_params(axis='both', labelsize=fontSize_ticks)
        plt.plot(x_ran, f1[a:b], label='Firm A', color='black')
        plt.plot(x_ran, f2[a:b], label='Firm B', color='pink')
        plt.plot(x_ran, f3[a:b], label='Firm C', color='cornflowerblue')

        plt.xlim(xmin=-2)
        plt.xlabel('Time steps', fontsize=fontSize_xlabel)
        plt.ylabel('Price', fontsize=fontSize_ylabel)
        plt.suptitle('Price Time Series', fontsize=fontSize_supTitle)
        plt.legend(fontsize=legend_size, loc=4)
        if not os.path.exists('data_cantillon'):
            os.makedirs('data_cantillon')
        os.chdir('data_cantillon')
        if not os.path.exists('plots_cantillon'):
            os.makedirs('plots_cantillon')
        os.chdir('plots_cantillon')
        png_name = 'toyEconomy_'
        png_name += save_name
        plt.savefig('%s.png' % png_name, bbox_inches='tight')
        plt.close()
        os.chdir('..')
        os.chdir('..')


simulations()
#pickle_economy()
#toy_economy()